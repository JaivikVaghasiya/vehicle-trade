import express, { Express } from 'express';
import { ConnectToDataBase } from './src/config';
import { errorHandler } from './src/error/handler';
import router from './src/routes';
import swaggerUI from 'swagger-ui-express';
import fileUpload from 'express-fileupload';
import cors from 'cors';
import helmet from 'helmet';
import { logger, swaggerSpecs as options, setupLogger } from './src/utils';

require('dotenv').config();
ConnectToDataBase();
setupLogger();

const app: Express = express();
const port = process.env.Port;
const allowedOrigins = [
  process.env.BASE_URL,
  process.env.FRONTED_URL_1,
  process.env.FRONTED_URL_2,
  process.env.FRONTED_URL_3,
];

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  '/api',
  swaggerUI.serve,
  swaggerUI.setup(options, {
    swaggerOptions: {
      persistAuthorization: true,
      displayRequestDuration: true,
      tryItOutEnabled: true,
      validatorUrl: null,
      defaultModelsExpandDepth: -1,
      // docExpansion: 'none',
      filter: true,
      operationsSorter: 'alpha',
      showExtensions: false,
      showRequestHeaders: true,
    },
  }),
);

app.use(
  cors({
    credentials: true,
    origin: function (origin, callback) {
      console.log('origin :>>', origin);
      if (allowedOrigins.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        console.log('Not allowed by cors :>> ');
        callback(null, true);
      }
    },
  }),
);

app.use(function (req, res, next) {
  if (allowedOrigins.includes(req.headers.origin)) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
  }
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept',
  );
  next();
});

// app.use(helmet());

app.use(express.static('client'));
app.use('/uploads', express.static('./uploads'));
app.use(fileUpload());
app.use('/vehicletrade', router);
app.use(errorHandler);
app.listen(port, () =>
  logger.info(
    'server fire on ' +
      port +
      `\n http://localhost:${port} \n Swagger fired on http://localhost:${port}/api`,
  ),
);
