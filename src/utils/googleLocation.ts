import requests from 'requests';
import { logger } from './logging';

/**
 * This function retrieves address by user location
 * @param latitude
 * @param longitude
 * @returns location details
 */
export const geoLocation = async (latitude: string, longitude: string) => {
  try {
    const position: string = `https://api.opencagedata.com/geocode/v1/json?q=${latitude},${longitude}&key=${process.env.GEOCODE_API_KEY}`;
    const promises = new Promise(async (resolve, reject) => {
      await requests(position).on('data', async function (chunk: any) {
        try {
          let data = JSON.parse(chunk);
          let { components, geometry } = data.results[0];
          let url = `https://google.com/maps?q=${data.results[0].geometry.lat},${data.results[0].geometry.lng}`;
          let saveTheData = {
            latitude: geometry.lat,
            longitude: geometry.lng,
            countryCode: components['ISO_3166-1_alpha-3'],
            country: components.country,
            state: components.state || 'unknown',
            district: components.hasOwnProperty('state_district')
              ? components.state_district.split(' ')[0]
              : null,
            area: components.suburb,
            city: components.hasOwnProperty('county')
              ? components.county.split(' ')[0]
              : null,
            road: components?.road != 'unnamed road' ? components.road : null,
            mapLink: url,
            postCode: components.hasOwnProperty('postcode')
              ? components.postcode
              : '',
          };
          resolve(saveTheData);
        } catch (error) {
          reject(error);
        }
      });
    });
    const data = await promises;
    return data;
  } catch (error) {
    console.log('error geoLocation :>> ', error);
    throw error;
  }
};
