import { sign, verify } from 'jsonwebtoken';
import { Utils, PaginateQuery } from '../types';
import { Types, isValidObjectId } from 'mongoose';
import { logger } from './logging';
import { HttpStatus, Message } from './localization';
import { BadRequestError } from '../error';
import { nodemailer } from 'nodemailer';

/**
 * Send response with common function
 */
const response: Utils.CResponse = (
  success: boolean,
  message: string,
  data: any,
) => {
  if ((data == null && ' ') || !data) {
    return {
      success: success,
      message: message,
    };
  }
  return {
    success: success,
    message: message,
    data: data,
  };
};
export const ObjectId = Types.ObjectId;
export const services: Utils.CServices | any = {
  /**
   * This function use to send response
   */
  sendResponse: (res: any, httpStatus, message, data) => {
    if (httpStatus == 200) {
      logger.info(res.req.method + ' ' + res.req.originalUrl + ' ' + 200);
      return res.status(httpStatus).send(response(true, message, data));
    }
    logger.error(
      `${res.req.method}  ${res.req.originalUrl} ${httpStatus} ${message}}`,
    );
    return res.status(httpStatus).send(response(false, message, data));
  },

  /**
   * This function send response of success
   * @param res res
   * @param message messageexample of seaching in nodejs with mongodb using regex


   * @param payload response payload
   */
  sendSuccess: (res, message, payload) => {
    return services.sendResponse(res, HttpStatus.SUCCESS, message, payload);
  },

  /**
   * This function use to generateToken
   */
  generateToken: (user, expireTime) => {
    const token: any = sign(
      { _id: `${user._id}`, role: `${user.role}` },
      process.env.SECURITY_KEY,
      {
        expiresIn: expireTime,
      },
    );
    return token;
  },

  /**
   * This function use to verify token
   */
  verifyToken: (token) => {
    try {
      const decode: any = verify(token, process.env.SECURITY_KEY);
      return decode;
    } catch (error) {
      if (error.message === 'jwt expired') {
        return true;
      }
      throw new Error(error);
    }
  },

  /**
   * Searching function
   */
  searching: {
    search: (search, operator = '$or') => {
      let filter: any = { [operator]: [] };
      for (let key in search) {
        let values =
          Array.isArray(search[key]) && search[key].length == 0
            ? null
            : search[key];
        const data =
          values && Array.isArray(values) && values.length
            ? {
                [key]: {
                  $in: values.map((value) => {
                    return isValidObjectId(value) ? new ObjectId(value) : value;
                  }),
                },
              }
            : values && { [key]: values };
        if (!data) continue;
        filter[operator].push(data);
      }
      filter = filter[operator].length ? filter : {};
      logger.info('\n' + 'filter >>>', filter);
      return filter;
    },
    regexSearch: (search, keys) => {
      let filter: any = {};
      filter.$or = keys.map((key) => {
        return {
          [key]: {
            $regex: search,
            $options: 'i',
          },
        };
      });
      return filter;
    },
  },
  /**
   * This function is use to paginate and sorting data
   * @param query page, limit, sortField, sortType
   * @param defaultSortField default sort field
   * @returns {page:number, perPage:number,sortObj: any, page: number }
   */
  paginationAndSorting: (query, defaultSortField) => {
    // pagination
    const { page, limit, sortField, sortType }: PaginateQuery = query;
    let pageNumber: number = page ? parseInt(page) : 1;
    let perPage: number = limit ? parseInt(limit) : 10;
    // Sorting
    let sortObj: { [key: string]: number } =
      sortField && sortType == 'asc'
        ? { [sortField]: 1 }
        : sortField && sortType == 'desc'
        ? { [sortField]: -1 }
        : { [defaultSortField]: -1 };
    return {
      pageNumber,
      skipRecord: (pageNumber - 1) * perPage,
      perPage,
      sortObj,
    };
  },

  /**
   * Add pagination details in response
   * @param data
   * @param param1
   * @param totalRecord
   * @returns paginated response
   */
  paginateResponse: (data, { pageNumber, perPage }, totalRecord) => {
    const dataList: any = {};
    dataList.list = data;
    dataList.page = pageNumber;
    dataList.limit = data.length;
    dataList.RecordsFiltered = data.length;
    dataList.totalCount = totalRecord;
    dataList.totalPage = Math.ceil(totalRecord / perPage);
    return dataList;
  },
};
