import log4js from 'log4js';
import moment from 'moment';

/** @enum {string} */
const LogLevel = {
  OFF: 'off',
  DEBUG: 'debug',
  INFO: 'info',
  WARN: 'warn',
  ERROR: 'error',
};

const DEFAULT_LOG_LEVEL: string = LogLevel.INFO;

// Configures default logging.
log4js.configure({
  appenders: { out: { type: 'stdout' } },
  categories: { default: { appenders: ['out'], level: DEFAULT_LOG_LEVEL } },
});

const logger = log4js.getLogger('server');
const httpLogger = log4js.getLogger('http');
const httpLoggingMiddleware = log4js.connectLogger(httpLogger, {
  level: 'auto',
  format: '`:status ":method :url"',
});

/**
 * Disable logging (it is useful for testing).
 */
function disableLogging() {
  log4js.shutdown();
  log4js.configure({
    appenders: {
      out: { type: 'stdout' },
      app: {
        type: 'file',
        filename: `./logs/${moment().format('DD-MM-YYYY')}.log`,
      },
    },
    categories: {
      default: { appenders: ['out', 'app'], level: 'debug' },
      // http: { appenders: ['out', 'app'], level: 'debug' },
    },
  });
}

/**
 * Configures logger.
 *
 * @param {{logLevel?: string, httpLevel?: string}} options
 */
function setupLogger(options: any = {}) {
  options.logLevel = options.logLevel || DEFAULT_LOG_LEVEL;
  options.httpLevel = options.httpLevel || DEFAULT_LOG_LEVEL;

  logger.debug(`new logger options: ${JSON.stringify(options)}`);

  log4js.shutdown();

  log4js.configure({
    appenders: {
      out: { type: 'stdout' },
      app: {
        type: 'file',
        filename: `./logs/${moment().format('DD-MM-YYYY')}.log`,
      },
    },
    categories: {
      default: { appenders: ['out', 'app'], level: 'debug' },
      // http: { appenders: ['out', 'app'], level: options.httpLevel },
    },
  });
}

/**
 * Closes logger.
 * @returns {Promise<void>}
 */
async function closeLogger() {
  return new Promise((resolve: any, reject) => {
    log4js.shutdown((err) => (err ? reject(err) : resolve()));
  });
}

export {
  LogLevel,
  logger,
  httpLoggingMiddleware,
  disableLogging,
  setupLogger,
  closeLogger,
};
