import twilio from 'twilio';
import { User } from '../types';
require('dotenv').config({ path: '.env' });
const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN,
);

/**
 * Send otp by twilio service
 * @param mobileNo
 * @returns
 */
export const sendOtp: User.SendOtp = async (mobileNo) => {
  try {
    const { sid } = await client.verify.services.create({
      friendlyName: 'Verification Service',
      codeLength: 4,
    });
    return await client.verify.services(sid).verifications.create({
      to: `+91${mobileNo}`,
      channel: 'sms',
    });
  } catch (error) {
    throw new Error(error.message || error);
  }
};

/**
 * verify Otp
 * @param mobileNo
 * @param code
 * @param sid
 * @returns
 */
export const verifyOtp: User.VerifyOtp = async (mobileNo, code, sid) => {
  try {
    return await client.verify.services(sid).verificationChecks.create({
      code: code,
      to: `+91${mobileNo}`,
    });
  } catch (error) {
    console.log('error :>> ', error);
    throw new Error(error.message || error);
  }
};
