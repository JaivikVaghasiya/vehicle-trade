import swaggerJSDoc from 'swagger-jsdoc';

/**
 * swagger options
 */
const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Car-trade',
      version: '1.0.0',
      description: 'Admin panel',
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        },
      },
    },
    servers: [
      {
        url: 'http://localhost:8080',
      },
      {
        url: 'http://0.0.0.0:8080',
      },
    ],
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ['./src/swagger/*/*.ts'],
};

export const swaggerSpecs = swaggerJSDoc(options);
