/**
 * @type enum number
 */
export enum ROLE {
  ADMIN = 1,
  DEALER,
  USER,
  MECHANIC,
}

/**
 * @type enum
 */
export enum APPROVAL_STATUS {
  APPROVED = 1,
  REJECTED,
  PENDING,
}
