import fs from 'fs';
import moment from 'moment';
import { Utils } from '../types';
import { BadRequestError, RequestError } from '../error';
import { HttpStatus, Message } from './localization';
import path from 'path';
import { logger } from './logging';

const commonFunction = (key, value, id) => {
  let imageData: Partial<ImageData | any | any[]> = [];
  let randomNumber: number =
    100000 + Math.floor(Math.random() * 900000) + moment().unix();
  value = value.length == undefined ? [value] : value;
  value.forEach((image: any) => {
    const extension = path.extname(image.name);
    if (!['.svg', '.png', '.jpg', '.webp', '.jpeg'].includes(extension))
      throw new BadRequestError(Message.ONLY_JPG_PNG_SVG_IMAGE_ALLOWED);
    image.mv(
      `./uploads/${id}/` + randomNumber + image.name.trim(),
      (error: any) => {
        if (error) {
          return { error };
        }
      },
    );
    imageData.push({
      [key]: `/uploads/${id}/` + randomNumber + image.name.trim(),
      vehicleId: id,
    });
  });
  return imageData;
};

/**
 * Save images in local
 * @param param0
 * @param id
 * @returns imageData
 */
export const uploadImage: Utils.UploadImage = (
  [firstBunch, secondBunch, thirdBunch],
  id,
) => {
  try {
    let imageData: any[] = [];
    if (!fs.existsSync(`./uploads/${id}/`)) {
      fs.mkdirSync(`./uploads/${id}/`, { recursive: true });
    }

    if (firstBunch && firstBunch.value !== undefined) {
      let { value, key } = firstBunch;
      let data: any = commonFunction(key, value, id);
      imageData.push(...data);
    }

    if (secondBunch && secondBunch.value !== undefined) {
      let { value, key } = secondBunch;
      const data: any = commonFunction(key, value, id);
      imageData.push(...data);
    }

    if (thirdBunch && thirdBunch.value !== undefined) {
      let { value, key } = thirdBunch;
      const data: any = commonFunction(key, value, id);
      imageData.push(...data);
    }
    return imageData;
  } catch (error) {
    throw new RequestError(
      error.code || HttpStatus.BAD_REQUEST,
      Message.SOMETHING_WENT_WRONG,
      error,
    );
  }
};

//deleting files from local storage;
export const deleteFile: Utils.DeleteFile = (filePath: string, id: string) => {
  try {
    if (fs.existsSync(`.${filePath}`)) {
      logger.info('\n' + filePath + ' directory Found');
      const results = fs.unlinkSync(`.${filePath}`);
      logger.info('results >>', results);
      // Deleting directory if empty
      const files = fs.readdirSync(`./uploads/${id}`);
      if (files.length == 0) {
        fs.rmdirSync(`./uploads/${id}`);
      }
    }
    logger.info('\n' + filePath + ' directory  not Found');
    return { success: true };
  } catch (error) {
    throw error;
  }
};
