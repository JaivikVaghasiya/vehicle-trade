import nodemailer from 'nodemailer';
import { BadRequestError } from '../error';
import { Message } from './localization';
import { logger } from './logging';

/**
 * Send mail to user with forgot password body
 * @param email
 * @param token jwt token
 */
export async function sendEmail(email, token) {
  console.log('token>>>', token);
  //   let testAccount = await nodemailer.createTestAccount();
  let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASS,
    },
  });
  // send mail with defined transport object
  let mailOptions = await transporter.sendMail(
    {
      from: process.env.EMAIL_ADDRESS, // sender address
      to: email, // list of receivers
      subject: 'body.subject', // Subject line
      text: 'body.text', // plain text body
      html: `<body>
    <div style="background-color: #f5f5f5; padding: 20px;">
      <table style="max-width: 600px; margin: 0 auto; background-color: #ffffff; padding: 20px; border-radius: 4px;">
        <tr>
          <td align="center">
            <h1>Password Reset</h1>
          </td>
        </tr>
        <tr>
          <td>
            <p>Dear User,</p>
            <p>We have received a request to reset your password. To proceed with the password reset, click the button below:</p>
            <div style="text-align: center; margin-top: 30px;">
              <a href="${process.env.FRONTEND_BASE_URL}/auth/password/${token}" style="background-color: #007bff; color: #ffffff; padding: 12px 24px; text-decoration: none; border-radius: 4px;">Reset Password</a>
            </div>
            <p>If you didn't initiate this request, please ignore this email.</p>
            <p>Thank you!</p>
          </td>
        </tr>
      </table>
    </div>
  </body>`, // html body
    },
    function (error, info) {
      if (error) {
        throw new BadRequestError(Message.SOMETHING_WENT_WRONG);
      } else {
        logger.info('Email sent: ' + info.response);
      }
    },
  );
}
