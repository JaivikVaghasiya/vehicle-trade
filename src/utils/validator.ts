import { Message } from './localization';
import { APPROVAL_STATUS, ROLE } from './enum';
import { isValidPhoneNumber } from 'libphonenumber-js';
import { logger } from './logging';
import { Utils } from '../types';
import { BadRequestError } from '../error';
import { isJSON, isLength, isEmpty } from 'validator';

const PHONE_NUMBER_KEY = 'phone';
const PHONE_CODE_KEY = 'countryCode';
const isMobilePhone = (options) => {
  return (value, { req, location, path }) => {
    // const keys = options.keys
    //   ? options.keys
    //   : [PHONE_NUMBER_KEY, PHONE_CODE_KEY];
    // const newPath = path.replace(keys[0], keys[1]);
    //  const code = at(req[location], newPath)[0]
    return isValidPhoneNumber(`+${91}${value}`);
  };
};

const is12HourTime = (value) => {
  // Use a regular expression to match the input against the expected format
  const regex = /^(1[0-2]|0?[1-9]):([0-5][0-9])\s?(AM|PM)$/i;
  return regex.test(value);
};

const location = (value) => {
  if (!isJSON(value)) {
    return false;
  }
  const keys = ['address', 'country', 'state', 'district', 'postCode', 'city'];
  value = JSON.parse(value);
  const bodyKeys = Object.keys(value);
  for (const key of keys) {
    if (!bodyKeys.includes(key) || isEmpty(value[key]) || !value[key])
      throw new BadRequestError(`${key} is required`);
  }
  if (!isLength(value.postCode, { min: 6, max: 6 }))
    throw new BadRequestError(Message.INVALID_ZIP_CODE);

  if (value?.countryCode && !isLength(value.countryCode, { min: 2, max: 3 }))
    throw new BadRequestError(Message.INVALID_COUNTRY_ISO_CODE);
  if (!isLength(value.country, { min: 2, max: 30 }))
    throw new BadRequestError(Message.INVALID_COUNTRY_NAME);
  if (!isLength(value.address, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_ADDRESS);
  if (!isLength(value.state, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_STATE_NAME);
  if (!isLength(value.district, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_DISTRICT);
  if (value?.road && !isLength(value.road, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_ROAD);
  if (value?.area && !isLength(value.area, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_AREA);
  if (!isLength(value.city, { min: 1, max: 50 }))
    throw new BadRequestError(Message.INVALID_CITY);
  return true;
};

/**
 * This function is used to check files if required
 * @param options options
 * @returns boolean
 */
const isFile = (options): any => {
  const { required, keys, message }: Utils.IsFileOptions = options;
  return (value: any, { req }) => {
    if (required) {
      if (!req.files) throw new Error('File is required');
      keys.forEach((element, index) => {
        if (!req.files[element]) {
          throw new Error(`${element} is required`);
        }
      });
    }
    return true;
  };
};

/**
 * General check wrapper
 *
 * @param {any} location location
 * @param {string} path path
 * @param {any} rule rule
 * @param {any} options options
 * @returns {any}
 */
const check = (location, path, rule, options: any = {}) => {
  rule = rule || Rule.unknown;
  const Message = options.message || rule.message;
  const chain = checkMandatory(location, path, Message, options);
  return rule.check(chain, options);
};

/**
 * check if field is mandatory
 *
 * @param {any} location location
 * @param {string} path path
 * @param {any} Message Message
 * @param {any} options options
 * @returns {any}
 */
const checkMandatory = (location, path, Message, options) => {
  const { required, empty, file } = options;
  const chain = location(path, Message.invalid);
  if (file) return chain;
  if (required) return chain.notEmpty().withMessage(Message.required).bail();
  return chain.optional({ nullable: true, checkFalsy: !!empty });
};

const flags = [
  'vehicle-type',
  'fuel-type',
  'transmission-type',
  'body-type',
  'vehicle-type-car',
  'vehicle-type-bus',
  'vehicle-type-truck',
  'vehicle-type-bike',
  'vehicle-type-bicycle',
  'vehicle-type-auto-rikshaw',
];

/**
 * apply all validation
 */
const verify = {
  unknown: (chain) => chain,
  email: (chain) => chain.trim().isEmail(),
  password: (chain) =>
    chain
      .trim()
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      ),
  shortText: (chain) => chain.isLength({ min: 1, max: 50 }),
  unsignedInt: (chain) => chain.isInt({ min: 0 }).toInt(),
  otp: (chain) => chain.isLength({ min: 4, max: 4 }).toInt(),
  mongoId: (chain) => chain.trim().isMongoId(),
  name: (chain) => chain.trim().escape().isLength({ max: 50 }),
  age: (chain) => chain.isInt({ min: 10 }).toInt(),
  longText: (chain) => chain.isLength({ min: 1, max: 500 }),
  JWT: (chain) => chain.isJWT(),
  userType: (chain) =>
    chain
      .isInt()
      .toInt()
      .isIn([ROLE.ADMIN, ROLE.DEALER, ROLE.USER, ROLE.MECHANIC]),
  mobilePhone: (chain, options) => chain.trim().custom(isMobilePhone(options)),
  price: (chain) => chain.trim().isInt().toInt().isLength({ min: 0, max: 12 }),
  year: (chain) => chain.trim().isLength({ min: 0, max: 4 }),
  kiloMeter: (chain) => chain.trim().isLength({ min: 0, max: 5 }),
  locationCode: (chain) => chain.trim().isFloat(),
  ownerType: (chain) => chain.isIn([1, 2, 3, 4, 5]),
  ownerTypeArray: (chain) => chain.isArray().isIn([1, 2, 3, 4, 5]),
  colorCode: (chain) => chain.trim().isLength({ max: 7 }),
  files: (chain, options) => chain.custom(isFile(options)),
  isoCode: (chain) => chain.trim().isLength({ min: 2, max: 3 }),
  requestStatus: (chain) =>
    chain
      .isInt()
      .toInt()
      .isIn([
        APPROVAL_STATUS.APPROVED,
        APPROVAL_STATUS.REJECTED,
        APPROVAL_STATUS.PENDING,
      ]),
  approveStatus: (chain) =>
    chain
      .isInt()
      .toInt()
      .isIn([APPROVAL_STATUS.APPROVED, APPROVAL_STATUS.REJECTED]),
  shortType: (chain) => chain.isIn(['asc', 'desc']),
  status: (chain) => chain.isBoolean(),
  object: (chain) => chain.isObject(),
  zipCode: (chain) => chain.isInt().toInt().isLength({ max: 6 }),
  array: (chain) => chain.isArray(),
  time: (chain) => chain.custom(is12HourTime),
  storeLocation: (chain) => chain.custom(location),
  mongoIdArray: (chain) => chain.isArray().trim().isMongoId(),
  uniqueFlag: (chain) => chain.isLength({ mix: 1, max: 50 }).trim(),
  timeStamp: (chain) => chain.trim().isLength({ min: 13, max: 13 }),
  sid: (chain) => chain.isLength({ min: 34, max: 34 }),
};

/**
 * define rules of validation path
 */
const Rule = {
  unknown: {
    check: verify.unknown,
    message: {},
  },
  email: {
    check: verify.email,
    message: {
      invalid: Message.INVALID_EMAIL,
      required: Message.EMAIL_IS_REQUIRED,
    },
  },
  userId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_USER_ID,
      required: Message.USER_ID_IS_REQUIRED,
    },
  },
  vehicleId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_VEHICLE_ID,
      required: Message.VEHICLE_ID_IS_REQUIRED,
    },
  },
  vehicleDetailsId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_VEHICLE_DETAILS_ID,
      required: Message.VEHICLE_DETAILS_ID_IS_REQUIRED,
    },
  },
  commentId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_COMMENTS_ID,
      required: Message.COMMENT_ID_IS_REQUIRED,
    },
  },
  favoriteId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_FAVORITE_ID,
      required: Message.FAVORITE_ID_IS_REQUIRED,
    },
  },
  dealerStoreId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_DEALER_STORE_ID,
      required: Message.DEALER_STORE_ID_IS_REQUIRED,
    },
  },
  activityId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_ACTIVITY_ID,
      required: Message.ACTIVITY_ID_IS_REQUIRED,
    },
  },
  vehicleType: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_VEHICLE_TYPE,
      required: Message.VEHICLE_TYPE_IS_REQUIRED,
    },
  },
  brandId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_BRAND_NAME,
      required: Message.BRAND_NAME_IS_REQUIRED,
    },
  },
  modelId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_MODEL_NAME,
      required: Message.MODEL_NAME_IS_REQUIRED,
    },
  },
  variantId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_VARIANT_NAME,
      required: Message.VARIANT_NAME_IS_REQUIRED,
    },
  },
  fuelTypeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_FUEL_TYPE,
      required: Message.FUEL_TYPE_IS_REQUIRED,
    },
  },
  bodyTypeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_BODY_TYPE,
      required: Message.BODY_NAME_IS_REQUIRED,
    },
  },
  transmissionTypeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_TRANSMISSION_TYPE,
      required: Message.TRANSMISSION_TYPE_IS_REQUIRED,
    },
  },
  imageId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_IMAGE_ID,
      required: Message.IMAGE_ID_IS_REQUIRED,
    },
  },
  attributeTypeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_ATTRIBUTE_TYPE,
      required: Message.ATTRIBUTE_TYPE_IS_REQUIRED,
    },
  },
  attributeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_ATTRIBUTE,
      required: Message.ATTRIBUTE_IS_REQUIRED,
    },
  },
  countryId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_COUNTRY_ID,
      required: Message.COUNTRY_ID_IS_REQUIRED,
    },
  },
  stateId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_STATE_NAME,
      required: Message.STATE_NAME_IS_REQUIRED,
    },
  },
  password: {
    check: verify.password,
    message: {
      invalid: Message.INVALID_PASSWORD,
      required: Message.PASSWORD_IS_REQUIRED,
    },
  },
  search: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_SEARCH,
    },
  },
  isDropdown: {
    check: verify.status,
    message: {
      invalid: Message.INVALID_ISDROPDOWN,
      required: Message.ISDROPDOWN_IS_REQUIRED,
    },
  },
  page: {
    check: verify.unsignedInt,
    message: { invalid: Message.INVALID_PAGE },
  },
  perPage: {
    check: verify.unsignedInt,
    message: { invalid: Message.INVALID_PER_PAGE },
  },
  sortField: {
    check: verify.shortText,
    message: { invalid: Message.INVALID_SORT_FIELD },
  },
  sortType: {
    check: verify.shortType,
    message: { invalid: Message.INVALID_SORT_TYPE },
  },
  firstName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_FIRST_NAME,
      required: Message.FIRST_NAME_IS_REQUIRED,
    },
  },
  lastName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_LAST_NAME,
      required: Message.LAST_NAME_IS_REQUIRED,
    },
  },
  startTime: {
    check: verify.timeStamp,
    message: {
      invalid: Message.INVALID_START_OF_TIME,
      required: Message.START_OF_TIME_IS_REQUIRED,
    },
  },
  endTime: {
    check: verify.timeStamp,
    message: {
      invalid: Message.INVALID_END_OF_TIME,
      required: Message.END_OF_TIME_IS_REQUIRED,
    },
  },
  mobilePhone: {
    check: verify.mobilePhone,
    message: {
      invalid: Message.INVALID_MOBILE_NUMBER,
      required: Message.LAST_NAME_IS_REQUIRED,
    },
  },
  age: {
    check: verify.age,
    message: {
      invalid: Message.INVALID_AGE,
      required: Message.AGE_IS_REQUIRED,
    },
  },
  address: {
    check: verify.longText,
    message: { invalid: Message.INVALID_ADDRESS },
  },
  note: {
    check: verify.longText,
    message: { invalid: Message.INVALID_NOTE },
  },
  type: {
    check: verify.userType,
    message: {
      invalid: Message.INVALID_TYPE,
      required: Message.TYPE_IS_REQUIRED,
    },
  },
  token: {
    check: verify.JWT,
    message: {
      invalid: Message.INVALID_TOKEN,
      required: Message.TOKEN_IS_REQUIRED,
    },
  },
  otp: {
    check: verify.otp,
    message: {
      invalid: Message.INVALID_OTP,
      required: Message.OTP_IS_REQUIRED,
    },
  },
  price: {
    check: verify.price,
    message: {
      invalid: Message.INVALID_PRICE,
      required: Message.PRICE_IS_REQUIRED,
    },
  },
  year: {
    check: verify.price,
    message: {
      invalid: Message.INVALID_YEAR,
      required: Message.YEAR_IS_REQUIRED,
    },
  },
  kiloMeter: {
    check: verify.price,
    message: {
      invalid: Message.INVALID_KILOMETER,
      required: Message.KILOMETER_IS_REQUIRED,
    },
  },
  longitude: {
    check: verify.locationCode,
    message: {
      invalid: Message.INVALID_LONGITUDE,
      required: Message.LONGITUDE_IS_REQUIRED,
    },
  },
  latitude: {
    check: verify.locationCode,
    message: {
      invalid: Message.INVALID_LATITUDE,
      required: Message.LATITUDE_IS_REQUIRED,
    },
  },
  country: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_COUNTRY_NAME,
      required: Message.COUNTRY_NAME_IS_REQUIRED,
    },
  },
  state: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_STATE_NAME,
      required: Message.STATE_NAME_IS_REQUIRED,
    },
  },
  comment: {
    check: verify.longText,
    message: {
      invalid: Message.INVALID_COMMENT,
      required: Message.COMMENT_IS_REQUIRED,
    },
  },
  employeeName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_EMPLOYEE_NAME,
      required: Message.EMPLOYEE_NAME_IS_REQUIRED,
    },
  },
  storeName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_STORE_NAME,
      required: Message.STORE_NAME_IS_REQUIRED,
    },
  },
  storeLocation: {
    check: verify.storeLocation,
    message: {
      invalid: Message.INVALID_STORE_LOCATION,
      required: Message.STORE_LOCATION_IS_REQUIRED,
    },
  },
  storeContact: {
    check: verify.mobilePhone,
    message: {
      invalid: Message.INVALID_STORE_CONTACT,
      required: Message.STORE_CONTACT_IS_REQUIRED,
    },
  },
  storeEmail: {
    check: verify.email,
    message: {
      invalid: Message.INVALID_STORE_EMAIL,
      required: Message.STORE_EMAIL_IS_REQUIRED,
    },
  },
  storeCloseTime: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_STORE_CLOSE_TIME,
      required: Message.STORE_CLOSE_TIME_IS_REQUIRED,
    },
  },
  storeOpenTime: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_STORE_OPEN_TIME,
      required: Message.STORE_OPEN_TIME_IS_REQUIRED,
    },
  },
  plateNumber: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_PLATE_NUMBER,
      required: Message.PLATE_NUMBER_IS_REQUIRED,
    },
  },
  ownerType: {
    check: verify.ownerType,
    message: {
      invalid: Message.INVALID_OWNER_TYPE,
      required: Message.OWNER_TYPE_IS_REQUIRED,
    },
  },
  colorCode: {
    check: verify.colorCode,
    message: {
      invalid: Message.INVALID_COLOR_CODE,
      required: Message.COLOR_CODE_IS_REQUIRED,
    },
  },
  files: {
    check: verify.files,
    message: {
      invalid: null,
      required: null,
    },
  },
  attributeName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_ATTRIBUTE_NAME,
      required: Message.ATTRIBUTE_NAME_IS_REQUIRED,
    },
  },
  attributeTypeName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_ATTRIBUTE_TYPE_NAME,
      required: Message.ATTRIBUTE_TYPE_NAME_IS_REQUIRED,
    },
  },
  brandName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_BRAND_NAME,
      required: Message.BRAND_NAME_IS_REQUIRED,
    },
  },
  modelName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_MODEL_NAME,
      required: Message.MODEL_NAME_IS_REQUIRED,
    },
  },
  variantName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_VARIANT_NAME,
      required: Message.VARIANT_NAME_IS_REQUIRED,
    },
  },
  stateName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_STATE_NAME,
      required: Message.STATE_NAME_IS_REQUIRED,
    },
  },
  countryName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_COUNTRY_NAME,
      required: Message.COUNTRY_NAME_IS_REQUIRED,
    },
  },
  stateIsoCode: {
    check: verify.isoCode,
    message: {
      invalid: Message.INVALID_STATE_ISO_CODE,
      required: Message.STATE_ISO_CODE_IS_REQUIRED,
    },
  },
  countryIsoCode: {
    check: verify.isoCode,
    message: {
      invalid: Message.INVALID_COUNTRY_ISO_CODE,
      required: Message.COUNTRY_ISO_CODE_IS_REQUIRED,
    },
  },
  requestStatus: {
    check: verify.requestStatus,
    message: {
      invalid: Message.INVALID_REQUEST_STATUS,
      required: Message.REQUEST_STATUS_IS_REQUIRED,
    },
  },
  approveStatus: {
    check: verify.approveStatus,
    message: {
      invalid: Message.INVALID_REQUEST_STATUS,
      required: Message.REQUEST_STATUS_IS_REQUIRED,
    },
  },

  //Garage Services Validator
  services: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_SERVICE_ID,
      required: Message.SERVICE_ID_IS_REQUIRED,
    },
  },
  serviceTypeId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_SERVICE_TYPE_ID,
      required: Message.SERVICE_TYPE_ID_IS_REQUIRED,
    },
  },

  // Garage Service Validator
  garageServicesId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_GARAGE_ID,
      required: Message.GARAGE_ID_IS_REQUIRED,
    },
  },
  // Garage Internal Service Validator
  serviceId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_SERVICE_NAME,
      required: Message.SERVICE_NAME_IS_REQUIRED,
    },
  },

  serviceNameId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_GARAGE_ID,
      required: Message.GARAGE_ID_IS_REQUIRED,
    },
  },

  servicePrice: {
    check: verify.price,
    message: {
      invalid: Message.INVALID_SERVICE_TYPE_ID,
      required: Message.SERVICE_TYPE_ID_IS_REQUIRED,
    },
  },

  // Garage Validator
  garageId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_GARAGE_ID,
      required: Message.GARAGE_ID_IS_REQUIRED,
    },
  },
  garageName: {
    check: verify.name,
    message: {
      invalid: Message.INVALID_GARAGE_NAME,
      required: Message.GARAGE_NAME_IS_REQUIRED,
    },
  },
  location: {
    check: verify.longText,
    message: {
      invalid: Message.INVALID_GARAGE_LOCATION,
      required: Message.GARAGE_LOCATION_IS_REQUIRED,
    },
  },
  garageContact: {
    check: verify.mobilePhone,
    message: {
      invalid: Message.INVALID_GARAGE_CONTACT,
      required: Message.GARAGE_CONTACT_IS_REQUIRED,
    },
  },
  garageEmail: {
    check: verify.email,
    message: {
      invalid: Message.INVALID_GARAGE_EMAIL,
      required: Message.GARAGE_EMAIL_IS_REQUIRED,
    },
  },
  garageCloseTime: {
    check: verify.time,
    message: {
      invalid: Message.INVALID_GARAGE_CLOSE_TIME,
      required: Message.GARAGE_CLOSE_TIME_IS_REQUIRED,
    },
  },
  garageOpenTime: {
    check: verify.time,
    message: {
      invalid: Message.INVALID_GARAGE_OPEN_TIME,
      required: Message.GARAGE_OPEN_TIME_IS_REQUIRED,
    },
  },
  description: {
    check: verify.longText,
    message: {
      invalid: Message.INVALID_DESCRIPTION,
      required: Message.DESCRIPTION_IS_REQUIRED,
    },
  },
  status: {
    check: verify.status,
    message: {
      invalid: Message.INVALID_STATUS,
      required: Message.STATUS_IS_REQUIRED,
    },
  },
  district: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_DISTRICT,
      required: Message.DISTRICT_IS_REQUIRED,
    },
  },
  area: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_AREA,
      required: Message.AREA_IS_REQUIRED,
    },
  },
  city: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_CITY,
      required: Message.CITY_IS_REQUIRED,
    },
  },
  road: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_ROAD,
      required: Message.ROAD_IS_REQUIRED,
    },
  },
  zipCode: {
    check: verify.zipCode,
    message: {
      invalid: Message.INVALID_ZIP_CODE,
      required: Message.ZIP_CODE_IS_REQUIRED,
    },
  },
  coordinates: {
    check: verify.array,
    message: {
      invalid: Message.INVALID_COORDINATES,
      required: Message.COORDINATES_IS_REQUIRED,
    },
  },
  storeIdArray: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_DEALER_STORE_ID,
      required: Message.DEALER_STORE_ID_IS_REQUIRED,
    },
  },
  flag: {
    check: verify.uniqueFlag,
    message: {
      invalid: Message.INVALID_FLAG,
      required: Message.FLAG_IS_REQUIRED,
    },
  },
  dealerId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_DEALER_ID,
      required: Message.DEALER_ID_IS_REQUIRED,
    },
  },
  role: {
    check: verify.shortText,
    message: {
      invalid: Message.INVALID_ROLE,
      required: Message.ROLE_IS_REQUIRED,
    },
  },
  roleId: {
    check: verify.mongoId,
    message: {
      invalid: Message.INVALID_ROLE,
      required: Message.ROLE_IS_REQUIRED,
    },
  },
  brandIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_BRAND_NAME,
      required: Message.BRAND_NAME_IS_REQUIRED,
    },
  },
  modelIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_MODEL_NAME,
      required: Message.MODEL_NAME_IS_REQUIRED,
    },
  },
  variantIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_VARIANT_NAME,
      required: Message.VARIANT_NAME_IS_REQUIRED,
    },
  },
  fuelTypeIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_FUEL_TYPE,
      required: Message.FUEL_TYPE_IS_REQUIRED,
    },
  },
  transmissionTypeIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_TRANSMISSION_TYPE,
      required: Message.TRANSMISSION_TYPE_IS_REQUIRED,
    },
  },
  bodyTypeIds: {
    check: verify.mongoIdArray,
    message: {
      invalid: Message.INVALID_BODY_TYPE,
      required: Message.BODY_TYPE_IS_REQUIRED,
    },
  },
  sid: {
    check: verify.sid,
    message: {
      invalid: Message.INVALID_SID,
      required: Message.SID_IS_REQUIRED,
    },
  },
  ownerTypeArray: {
    check: verify.ownerTypeArray,
    message: {
      invalid: Message.INVALID_OWNER_TYPE,
      required: Message.OWNER_TYPE_IS_REQUIRED,
    },
  },
};

export { check, Rule };
