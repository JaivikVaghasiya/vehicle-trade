import { Router } from 'express';
import { stateController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.get(
  '/list',
  check(query, 'countryId', Rule.countryId, { empty: true }),
  check(query, 'countryName', Rule.country, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await stateController.list({
      countryId: req.query.countryId,
      countryName: req.query.countryName,
    });
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/add',
  auth,
  check(body, 'countryId', Rule.countryId, { required: true }),
  check(body, 'stateIsoCode', Rule.stateIsoCode, { required: true }),
  check(body, 'stateName', Rule.stateName, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      countryId: req.body.countryId,
      stateIsoCode: req.body.stateIsoCode,
      stateName: req.body.stateName,
    };
    const { message, responseData }: any = await stateController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'stateId', Rule.stateId, { required: true }),
  check(body, 'countryId', Rule.countryId, { required: true }),
  check(body, 'stateIsoCode', Rule.stateIsoCode, { required: true }),
  check(body, 'stateName', Rule.stateName, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      countryId: req.body.countryId,
      stateIsoCode: req.body.stateIsoCode,
      stateName: req.body.stateName,
    };
    const { message, responseData }: any = await stateController.update(
      req.body.stateId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:stateId',
  auth,
  check(param, 'stateId', Rule.stateId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await stateController.delete(
      req.params.stateId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
