import { Router } from 'express';
import { countryController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param } from 'express-validator';

const router: Router = Router();

router.get(
  '/list',
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await countryController.list();
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/add',
  auth,
  check(body, 'countryIsoCode', Rule.countryIsoCode, { required: true }),
  check(body, 'countryName', Rule.countryName, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      countryName: req.body.countryName,
      countryIsoCode: req.body.countryIsoCode,
    };
    const { message, responseData }: any = await countryController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'countryId', Rule.countryId, { required: true }),
  check(body, 'countryIsoCode', Rule.countryIsoCode, { required: true }),
  check(body, 'countryName', Rule.countryName, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      countryName: req.body.countryName,
      countryIsoCode: req.body.countryIsoCode,
    };
    const { message, responseData }: any = await countryController.update(
      req.body.countryId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:countryId',
  auth,
  check(param, 'countryId', Rule.countryId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await countryController.delete(
      req.params.countryId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
