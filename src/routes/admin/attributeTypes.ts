import { Router } from 'express';
import { attributeTypeController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param } from 'express-validator';

const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'attributeTypeName', Rule.attributeTypeName, { required: true }),
  check(body, 'description', Rule.description, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      attributeTypeName: req.body.attributeTypeName,
      description: req.body.description,
    };
    const { message, responseData }: any = await attributeTypeController.add(
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await attributeTypeController.list();
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'attributeTypeId', Rule.attributeTypeId, { required: true }),
  check(body, 'attributeTypeName', Rule.attributeTypeName, { required: true }),
  check(body, 'description', Rule.description, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      attributeTypeName: req.body.attributeTypeName,
      description: req.body.description,
    };
    const { message, responseData }: any = await attributeTypeController.update(
      req.body.attributeTypeId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:attributeTypeId',
  auth,
  check(param, 'attributeTypeId', Rule.attributeTypeId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await attributeTypeController.delete(
      req.params.attributeTypeId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
