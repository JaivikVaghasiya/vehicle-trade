import { Router } from 'express';
import { modelController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: any = Router();

router.post(
  '/add',
  auth,
  check(body, 'modelName', Rule.modelName, { required: true }),
  check(body, 'brandId', Rule.brandId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      brandId: req.body.brandId,
      modelName: req.body.modelName,
    };
    const { message, responseData }: any = await modelController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/list',
  check(query, 'brandId', Rule.brandId, { empty: true }),
  check(body, 'page', Rule.page, { empty: true }),
  check(body, 'limit', Rule.perPage, { empty: true }),
  check(body, 'sortField', Rule.sortField, { empty: true }),
  check(body, 'sortType', Rule.sortType, { empty: true }),
  check(body, 'search', Rule.search, { empty: true }),
  check(body, 'isDropdown', Rule.isDropdown, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      brandId: req.body.brandId,
      page: req.body.page,
      limit: req.body.limit,
      sortField: req.body.sortField,
      sortType: req.body.sortType,
      search: req.body.search,
      flag: req.body.flag,
      isDropdown: req.body.isDropdown,
    };
    const { message, responseData }: any = await modelController.list(body);

    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'modelName', Rule.modelName, { required: true }),
  check(body, 'brandId', Rule.brandId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      brandId: req.body.brandId,
      modelName: req.body.modelName,
    };
    const { message, responseData }: any = await modelController.update(
      req.body.modelId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:modelId',
  auth,
  check(param, 'modelId', Rule.modelId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await modelController.delete(
      req.params.modelId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
