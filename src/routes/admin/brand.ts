import { Router } from 'express';
import { brandController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: any = Router();

router.post(
  '/add',
  auth,
  check(body, 'brandName', Rule.brandName, { required: true }),
  check(body, 'attributeId', Rule.attributeId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      brandName: req.body.brandName,
      attributeId: req.body.attributeId,
    };
    const { message, responseData }: any = await brandController.add(
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/list',
  check(body, 'flag', Rule.flag, { empty: true }),
  check(body, 'page', Rule.page, { empty: true }),
  check(body, 'limit', Rule.perPage, { empty: true }),
  check(body, 'sortField', Rule.sortField, { empty: true }),
  check(body, 'sortType', Rule.sortType, { empty: true }),
  check(body, 'search', Rule.search, { empty: true }),
  check(body, 'isDropdown', Rule.isDropdown, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      page: req.body.page,
      limit: req.body.limit,
      sortField: req.body.sortField,
      sortType: req.body.sortType,
      search: req.body.search,
      flag: req.body.flag,
      isDropdown: req.body.isDropdown,
    };
    const { message, responseData }: any = await brandController.list(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'brandId', Rule.brandId, { required: true }),
  check(body, 'brandName', Rule.brandName, { required: true }),
  check(body, 'attributeId', Rule.attributeId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      brandName: req.body.brandName,
      attributeId: req.body.attributeId,
    };
    const { message, responseData }: any = await brandController.update(
      req.body.brandId,
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:brandId',
  auth,
  check(param, 'brandId', Rule.brandId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await brandController.delete(
      req.params.brandId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
