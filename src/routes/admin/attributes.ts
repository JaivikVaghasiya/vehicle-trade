import { Router } from 'express';
import { attributeController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'attributeName', Rule.attributeName, { required: true }),
  check(body, 'attributeTypeId', Rule.attributeTypeId, { required: true }),
  check(body, 'description', Rule.description, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      attributeName: req.body.attributeName,
      attributeTypeId: req.body.attributeTypeId,
      description: req.body.description,
    };
    const { message, responseData }: any = await attributeController.add(
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/list',
  check(body, 'page', Rule.page, { empty: true }),
  check(body, 'limit', Rule.perPage, { empty: true }),
  check(body, 'sortField', Rule.sortField, { empty: true }),
  check(body, 'sortType', Rule.sortType, { empty: true }),
  check(body, 'search', Rule.search, { empty: true }),
  check(body, 'flag', Rule.flag, { empty: true }),
  check(body, 'isDropdown', Rule.isDropdown, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      page: req.body.page,
      limit: req.body.limit,
      sortField: req.body.sortField,
      sortType: req.body.sortType,
      search: req.body.search,
      flag: req.body.flag,
      isDropdown: req.body.isDropdown,
    };
    const { message, responseData }: any = await attributeController.list(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'attributeId', Rule.attributeId, { required: true }),
  check(body, 'attributeTypeId', Rule.attributeTypeId, { required: true }),
  check(body, 'attributeName', Rule.attributeName, { empty: true }),
  check(body, 'description', Rule.description, { empty: true }),
  asyncHandler(async function (req: any, res) {
    console.log('req.files :>> ', req.files);
    const body = {
      attributeName: req.body.attributeName,
      description: req.body.description,
    };
    const { message, responseData }: any = await attributeController.update(
      req.body.attributeId,
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:attributeId',
  auth,
  check(param, 'attributeId', Rule.attributeId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await attributeController.delete(
      req.params.attributeId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/updateStatus',
  auth,
  asyncHandler(async function (req: any, res) {
    const body = {
      attributeId: req.body.attributeId,
      isActive: req.body.isActive,
    };
    const { message, responseData }: any =
      await attributeController.updateStatus(body);
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
