import { Router } from 'express';
import { userRoleController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'role', Rule.role, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = { role: req.body.role };
    const { message, responseData }: any = await userRoleController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userRoleController.list();
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'roleId', Rule.roleId, { required: true }),
  check(body, 'role', Rule.role, { required: true }),
  asyncHandler(async function (req, res) {
    const body = {
      roleId: req.body.roleId,
      role: req.body.role,
    };
    const { message, responseData }: any = await userRoleController.update(
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:roleId',
  auth,
  check(param, 'commentId', Rule.commentId, { required: true }),
  asyncHandler(async function (req, res) {
    const { message, responseData }: any = await userRoleController.delete(
      req.params.roleId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
