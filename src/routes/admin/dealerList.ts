import express from 'express';
import { dealerListController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body } from 'express-validator';
const router: any = express.Router();

router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await dealerListController.list(
      req.query,
      req.authUser._id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'dealerId', Rule.dealerId, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      isActive: req.body.isActive,
      dealerId: req.body.dealerId,
    };
    const { message, responseData }: any = await dealerListController.update(
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
