import { Router } from 'express';
import { adminController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, query } from 'express-validator';
const router: Router = Router();

router.get(
  '/vehicle/status',
  auth,
  check(query, 'vehicleStatus', Rule.requestStatus, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await adminController.vehicleRequests(req.query.vehicleStatus);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/dealer/status/list',
  auth,
  check(query, 'dealerStatus', Rule.requestStatus, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await adminController.dealerRequests(
      req.query.dealerStatus,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/location/by/address',
  check(query, 'latitude', Rule.latitude, { required: true }),
  check(query, 'longitude', Rule.longitude, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await adminController.addressByLocation(
        req.query.latitude,
        req.query.longitude,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/vehicle/status/update',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'vehicleStatus', Rule.approveStatus, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      vehicleId: req.body.vehicleId,
      vehicleStatus: req.body.vehicleStatus,
    };
    const { message, responseData }: any =
      await adminController.updateVehicleStatus(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/dealer/status/update',
  auth,
  check(body, 'userId', Rule.userId, { required: true }),
  check(body, 'dealerStatus', Rule.approveStatus, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      userId: req.body.userId,
      dealerStatus: req.body.dealerStatus,
    };
    const { message, responseData }: any =
      await adminController.updateDealerStatus(body);
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
