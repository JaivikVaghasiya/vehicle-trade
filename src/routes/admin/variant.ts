import { Router } from 'express';
import { variantController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'variantName', Rule.variantName, { required: true }),
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'brandId', Rule.brandId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      variantName: req.body.variantName,
      modelId: req.body.modelId,
      brandId: req.body.brandId,
    };
    const { message, responseData }: any = await variantController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/list',
  check(body, 'modelId', Rule.modelId, { empty: true }),
  check(body, 'page', Rule.page, { empty: true }),
  check(body, 'limit', Rule.perPage, { empty: true }),
  check(body, 'sortField', Rule.sortField, { empty: true }),
  check(body, 'sortType', Rule.sortType, { empty: true }),
  check(body, 'search', Rule.search, { empty: true }),
  check(body, 'isDropdown', Rule.isDropdown, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body ={
      modelId:req.body.modelId,
      page:req.body.page,
      limit:req.body.limit,
      sortField:req.body.sortField,
      sortType:req.body.sortType,
      search:req.body.search,
      isDropdown:req.body.isDropdown,
    };
    const { message, responseData }: any = await variantController.list(
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'variantId', Rule.variantId, { required: true }),
  check(body, 'variantName', Rule.variantName, { required: true }),
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'brandId', Rule.brandId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      variantName: req.body.variantName,
      modelId: req.body.modelId,
      brandId: req.body.brandId,
    };
    const { message, responseData }: any = await variantController.update(
      req.body.variantId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:variantId',
  auth,
  check(param, 'variantId', Rule.variantId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await variantController.delete(
      req.params.variantId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
