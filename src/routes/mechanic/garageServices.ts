// import express from 'express';
// import { garageServicesController } from '../../controller';
// import {
//   asyncHandler,
//   authenticateUser as auth,
// } from '../../middleware/middleware';
// import { check, logger, Rule, sendSuccess } from '../../utils';
// import { body, param, query } from 'express-validator';
// const router: any = express.Router();

// router.post(
//   '/add',
//   auth,
//   check(body, 'serviceTypeId', Rule.serviceTypeId, { required: true }),
//   check(body, 'garageId', Rule.garageId, { required: true }),
//   check(body, 'services.*.serviceId', Rule.serviceId, { required: true }),
//   check(body, 'services.*.price', Rule.price, { required: true }),
//   asyncHandler(async function (req: any, res) {
//     let body = {
//       garageId: req.body.garageId,
//       services: req.body.services,
//       serviceTypeId: req.body.serviceTypeId,
//     };
//     const { message, responseData }: any = await garageServicesController.add(
//       body,
//     );
//     return sendSuccess(res, message, responseData);
//   }),
// );

// router.get(
//   '/list',
//   auth,
//   check(query, 'garageId', Rule.garageId, { required: true }),
//   check(query, 'serviceType', Rule.serviceTypeId, { empty: true }),
//   asyncHandler(async function (req: any, res) {
//     const query = {
//       garageId: req.query.garageId,
//       serviceType: req.query.serviceType,
//     };
//     const { message, responseData }: any = await garageServicesController.list(
//       query,
//     );
//     return sendSuccess(res, message, responseData);
//   }),
// );

// router.put(
//   '/update/:garageServicesId',
//   auth,
//   check(param, 'garageServicesId', Rule.serviceId, { required: true }),
//   check(body, 'serviceTypeId', Rule.serviceTypeId, { required: true }),
//   check(body, 'garageId', Rule.garageId, { required: true }),
//   check(body, 'services.*.serviceId', Rule.serviceId, { required: true }),
//   check(body, 'services.*.price', Rule.price, { required: true }),
//   asyncHandler(async function (req: any, res) {
//     let body = {
//       garageId: req.body.garageId,
//       services: req.body.services,
//       serviceTypeId: req.body.serviceTypeId,
//     };
//     const { message, responseData }: any =
//       await garageServicesController.update(req.params.garageServicesId, body);
//     return sendSuccess(res, message, responseData);
//   }),
// );

// router.delete(
//   '/delete',
//   auth,
//   check(query, 'garageServicesId', Rule.garageServicesId, { required: true }),
//   check(query, 'serviceId', Rule.serviceId, { required: true }),
//   asyncHandler(async function (req: any, res) {
//     const payload: any = {
//       garageServicesId: req.query.garageServicesId,
//       serviceId: req.query.serviceId,
//     };
//     const { message, responseData }: any =
//       await garageServicesController.delete(payload);
//     return sendSuccess(res, message, responseData);
//   }),
// );

// router.put(
//   '/status/update',
//   auth,
//   check(body, 'garageServicesId', Rule.garageServicesId, { required: true }),
//   check(body, 'isActive', Rule.status, { required: true }),
//   check(body, 'serviceId', Rule.serviceId, { required: true }),
//   asyncHandler(async function (req: any, res) {
//     const payload = {
//       garageServicesId: req.body.garageServicesId,
//       isActive: req.body.isActive,
//       serviceId: req.body.serviceId,
//     };
//     const { message, responseData }: any =
//       await garageServicesController.activeInactiveService(payload);
//     return sendSuccess(res, message, responseData);
//   }),
// );

// export default router;
