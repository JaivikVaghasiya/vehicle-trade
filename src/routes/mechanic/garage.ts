import express from 'express';
import { garageController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, logger, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: any = express.Router();


router.post(
  '/add',
  auth,
  check(body, 'employeeName', Rule.employeeName, { empty: true }),
  check(body, 'garageName', Rule.garageName, { required: true }),
  check(body, 'location.address', Rule.address, { required: true }),
  check(body, 'location.countryCode', Rule.countryIsoCode, { required: true }),
  check(body, 'location.country', Rule.country, { required: true }),
  check(body, 'location.state', Rule.state, { required: true }),
  check(body, 'location.district', Rule.district, { required: true }),
  check(body, 'location.area', Rule.area, { required: false }),
  check(body, 'location.city', Rule.city, { required: false }),
  check(body, 'location.road', Rule.road, { required: false }),
  check(body, 'location.postCode', Rule.zipCode, { required: true }),
  check(body, 'location.coordinates', Rule.coordinates, { required: true }),
  check(body, 'garageContact', Rule.garageContact, { required: true }),
  check(body, 'garageEmail', Rule.garageEmail, { required: true }),
  check(body, 'garageCloseTime', Rule.garageCloseTime, { required: true }),
  check(body, 'garageOpenTime', Rule.garageOpenTime, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      employeeName: req.body.employeeName,
      garageName: req.body.garageName,
      location: {
        address: req.body.location.address,
        country: req.body.location.country,
        countryCode: req.body.location.countryCode,
        state: req.body.location.state,
        district: req.body.location.district,
        area: req.body.location.area,
        city: req.body.location.city,
        road: req.body.location.road,
        postCode: req.body.location.postCode,
        coordinates: req.body.location.coordinates,
      },
      garageContact: req.body.garageContact,
      garageEmail: req.body.garageEmail,
      garageOpenTime: req.body.garageOpenTime,
      garageCloseTime: req.body.garageCloseTime,
      mechanicId: req.authUser._id,
    };
    const { message, responseData }: any = await garageController.add(body);
    console.log('message, :>> ', message);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await garageController.list(
      req.authUser._id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update/:garageId',
  auth,
  check(param, 'garageId', Rule.garageId, { required: true }),
  check(body, 'employeeName', Rule.employeeName, { empty: true }),
  check(body, 'garageName', Rule.garageName, { required: true }),
  check(body, 'location.address', Rule.address, { required: true }),
  check(body, 'location.countryCode', Rule.countryIsoCode, { required: true }),
  check(body, 'location.country', Rule.country, { required: true }),
  check(body, 'location.state', Rule.state, { required: true }),
  check(body, 'location.district', Rule.district, { required: true }),
  check(body, 'location.area', Rule.area, { required: false }),
  check(body, 'location.city', Rule.city, { required: false }),
  check(body, 'location.road', Rule.road, { required: false }),
  check(body, 'location.postCode', Rule.zipCode, { required: true }),
  check(body, 'location.coordinates', Rule.coordinates, { required: true }),
  check(body, 'garageContact', Rule.garageContact, { required: true }),
  check(body, 'garageEmail', Rule.garageEmail, { required: true }),
  check(body, 'garageCloseTime', Rule.garageCloseTime, { required: true }),
  check(body, 'garageOpenTime', Rule.garageOpenTime, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      employeeName: req.body.employeeName,
      garageName: req.body.garageName,
      location: {
        address: req.body.location.address,
        country: req.body.location.country,
        countryCode: req.body.location.countryCode,
        state: req.body.location.state,
        district: req.body.location.district,
        area: req.body.location.area,
        city: req.body.location.city,
        road: req.body.location.road,
        postCode: req.body.location.postCode,
        coordinates: req.body.location.coordinates,
      },
      garageContact: req.body.garageContact,
      garageEmail: req.body.garageEmail,
      garageOpenTime: req.body.garageOpenTime,
      garageCloseTime: req.body.garageCloseTime,
      mechanicId: req.authUser._id,
    };
    const { message, responseData }: any = await garageController.update(
      req.params.garageId,
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:garageId',
  auth,
  check(param, 'garageId', Rule.garageId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await garageController.delete(
      req.params.garageId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
