import { Router } from 'express';
const router: Router = Router();
import brandRoutes from './admin/brand';
import modelRoutes from './admin/model';
import variantRoutes from './admin/variant';
import attributeRoutes from './admin/attributes';
import attributeTypeRotes from './admin/attributeTypes';
import dealerStore from './dealer/dealerStore';
import vehicleRoutes from './dealer/vehicle';
import userRoutes from './user/user';
import homeScreenRoutes from './user/home';
import adminRoutes from './admin/admin';
import favoriteRoutes from './user/favorite';
import commentsRoutes from './user/comment';
import userInquiryRotes from './user/userInquiry';
import dealerDashboardRotes from './dealer/dealerDashboard';
import dealerRoutes from './dealer/dealer';
import countryRoutes from './admin/country';
import stateRoutes from './admin/state';
import garageRoutes from './mechanic/garage';
// import garageServicesRoutes from './mechanic/garageServices';
import dealerActivityRouters from './dealer/userActivity';
import manualInquiryRoutes from './dealer/manualInquiry';
import dealerListRoutes from './admin/dealerList';
import userRoleRoutes from './admin/role';

// admin routes
router.use('/admin', adminRoutes);
router.use('/admin/brand', brandRoutes);
router.use('/admin/model', modelRoutes);
router.use('/admin/variant', variantRoutes);
router.use('/admin/attribute', attributeRoutes);
router.use('/admin/attribute-type', attributeTypeRotes);
router.use('/admin/country', countryRoutes);
router.use('/admin/state', stateRoutes);
router.use('/admin/role', userRoleRoutes);
// dealer routes
router.use('/dealer', dealerRoutes);
router.use('/dealer/store', dealerStore);
router.use('/dealer/vehicle', vehicleRoutes);
router.use('/dealer/dashboard', dealerDashboardRotes);
router.use('/dealer/activity', dealerActivityRouters);

// user routes
router.use('/user', userRoutes);
router.use('/user/vehicle', homeScreenRoutes);
router.use('/user/favorite', favoriteRoutes);
router.use('/user/inquiry', userInquiryRotes);
router.use('/user/comment', commentsRoutes);
router.use('/user/manual/inquiry', manualInquiryRoutes);
router.use('/admin/dealer/', dealerListRoutes);

// mechanic routes
router.use('/mechanic/garage', garageRoutes);
// router.use('/mechanic/garage/service', garageServicesRoutes);

export default router;
