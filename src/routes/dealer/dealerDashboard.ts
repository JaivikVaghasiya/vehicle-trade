import { Router } from 'express';
import { dealerDashboardController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.get(
  '/weekly/sell',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.weeklySell(req.authUser._id, req.query);

    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/weekly/sell/store',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.weeklySellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/daily/sell/store',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.dailySellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/monthly/sell/store',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.monthlySellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/day/sell',
  auth,
  check(query, 'startOfDay', Rule.startTime, { empty: true }),
  check(query, 'endOfDay', Rule.endTime, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.daySell(req.authUser._id, req.query);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/monthly/day',
  auth,
  // check(query, 'startOfMonth', Rule.startTime, { empty: true }),
  // check(query, 'endOfMonth', Rule.endTime, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.monthlyDayStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/weekly/day',
  auth,
  // check(query, 'startOfWeek', Rule.startTime, { empty: true }),
  // check(query, 'endOfWeek', Rule.endTime, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.weeklyDayStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/yearly/sell',
  auth,
  // check(query, 'startOfYear', Rule.startTime, { empty: true }),
   check(query, 'endOfYear', Rule.endTime, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.yearlySell(req.authUser._id, req.query);

    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/all/Current/Sell/Count',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.allCurrentSellCount(req.authUser._id);
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/inquiry/count',
  auth,
  check(body, 'brandId', Rule.brandName, { empty: true }),
  check(body, 'modelId', Rule.modelName, { empty: true }),
  check(body, 'variantId', Rule.variantName, { empty: true }),
  check(body, 'fuelTypeId', Rule.fuelTypeId, { empty: true }),
  check(body, 'bodyTypeId', Rule.bodyTypeId, { empty: true }),
  check(body, 'transmissionTypeId', Rule.transmissionTypeId, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const body = { storeId: req.body.storeId };
    const filter = {
      'vehicleData.brandId': req.body.brandId,
      'vehicleData.modelId': req.body.modelId,
      'vehicleData.variantId': req.body.variantId,
      'vehicleDetails.fuelType': req.body.fuelTypeId,
      'vehicleDetails.transmissionType': req.body.transmissionTypeId,
      'vehicleDetails.bodyType': req.body.bodyTypeId,
    };
    const { message, responseData }: any =
      await dealerDashboardController.inquiryCountByFilter(
        req.authUser._id,
        body,
        filter,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/month/sell/store',
  auth,
  check(query, 'firstOfMonth', Rule.startTime, { empty: true }),
  check(query, 'lastOfMonth', Rule.endTime, { empty: true }),
  check(query, 'storeId', Rule.dealerStoreId, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.monthSellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/all/brand/inquiry/count',
  auth,
  check(body, 'storeIds', Rule.storeIdArray, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.allBrandInquiryCount(
        req.authUser._id,
        req.body.storeIds,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/year/sell/store',
  auth,
  check(query, 'startOfYear', Rule.startTime, { empty: true }),
  check(query, 'endOfYear', Rule.endTime, { empty: true }),
  check(query, 'storeId', Rule.dealerStoreId, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.yearSellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/week/sell/store',
  auth,
  check(query, 'startOfWeek', Rule.startTime, { empty: true }),
  check(query, 'endOfWeek', Rule.endTime, { empty: true }),
  check(query, 'storeId', Rule.dealerStoreId, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerDashboardController.weekSellStore(
        req.authUser._id,
        req.query,
      );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
