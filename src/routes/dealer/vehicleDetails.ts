// import {
//   asyncHandler,
//   authenticateUser as auth,
// } from '../../middleware/middleware';
// import { check, Rule, sendSuccess } from '../../utils';
// import { body, param } from 'express-validator';
// import express from 'express';
// const router: any = express.Router();
// import { vehicleDetailsController } from './../../controller';

// router.post(
//   '/add',
//   auth,
//   check(body, 'vehicleId', Rule.vehicleId, { required: true }),
//   check(body, 'fuelType', Rule.fuelTypeId, { required: true }),
//   check(body, 'transmissionType', Rule.transmissionTypeId, { required: true }),
//   check(body, 'bodyType', Rule.bodyTypeId, { required: true }),
//   check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
//   check(body, 'file', Rule.files, {
//     file: true,
//     required: false,
//     keys: ['policyImage', 'RCbookImage'],
//   }),
//   asyncHandler(async function (req: any, res) {
//     let body = {
//       vehicleId: req.body.vehicleId,
//       fuelType: req.body.fuelType,
//       transmissionType: req.body.transmissionType,
//       bodyType: req.body.bodyType,
//       dealerStoreId: req.body.dealerStoreId,
//     };
//     const { message, responseData }: any = await vehicleDetailsController.add(
//       body,
//       req.files,
//     );
//     return sendSuccess(res, message, responseData);
//   }),
// );

// router.put(
//   '/update/:vehicleDetailsId',
//   auth,
//   auth,
//   check(param, 'vehicleDetailsId', Rule.vehicleDetailsId, { required: true }),
//   check(body, 'vehicleId', Rule.vehicleId, { required: true }),
//   check(body, 'fuelType', Rule.fuelTypeId, { required: true }),
//   check(body, 'transmissionType', Rule.transmissionTypeId, { required: true }),
//   check(body, 'bodyType', Rule.bodyTypeId, { required: true }),
//   check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
//   check(body, 'file', Rule.files, {
//     file: true,
//     required: false,
//     keys: ['policyImage', 'RCbookImage'],
//   }),
//   asyncHandler(async function (req: any, res) {
//     let body = {
//       vehicleId: req.body.vehicleId,
//       fuelType: req.body.fuelType,
//       transmissionType: req.body.transmissionType,
//       bodyType: req.body.bodyType,
//       dealerStoreId: req.body.dealerStoreId,
//     };
//     const { message, responseData }: any =
//       await vehicleDetailsController.update(
//         req.params.vehicleDetailsId,
//         body,
//         req.files,
//       );
//     return sendSuccess(res, message, responseData);
//   }),
// );

// export default router;
