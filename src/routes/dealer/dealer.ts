import { Router } from 'express';
import { dealerController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, logger, Rule, sendSuccess } from '../../utils';
import { body, query } from 'express-validator';
const router: Router = Router();

router.get(
  '/sold',
  auth,
  check(query, 'page', Rule.page, { empty: true }),
  check(query, 'limit', Rule.perPage, { empty: true }),
  check(query, 'sortField', Rule.sortField, { empty: true }),
  check(query, 'sortType', Rule.sortType, { empty: true }),
  check(query, 'search', Rule.search, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await dealerController.soldVehicles(
      req.authUser._id,
      req.query,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.get(
  '/on-sell',
  auth,
  check(query, 'page', Rule.page, { empty: true }),
  check(query, 'limit', Rule.perPage, { empty: true }),
  check(query, 'sortField', Rule.sortField, { empty: true }),
  check(query, 'sortType', Rule.sortType, { empty: true }),
  check(query, 'search', Rule.search, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await dealerController.remainingVehicles(req.authUser._id, req.query);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/user/inquiry',
  auth,
  check(query, 'page', Rule.page, { empty: true }),
  check(query, 'limit', Rule.perPage, { empty: true }),
  check(query, 'sortField', Rule.sortField, { empty: true }),
  check(query, 'sortType', Rule.sortType, { empty: true }),
  check(query, 'search', Rule.search, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await dealerController.userInquiries(
      req.authUser._id,
      req.query,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/approve/user/inquiry',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'userId', Rule.userId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      userId: req.body.userId,
      vehicleId: req.body.vehicleId,
    };
    const { message, responseData }: any =
      await dealerController.approveUserInquiries(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/refuse/user/inquiry',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'userId', Rule.userId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      userId: req.body.userId,
      vehicleId: req.body.vehicleId,
    };
    const { message, responseData }: any =
      await dealerController.refuseUserInquiries(body);
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
