import express from 'express';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: any = express.Router();
import { vehicleController } from './../../controller';

router.post(
  '/add',
  auth,
  check(body, 'brandId', Rule.brandId, { required: true }),
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'variantId', Rule.variantId, { required: true }),
  check(body, 'attributeId', Rule.vehicleType, { required: true }),
  check(body, 'plateNumber', Rule.plateNumber, { required: true }),
  check(body, 'ownerType', Rule.ownerType, { required: true }),
  check(body, 'manufactureYear', Rule.year, { required: true }),
  check(body, 'colorCode', Rule.colorCode, { required: true }),
  check(body, 'kiloMeters', Rule.kiloMeter, { required: true }),
  check(body, 'price', Rule.price, { required: true }),
  check(body, 'file', Rule.files, {
    file: true,
    required: true,
    keys: ['mainImage', 'interiorImages', 'exteriorImages'],
  }),
  check(body, 'fuelType', Rule.fuelTypeId, { required: true }),
  check(body, 'transmissionType', Rule.transmissionTypeId, { required: true }),
  check(body, 'bodyType', Rule.bodyTypeId, { required: true }),
  check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
  check(body, 'file', Rule.files, {
    file: true,
    required: false,
    keys: ['policyImage', 'RCbookImage'],
  }),
  asyncHandler(async function (req: any, res) {
    const vehicleBody = {
      brandId: req.body.brandId,
      modelId: req.body.modelId,
      attributeId: req.body.attributeId,
      plateNumber: req.body.plateNumber,
      kiloMeters: req.body.kiloMeters,
      variantId: req.body.variantId,
      ownerType: req.body.ownerType,
      price: req.body.price,
      manufactureYear: req.body.manufactureYear,
      colorCode: req.body.colorCode,
    };
    const vehicleDetailsBody = {
      fuelType: req.body.fuelType,
      transmissionType: req.body.transmissionType,
      bodyType: req.body.bodyType,
      dealerStoreId: req.body.dealerStoreId,
    };
    const { message, responseData }: any = await vehicleController.add(
      vehicleBody,
      vehicleDetailsBody,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.get(
  '/list',
  auth,
  check(query, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await vehicleController.list(
      req.query.vehicleId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/edit',
  auth,
  check(query, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await vehicleController.edit(
      req.query.vehicleId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'brandId', Rule.brandId, { required: true }),
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'variantId', Rule.variantId, { required: true }),
  check(body, 'attributeId', Rule.vehicleType, { required: true }),
  check(body, 'plateNumber', Rule.plateNumber, { required: true }),
  check(body, 'ownerType', Rule.ownerType, { required: true }),
  check(body, 'manufactureYear', Rule.year, { required: true }),
  check(body, 'colorCode', Rule.colorCode, { required: true }),
  check(body, 'kiloMeters', Rule.kiloMeter, { required: true }),
  check(body, 'price', Rule.price, { required: true }),
  check(body, 'file', Rule.files, {
    file: true,
    required: false,
    keys: ['mainImage', 'interiorImages', 'exteriorImages'],
  }),
  check(body, 'fuelType', Rule.fuelTypeId, { required: true }),
  check(body, 'transmissionType', Rule.transmissionTypeId, { required: true }),
  check(body, 'bodyType', Rule.bodyTypeId, { required: true }),
  check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
  check(body, 'mainImageId', Rule.imageId, { empty: true }),
  check(body, 'file', Rule.files, {
    file: true,
    required: false,
    keys: ['policyImage', 'RCbookImage'],
  }),
  asyncHandler(async function (req: any, res) {
    const vehicleBody = {
      brandId: req.body.brandId,
      modelId: req.body.modelId,
      attributeId: req.body.attributeId,
      plateNumber: req.body.plateNumber,
      kiloMeters: req.body.kiloMeters,
      variantId: req.body.variantId,
      ownerType: req.body.ownerType,
      price: req.body.price,
      manufactureYear: req.body.manufactureYear,
      colorCode: req.body.colorCode,
      mainImageId: req.body.mainImageId,
    };
    const vehicleDetailsBody = {
      fuelType: req.body.fuelType,
      transmissionType: req.body.transmissionType,
      bodyType: req.body.bodyType,
      dealerStoreId: req.body.dealerStoreId,
    };
    const { message, responseData }: any = await vehicleController.update(
      req.body.vehicleId,
      vehicleBody,
      vehicleDetailsBody,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.delete(
  '/delete/:vehicleId',
  auth,
  check(param, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await vehicleController.delete(
      req.params.vehicleId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/image/:imageId',
  auth,
  check(param, 'imageId', Rule.imageId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await vehicleController.deleteImage(
      req.params.imageId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
