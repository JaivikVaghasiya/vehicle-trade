import { Router } from 'express';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, query } from 'express-validator';
import { manualInquiryController } from '../../controller/dealer/manualInquiry';
const router: Router = Router();

router.post(
  '/list',
  auth,
  check(body, 'limit', Rule.perPage, { empty: true }),
  check(body, 'sortField', Rule.sortField, { empty: true }),
  check(body, 'page', Rule.page, { empty: true }),
  check(body, 'sortType', Rule.sortType, { empty: true }),
  check(body, 'search', Rule.search, { empty: true }),
  check(body, 'brandId', Rule.brandIds, { empty: true }),
  check(body, 'modelId', Rule.modelIds, { empty: true }),
  check(body, 'fuelTypeId', Rule.fuelTypeIds, { empty: true }),
  check(body, 'variantId', Rule.variantIds, { empty: true }),
  check(body, 'transmissionTypeId', Rule.transmissionTypeIds, { empty: true }),
  check(body, 'bodyTypeId', Rule.bodyTypeIds, { empty: true }),
  check(body, 'ownerType', Rule.ownerTypeArray, { empty: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      minBudget: req.body.minBudget,
      maxBudget: req.body.maxBudget,
      page: req.body.page,
      limit: req.body.limit,
      sortField: req.body.sortField,
      sortType: req.body.sortType,
      search: req.body.search,
    };

    let filter = {
      brandId: req.body.brandId,
      modelId: req.body.modelId,
      fuelType: req.body.fuelTypeId,
      transmissionType: req.body.transmissionTypeId,
      bodyType: req.body.bodyTypeId,
      variantId: req.body.variantId,
      manufactureYear: req.body.manufactureYear,
      ownerType: req.body.ownerType,
    };
    const { message, responseData }: any = await manualInquiryController.list(
      req.authUser_id,
      body,
      filter,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/add',
  auth,
  check(body, 'brandId', Rule.brandId, { required: true }),
  check(body, 'modelId', Rule.modelId, { required: true }),
  check(body, 'userId', Rule.userId, { empty: true }),
  check(body, 'fuelTypeId', Rule.fuelTypeId, { required: true }),
  check(body, 'variantId', Rule.variantId, { required: true }),
  check(body, 'transmissionTypeId', Rule.transmissionTypeId),
  check(body, 'location.address', Rule.address, { required: true }),
  check(body, 'location.countryCode', Rule.countryIsoCode, { required: true }),
  check(body, 'location.country', Rule.country, { required: true }),
  check(body, 'location.state', Rule.state, { required: true }),
  check(body, 'location.district', Rule.district, { required: true }),
  check(body, 'location.area', Rule.area, { required: false }),
  check(body, 'location.city', Rule.city, { required: false }),
  check(body, 'location.road', Rule.road, { required: false }),
  check(body, 'location.postCode', Rule.zipCode, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body: any = {
      brandId: req.body.brandId,
      modelId: req.body.modelId,
      userId: req.body.userId,
      fuelTypeId: req.body.fuelTypeId,
      transmissionTypeId: req.body.transmissionTypeId,
      variantId: req.body.variantId,
      isDelivered: req.body.isDelivered,
      priceRange: req.body.priceRange,
      ownerType: req.body.ownerType,
      manufactureYear: req.body.manufactureYear,
      location: {
        address: req.body.location.address,
        country: req.body.location.country,
        countryCode: req.body.location.countryCode,
        state: req.body.location.state,
        district: req.body.location.district,
        area: req.body.location.area,
        city: req.body.location.city,
        road: req.body.location.road,
        postCode: req.body.location.postCode,
      },
    };
    const { message, responseData }: any = await manualInquiryController.add(
      body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
