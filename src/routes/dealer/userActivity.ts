import { Router } from 'express';
import { userActivityController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      vehicleId: req.body.vehicleId,
      dealerStoreId: req.body.dealerStoreId,
      userId: req.authUser._id,
    };
    const { message, responseData = null }: any =
      await userActivityController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  auth,
  check(query, 'page', Rule.page, { empty: true }),
  check(query, 'limit', Rule.perPage, { empty: true }),
  check(query, 'sortField', Rule.sortField, { empty: true }),
  check(query, 'sortType', Rule.sortType, { empty: true }),
  check(query, 'search', Rule.search, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userActivityController.list(
      req.authUser._id,
      req.query,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:id',
  auth,
  check(param, 'id', Rule.activityId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData = null }: any =
      await userActivityController.delete(req.params.id);
    return sendSuccess(res, message, responseData);
  }),
);
export default router;
