import express from 'express';
import { dealerStoreController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, logger, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: any = express.Router();

router.post(
  '/add',
  auth,
  check(body, 'employeeName', Rule.employeeName, { empty: true }),
  check(body, 'storeName', Rule.storeName, { required: true }),
  check(body, 'storeLocation', Rule.storeLocation, { required: true }),
  check(body, 'storeContact', Rule.storeContact, { required: true }),
  check(body, 'storeEmail', Rule.storeEmail, { required: true }),
  check(body, 'storeCloseTime', Rule.storeCloseTime, { required: true }),
  check(body, 'storeOpenTime', Rule.storeOpenTime, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      employeeName: req.body.employeeName,
      storeName: req.body.storeName,
      storeLocation: JSON.parse(req.body.storeLocation),
      storeContact: req.body.storeContact,
      storeEmail: req.body.storeEmail,
      storeOpenTime: req.body.storeOpenTime,
      storeCloseTime: req.body.storeCloseTime,
      dealerId: req.authUser._id,
    };
    const { message, responseData }: any = await dealerStoreController.add(
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await dealerStoreController.list(
      req.authUser._id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update/:storeId',
  auth,
  check(param, 'storeId', Rule.dealerStoreId, { required: true }),
  check(body, 'employeeName', Rule.employeeName, { empty: true }),
  check(body, 'storeName', Rule.storeName, { required: true }),
  check(body, 'storeLocation', Rule.storeLocation, { required: true }),
  check(body, 'storeContact', Rule.storeContact, { required: true }),
  check(body, 'storeEmail', Rule.storeEmail, { required: true }),
  check(body, 'storeCloseTime', Rule.storeCloseTime, { required: true }),
  check(body, 'storeOpenTime', Rule.storeOpenTime, { required: true }),
  asyncHandler(async function (req: any, res) {
    let body = {
      employeeName: req.body.employeeName,
      storeName: req.body.storeName,
      storeLocation: JSON.parse(req.body.storeLocation),
      storeContact: req.body.storeContact,
      storeEmail: req.body.storeEmail,
      storeOpenTime: req.body.storeOpenTime,
      storeCloseTime: req.body.storeCloseTime,
      dealerId: req.authUser._id,
    };
    const { message, responseData }: any = await dealerStoreController.update(
      req.params.storeId,
      body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:storeId',
  auth,
  check(param, 'storeId', Rule.dealerStoreId, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await dealerStoreController.delete(
      req.params.storeId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
