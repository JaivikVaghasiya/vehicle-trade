import { Router } from 'express';
import { userInquiry } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param } from 'express-validator';
const router: Router = Router();

router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData = null }: any = await userInquiry.list(
      req.authUser._id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/add',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'dealerStoreId', Rule.dealerStoreId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      vehicleId: req.body.vehicleId,
      dealerStoreId: req.body.dealerStoreId,
      userId: req.authUser._id,
    };
    const { message, responseData = null }: any = await userInquiry.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:id',
  auth,
  check(param, 'id', Rule.dealerStoreId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData = null }: any = await userInquiry.delete(
      req.params.id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
