import { Router } from 'express';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param } from 'express-validator';
import { userController } from '../../controller/user/user';
const router: Router = Router();

router.post(
  '/otp',
  check(body, 'mobileNo', Rule.mobilePhone, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userController.sendOtp(
      req.body.mobileNo,
    );
    return sendSuccess(res, message, responseData);
  }),
);
router.post(
  '/signup',
  check(body, 'firstName', Rule.firstName, { required: true }),
  check(body, 'lastName', Rule.lastName, { required: true }),
  check(body, 'email', Rule.email, { required: true }),
  check(body, 'mobileNo', Rule.mobilePhone, { required: true }),
  check(body, 'role', Rule.type, { required: true }),
  check(body, 'password', Rule.password, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userController.signUp(
      req.body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/login',
  check(body, 'mobileNo', Rule.mobilePhone, { empty: true }),
  check(body, 'email', Rule.email, { empty: true }),
  check(body, 'password', Rule.unknown, { empty: true }),
  check(body, 'otp', Rule.otp, { empty: true }),
  check(body, 'sid', Rule.unknown, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userController.logIn(req.body);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await userController.list(
      req.authUser,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update/:userId',
  auth,
  check(param, 'userId', Rule.userId, { required: true }),
  check(body, 'firstName', Rule.firstName, { required: true }),
  check(body, 'lastName', Rule.lastName, { required: true }),
  check(body, 'email', Rule.email, { required: true }),
  check(body, 'mobileNo', Rule.mobilePhone, { required: true }),
  check(body, 'role', Rule.type, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const userId = req.params.userId;
    const { message, responseData = null }: any = await userController.update(
      userId,
      req.body,
      req.files,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:userId',
  auth,
  check(param, 'userId', Rule.userId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const userId = req.params.userId;
    const { message, responseData }: any = await userController.delete(userId);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/edit',
  auth,
  check(param, 'userId', Rule.userId, { required: true }),
  asyncHandler(async function (req: any, res, next?) {
    const userId = req.query.userId;
    const { message, responseData }: any = await userController.edit(userId);
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/forget/password',
  check(body, 'email', Rule.email, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      email: req.body.email,
    };
    const { message, responseData } = await userController.forgetPassword(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/reset/password',
  asyncHandler(async function (req: any, res) {
    const body = {
      password: req.body.password,
      token: req.body.token,
    };
    const { message } = await userController.resetPassword(body);
    return sendSuccess(res, message);
  }),
);

router.post(
  '/verify/otp',
  check(body, 'mobileNo', Rule.mobilePhone, { required: true }),
  check(body, 'otp', Rule.otp, { required: true }),
  check(body, 'sid', Rule.sid, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      mobileNo: req.body.mobileNo,
      otp: req.body.otp,
      sid: req.body.sid,
    };
    const { message, responseData } = await userController.verifyOtp(body);
    console.log('responseData :>> ', responseData);
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
