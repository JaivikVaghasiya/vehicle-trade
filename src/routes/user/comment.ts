import { Router } from 'express';
import { commentsController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body, param, query } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  check(body, 'comment', Rule.comment, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = { ...req.body, userId: req.authUser._id };
    const { message, responseData }: any = await commentsController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  auth,
  check(query, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await commentsController.list(
      req.query.vehicleId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.put(
  '/update/:commentId',
  auth,
  check(param, 'commentId', Rule.commentId, { required: true }),
  check(body, 'comment', Rule.comment, { required: true }),
  asyncHandler(async function (req, res) {
    const { message, responseData }: any = await commentsController.update(
      req.params.commentId,
      req.body,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.delete(
  '/delete/:commentId',
  auth,
  check(param, 'commentId', Rule.commentId, { required: true }),
  asyncHandler(async function (req, res) {
    const { message, responseData }: any = await commentsController.delete(
      req.params.commentId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
