import { Router } from 'express';
import { vehicleFilterController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { param, query } from 'express-validator';
const router: any = Router();

router.get(
  '/getVehicleFilter',
  auth,
  check(query, 'minPrice', Rule.price, { empty: true }),
  check(query, 'maxPrice', Rule.price, { empty: true }),
  check(query, 'minYear', Rule.year, { empty: true }),
  check(query, 'maxYear', Rule.year, { empty: true }),
  check(query, 'minKiloMeters', Rule.kiloMeter, { empty: true }),
  check(query, 'maxKiloMeters', Rule.kiloMeter, { empty: true }),
  check(query, 'search', Rule.search, { empty: true }),
  check(query, 'latitude', Rule.latitude, { empty: true }),
  check(query, 'longitude', Rule.longitude, { empty: true }),
  check(query, 'vehicleType', Rule.vehicleType, { empty: true }),
  check(query, 'brandName', Rule.brandName, { empty: true }),
  check(query, 'modelName', Rule.modelName, { empty: true }),
  check(query, 'variantName', Rule.variantName, { empty: true }),
  check(query, 'fuelType', Rule.fuelTypeId, { empty: true }),
  check(query, 'bodyType', Rule.bodyTypeId, { empty: true }),
  check(query, 'transmissionType', Rule.transmissionTypeId, { empty: true }),
  check(query, 'country', Rule.country, { empty: true }),
  check(query, 'state', Rule.state, { empty: true }),
  asyncHandler(async function (req: any, res) {
    const userId = (req?.authUser && req.authUser._id) || null;
    const filter = {
      attributeId: req.query.vehicleType,
      brandId: req.query.brandName,
      modelId: req.query.modelName,
      variantId: req.query.variantName,
      'vehicleData.fuelType': req.query.fuelType,
      'vehicleData.bodyType': req.query.bodyType,
      'vehicleData.transmissionType': req.query.transmissionType,
    };
    console.log('filter :>> ', filter);
    const { message, responseData }: any =
      await vehicleFilterController.getVehicleDetails(
        req.query,
        userId,
        filter,
      );
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/:vehicleId',
  check(param, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await vehicleFilterController.getVehicleById(req.params.vehicleId);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/near/garages/list',
  check(query, 'latitude', Rule.latitude, { required: true }),
  check(query, 'longitude', Rule.longitude, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any =
      await vehicleFilterController.listNearGarage(req.query);
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
