import { Router } from 'express';
import { favoritesController } from '../../controller';
import {
  asyncHandler,
  authenticateUser as auth,
} from '../../middleware/middleware';
import { check, Rule, sendSuccess } from '../../utils';
import { body } from 'express-validator';
const router: Router = Router();

router.post(
  '/add',
  auth,
  check(body, 'vehicleId', Rule.vehicleId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const body = {
      vehicleId: req.body.vehicleId,
      userId: req.authUser._id.toString(),
    };
    const { message, responseData }: any = await favoritesController.add(body);
    return sendSuccess(res, message, responseData);
  }),
);

router.get(
  '/list',
  auth,
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await favoritesController.list(
      req.authUser._id,
    );
    return sendSuccess(res, message, responseData);
  }),
);

router.post(
  '/delete/:favoriteId',
  auth,
  check(body, 'favoriteId', Rule.favoriteId, { required: true }),
  asyncHandler(async function (req: any, res) {
    const { message, responseData }: any = await favoritesController.delete(
      req.params.favoriteId,
    );
    return sendSuccess(res, message, responseData);
  }),
);

export default router;
