import { Message, ObjectId, logger, searching } from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { dealerDashboardServices, dealerStoreServices } from '../../services';
import { BadRequestError } from '../../error';
import moment from 'moment';

export class dealerDashboardController {
  /**
   * list count of weekly sell
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static weeklySell: Dealer.weeklySell = async (dealerId, query) => {
    let startOfWeek = query.startOfWeek
      ? Number(query.startOfWeek) / 1000
      : moment().startOf('week').unix();
    let endOfWeek = query.endOfWeek
      ? Number(query.endOfWeek) / 1000
      : moment().endOf('week').unix();
    const dealerDashboardVehicles = await dealerDashboardServices.weeklySell(
      dealerId,
      startOfWeek,
      endOfWeek,
    );
    return {
      message: Message.SUCCESSFUL,
      responseData: dealerDashboardVehicles,
    };
  };

  /**
   * list weekly sell count as per store
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static weeklySellStore: Dealer.weeklySellStore = async (dealerId, query) => {
    let startOfWeek = query.startOfWeek
      ? Number(query.startOfWeek) / 1000
      : moment().startOf('week').unix();
    let endOfWeek = query.endOfWeek
      ? Number(query.endOfWeek) / 1000
      : moment().endOf('week').unix();
    const dealerDashboardVehicles =
      await dealerDashboardServices.weeklySellStore(
        dealerId,
        startOfWeek,
        endOfWeek,
      );
    return {
      message: Message.SUCCESSFUL,
      responseData: dealerDashboardVehicles,
    };
  };

  /**
   * monthly sell count as per store
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static monthlySellStore: Dealer.monthlySellStore = async (
    dealerId,
    query,
  ) => {
    let firstOfMonth = query.firstOfMonth
      ? Number(query.firstOfMonth)
      : moment().startOf('year').valueOf();
    let lastOfMonth = query.lastOfMonth
      ? Number(query.lastOfMonth)
      : moment().endOf('year').valueOf();
    let Data = [];
    let currentDate = moment(firstOfMonth);
    while (currentDate.isSameOrBefore(moment(lastOfMonth))) {
      await processMonth(dealerId, currentDate.year(), currentDate.month() + 1);
      currentDate.add(1, 'month');
    }
    async function processMonth(userId, year, month) {
      const firstOfMonth = moment([year, month - 1])
        .startOf('month')
        .unix();
      const lastOfMonth = moment([year, month - 1])
        .endOf('month')
        .unix();
      const dealerDashboardVehicles =
        await dealerDashboardServices.monthlySellStore(
          userId,
          firstOfMonth,
          lastOfMonth,
        );
      Data.push(dealerDashboardVehicles[0]);
    }
    return {
      message: Message.SUCCESSFUL,
      responseData: Data,
    };
  };

  /**
   * give count of a day as per store
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static dailySellStore: Dealer.dailySellStore = async (dealerId, query) => {
    let startOfDay = query.startOfDay
      ? Number(query.startOfDay) / 1000
      : moment().startOf('day').unix();
    let endOfDay = query.endOfDay
      ? Number(query.endOfDay) / 1000
      : moment().endOf('day').unix();
    const dealerDashboardVehicles =
      await dealerDashboardServices.dailySellStore(
        dealerId,
        startOfDay,
        endOfDay,
      );
    return {
      message: Message.SUCCESSFUL,
      responseData: dealerDashboardVehicles,
    };
  };

  /**
   * all week day sell count
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static daySell: Dealer.daySell = async (dealerId, query) => {
    let startOfDay = query.startOfDay
      ? Number(query.startOfDay) / 1000
      : moment().startOf('week').unix();
    let endOfDay = query.endOfDay
      ? Number(query.endOfDay) / 1000
      : moment().endOf('week').unix();
    const dealerDashboardVehicles = await dealerDashboardServices.daySell(
      dealerId,
      startOfDay,
      endOfDay,
    );
    return {
      message: Message.SUCCESSFUL,
      responseData: dealerDashboardVehicles,
    };
  };

  /**
   * all the day of a month sell count
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static monthlyDayStore: Dealer.monthlyDayStore = async (dealerId, query) => {
    let startOfMonth = query.startOfMonth
      ? Number(query.startOfMonth)
      : moment().startOf('day').valueOf();
    let endOfMonth = query.endOfMonth
      ? Number(query.endOfMonth)
      : moment().endOf('day').valueOf();
    const storeId = query.storeId;

    const now = moment(startOfMonth); // get current month and year
    const month = now.month() + 1;
    const year = now.year();

    const numDaysInMonth = moment(endOfMonth).date();
    if (numDaysInMonth > 31) {
      throw new BadRequestError(Message.INVALID_MONTH);
    }
    const dateArray = [];
    for (let i = 0; i < numDaysInMonth; i++) {
      const startOfMonth = moment(`${year}-${month}-${i + 1}`)
        .startOf('day')
        .unix();
      const endOfMonth = moment(`${year}-${month}-${i + 1}`)
        .endOf('day')
        .unix();

      const dealerDashboardVehicles =
        await dealerDashboardServices.monthlyDayStore(
          dealerId,
          startOfMonth,
          endOfMonth,
          storeId,
        );

      dateArray.push(dealerDashboardVehicles[0]);
    }

    return {
      message: Message.SUCCESSFUL,
      responseData: dateArray,
    };
  };

  /**
   * all the day of a week sell count`
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static weeklyDayStore: Dealer.weeklyDaySell = async (dealerId, query) => {
    let startOfWeek = query.startOfWeek
      ? moment(Number(query.startOfWeek)).startOf('week')
      : moment().startOf('week');
    let endOfWeek = query.endOfWeek
      ? moment(Number(query.endOfWeek)).endOf('isoWeek')
      : moment().endOf('day').valueOf();
    const storeId = query.storeId;

    // const storeId = new ObjectId(query.storeId);
    const dateArray = [];

    const limit = moment(endOfWeek.valueOf()).isoWeekday();

    for (let i = 0; i < limit; i++) {
      const startOfDay = startOfWeek.add(1, 'day').unix();
      const endOfDay = Number(startOfDay) + 86399;
      const dealerDashboardVehicles =
        await dealerDashboardServices.weeklyDayStore(
          dealerId,
          startOfDay,
          endOfDay,
          storeId,
        );
      dateArray.push(dealerDashboardVehicles[0]);
    }

    return {
      message: Message.SUCCESSFUL,
      responseData: dateArray,
    };
  };

  static yearlySell: Dealer.yearlySell = async (dealerId, query) => {
    const startOfYear = query.startOfYear
      ? Number(query.startOfYear)
      : moment().startOf('month').valueOf();
    const endOfYear = query.endOfYear
      ? Number(query.endOfYear)
      : moment().endOf('month').valueOf();
    const storeId = query.storeId;

    //  const storeId = query.storeId ? new ObjectId(query.storeId) : null;
    const now = moment(startOfYear); // get current month and year
    const year = now.year();

    const numDaysInMonth = moment(endOfYear).month();
    if (numDaysInMonth > 12) {
      throw new BadRequestError(Message.INVALID_YEAR);
    }
    const dateArray = [];
    for (let i = 0; i <= numDaysInMonth; i++) {
      const startDate = moment(`${year}-${i + 1}-${1}`)
        .startOf('month')
        .unix();
      const endDate = moment(`${year}-${i + 1}-${1}`)
        .endOf('month')
        .unix();
      const dealerDashboardVehicles = await dealerDashboardServices.yearlySell(
        dealerId,
        startDate,
        endDate,
        storeId,
      );
      dateArray.push(dealerDashboardVehicles[0]);
    }

    return {
      message: Message.SUCCESSFUL,
      responseData: dateArray,
    };
  };

  /**
   * vehicle brand wise sell count
   * @param dealerId
   * @returns {ReturnValue}
   */
  static allCurrentSellCount: Dealer.CurrentSellCount = async (dealerId) => {
    const monthlySell = {
      startOfMonth: moment().startOf('month').unix(),
      endOfMonth: moment().endOf('month').unix(),
    };
    const yearlySell = {
      startOfYear: moment().startOf('year').unix(),
      endOfYear: moment().endOf('year').unix(),
    };
    const weeklySell = {
      startOfWeek: moment().startOf('week').unix(),
      endOfWeek: moment().endOf('week').unix(),
    };
    const dailySell = {
      startOfDay: moment().startOf('day').unix(),
      endOfDay: moment().endOf('day').unix(),
    };

    const dealerDashboardVehicles =
      await dealerDashboardServices.allCurrentSellCount(
        dealerId,
        monthlySell,
        yearlySell,
        weeklySell,
        dailySell,
      );
    return {
      message: Message.SUCCESSFUL,
      responseData: dealerDashboardVehicles,
    };
  };

  /**
   * user inquiries count by multiple filters like brand, model...
   * @param dealerId
   * @param body
   * @param match
   * @returns {ReturnValue}
   */
  static inquiryCountByFilter: any = async (dealerId, body, match) => {
    let filter: any;
    let storeIds =
      body.hasOwnProperty('storeId') && body.storeId
        ? {
            'vehicleDetails.dealerStoreId': {
              $in: body.storeId.map((storeId) => new ObjectId(storeId)),
            },
          }
        : {
            'vehicleDetails.dealerStoreId': {
              $in: (
                await dealerStoreServices.findAllDealerStore({
                  dealerId,
                })
              ).map((store) => store._id),
            },
          };
    filter = { ...(await searching.search(match, '$and')), ...storeIds };
    const count = await dealerDashboardServices.inquiryCountByFilter(filter);
    return {
      message: Message.SUCCESSFUL,
      responseData: { count },
    };
  };

  /**
   * User inquiry count as per brand
   * @param dealerId
   * @param storeIds
   * @returns {ReturnValue}
   */
  static allBrandInquiryCount: any = async (dealerId, storeIds) => {
    let filter = storeIds
      ? {
          'vehicleDetails.dealerStoreId': {
            $in: storeIds.map((storeId) => new ObjectId(storeId)),
          },
        }
      : {
          'vehicleDetails.dealerStoreId': {
            $in: (
              await dealerStoreServices.findAllDealerStore({
                dealerId,
              })
            ).map((store) => store._id),
          },
        };
    const responseData = await dealerDashboardServices.allBrandInquiryCount(
      filter,
    );
    return {
      message: Message.SUCCESSFUL,
      responseData,
    };
  };

  static monthSellStore: Dealer.monthSellStore = async (dealerId, query) => {
    let firstOfMonth = query.firstOfMonth
      ? Number(query.firstOfMonth)
      : moment().startOf('day').valueOf();
    let lastOfMonth = query.lastOfMonth
      ? Number(query.lastOfMonth)
      : moment().endOf('day').valueOf();
    const storeId = new ObjectId(query.storeId);
    let storeSellCountArray = [];
    let storeName = '';
    const now = moment(firstOfMonth); // get current month and year
    const month = now.month() + 1;
    const year = now.year();

    const numDaysInMonth = moment(lastOfMonth).date();
    if (numDaysInMonth > 31) {
      throw new BadRequestError(Message.INVALID_MONTH);
    }
    for (let i = 0; i < numDaysInMonth; i++) {
      const firstOfMonth = moment(`${year}-${month}-${i + 1}`)
        .startOf('day')
        .unix();
      const lastOfMonth = moment(`${year}-${month}-${i + 1}`)
        .endOf('day')
        .unix();
      const dealerDashboardVehicles =
        await dealerDashboardServices.monthSellStore(
          storeId,
          firstOfMonth,
          lastOfMonth,
        );
      storeName = dealerDashboardVehicles[0].storeName;
      storeSellCountArray.push(dealerDashboardVehicles[0].monthlySellCount);
    }
    const result = {
      storeName,
      monthlySellCount: storeSellCountArray,
    };
    return {
      message: Message.SUCCESSFUL,
      responseData: result,
    };
  };

  static yearSellStore: Dealer.yearSellStore = async (dealerId, query) => {
    const startOfYear = query.startOfYear
      ? Number(query.startOfYear)
      : moment().startOf('month').valueOf();
    const endOfYear = query.endOfYear
      ? Number(query.endOfYear)
      : moment().endOf('month').valueOf();
    const storeId = new ObjectId(query.storeId);
    const now = moment(startOfYear); // get current month and year
    const year = now.year();
    const dateArray = [];
    let storeName = '';
    const numDaysInMonth = moment(endOfYear).month();
    if (numDaysInMonth > 12) {
      throw new BadRequestError(Message.INVALID_YEAR);
    }

    for (let i = 0; i <= numDaysInMonth; i++) {
      const startDate = moment(`${year}-${i + 1}-${1}`)
        .startOf('month')
        .unix();
      const endDate = moment(`${year}-${i + 1}-${1}`)
        .endOf('month')
        .unix();
      const dealerDashboardVehicles =
        await dealerDashboardServices.yearSellStore(
          storeId,
          startDate,
          endDate,
        );
      storeName = dealerDashboardVehicles[0].storeName;
      dateArray.push(dealerDashboardVehicles[0].yearlySellCount);
    }

    const result = {
      storeName,
      yearlySellCount: dateArray,
    };
    return {
      message: Message.SUCCESSFUL,
      responseData: result,
    };
  };

  static weekSellStore: Dealer.weekSellStore = async (dealerId, query) => {
    let startOfWeek = query.startOfWeek
      ? moment(Number(query.startOfWeek)).startOf('week')
      : moment().startOf('week');
    let endOfWeek = query.endOfWeek
      ? moment(Number(query.endOfWeek)).endOf('isoWeek')
      : moment().endOf('day').valueOf();
    const storeId = new ObjectId(query.storeId);
    const dateArray = [];
    let storeName = '';
    const limit = moment(endOfWeek.valueOf()).isoWeekday();
    for (let i = 0; i < limit; i++) {
      const startOfDay = startOfWeek.add(1, 'day').unix();
      const endOfDay = Number(startOfDay) + 86399;
      const dealerDashboardVehicles =
        await dealerDashboardServices.weekSellStore(
          storeId,
          startOfDay,
          endOfDay,
        );
      storeName = dealerDashboardVehicles[0].storeName;
      dateArray.push(dealerDashboardVehicles[0].weeklySellCount);
    }
    const result = {
      storeName,
      weeklySellCount: dateArray,
    };
    return {
      message: Message.SUCCESSFUL,
      responseData: result,
    };
  };
}
