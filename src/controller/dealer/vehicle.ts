import { Message, deleteFile, ObjectId, logger } from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { vehicleServices } from '../../services';

export class vehicleController {
  /**
   * Add vehicle by Dealer
   * @param vehicleBody
   * @param vehicleDetailsBody
   * @param files
   * @returns {ReturnValue}
   */
  static add: Dealer.AddVehicle = async (
    vehicleBody,
    vehicleDetailsBody,
    files,
  ) => {
    const addVehicle: Dealer.Vehicle = await vehicleServices.addVehicle(
      vehicleBody,
      vehicleDetailsBody,
      files,
    );
    return {
      message: Message.VEHICLE_ADDED_SUCCESSFULLY,
      responseData: {
        _id: addVehicle._id,
      },
    };
  };

  /**
   * list Vehicle one or multiple with full details
   * @param vehicleId
   * @returns {ReturnValue}
   */
  static list: Dealer.ListVehicle = async (vehicleId) => {
    const vehicleData: Dealer.Vehicle[] =
      await vehicleServices.vehicleListByFilter({
        _id: new ObjectId(vehicleId),
      });
    return {
      message: Message.GET_VEHICLE_SUCCESSFUL,
      responseData: vehicleData,
    };
  };

  /**
   * list single vehicle to edit it
   * @param vehicleId
   * @returns
   */
  static edit: Dealer.ListVehicle = async (vehicleId) => {
    const vehicleData: Dealer.Vehicle[] =
      await vehicleServices.vehicleFullListByFilter(
        {
          _id: new ObjectId(vehicleId),
        },
        true,
      );
    return {
      message: Message.VEHICLE_LISTED_SUCCESSFULLY,
      responseData: vehicleData[0],
    };
  };

  /**
   * Update vehicle details
   * @param vehicleId
   * @param vehicleBody
   * @param vehicleDetailsBody
   * @param files
   * @returns {ReturnValue}
   */
  static update: Dealer.UpdateVehicle = async (
    vehicleId,
    vehicleBody,
    vehicleDetailsBody,
    files,
  ) => {
    const updateVehicle: Dealer.Vehicle = await vehicleServices.updateVehicle(
      vehicleId,
      vehicleBody,
      vehicleDetailsBody,
      files,
    );
    return {
      message: Message.VEHICLE_UPDATED_SUCCESSFULLY,
      responseData: { _id: updateVehicle._id },
    };
  };

  /**
   * Delete vehicle
   * @param vehicleId
   * @returns {ReturnValue}
   */
  static delete: Dealer.DeleteVehicle = async (vehicleId) => {
    await vehicleServices.deleteVehicle(vehicleId);
    return { message: Message.VEHICLE_DELETED_SUCCESSFULLY };
  };

  /**
   * Delete vehicle Images
   * @param imageId
   * @returns {ReturnValue}
   */
  static deleteImage: Dealer.DeleteVehicleImage = async (imageId) => {
    let { checkVehicle, filePath }: any =
      await vehicleServices.deleteVehicleImage(imageId);
    filePath = JSON.parse(JSON.stringify(filePath));
    const [objectKeys] = Object.keys(filePath);
    await deleteFile(checkVehicle[objectKeys], checkVehicle.vehicleId);
    return { message: Message.VEHICLE_IMAGE_DELETED_SUCCESSFULLY };
  };
}
