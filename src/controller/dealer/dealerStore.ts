import { Message } from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { dealerStoreServices } from '../../services';

export class dealerStoreController {
  /**
   * Dealer can add store
   * @param body
   * @param files
   * @returns {ReturnValue}
   */
  static add: Dealer.AddStore = async (body, files) => {
    const addDealerStore: Dealer.DealerStore =
      await dealerStoreServices.addStore(body, files);
    return {
      message: Message.ADD_DEALER_STORE_SUCCESSFUL,
      responseData: addDealerStore,
    };
  };

  /**
   * lists all stores of a dealer
   * @param dealerId
   * @returns {ReturnValue}
   */
  static list: Dealer.ListStore = async (dealerId) => {
    let getDealerStore: Dealer.DealerStore[] =
      await dealerStoreServices.listStore(dealerId);
    return { message: Message.SUCCESSFUL, responseData: getDealerStore };
  };

  /**
   * update details of store
   * @param storeId
   * @param body
   * @param files
   * @returns {ReturnValue}
   */
  static update: Dealer.UpdateStore = async (storeId, body, files) => {
    const updateDealerStore: Dealer.DealerStore =
      await dealerStoreServices.updateStore(storeId, body, files);
    return {
      message: Message.DEALER_STORE_UPDATED_SUCCESSFULLY,
      responseData: { _id: updateDealerStore._id },
    };
  };

  /**
   * delete dealer store
   * @param storeId
   * @returns {ReturnValue}
   */
  static delete: Dealer.DeleteStore = async (storeId) => {
    await dealerStoreServices.deleteStore(storeId);
    return { message: Message.DEALER_STORE_UPDATED_SUCCESSFULLY };
  };
}
