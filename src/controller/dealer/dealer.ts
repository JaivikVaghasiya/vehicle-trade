import {
  Message,
  logger,
  paginationAndSorting,
  searching,
  paginateResponse,
  ObjectId,
} from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { dealerServices } from '../../services';

/**
 * This common function for  this file apis
 * @param search search
 * @returns {Array} search
 */
let searchValue: any;
const commonFunction = async ({ query, dealerId }, serviceName) => {
  let filter: any = {};
  const pagination: any = paginationAndSorting(query, '_id');
  if (query?.search) {
    searchValue = searching.regexSearch(
      RegExp(query.search.trim().split(/\s+/).join('|')),
      [
        'storeName',
        'storeContact',
        'variant.model.brand.brandName',
        'variant.model.modelName',
        'variant.variantName',
        'vehicleData.plateNumber',
      ],
    );
    searchValue.$or.push({
      $expr: {
        $eq: [{ $toString: '$vehicleData.manufactureYear' }, query.search],
      },
    });
  }
  let match = query?.storeId
    ? { _id: new ObjectId(query.storeId) }
    : { dealerId };
  filter = { pagination, match: { ...searchValue, ...match } };
  const responseData = await dealerServices[serviceName](filter, false);
  const totalRecords: number = (await dealerServices[serviceName](filter, true))
    .length;
  return paginateResponse(responseData, pagination, totalRecords);
};

export class dealerController {
  /**
   * This function retrieve all sold vehicle list
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static soldVehicles: Dealer.SoldVehicle = async (dealerId, query) => {
    const responseData = await commonFunction(
      { query, dealerId },
      'soldVehicle',
    );
    return { message: Message.SUCCESSFUL, responseData };
  };

  /**
   * This function lists all unsold vehicle list
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static remainingVehicles: Dealer.RemainingVehicles = async (
    dealerId,
    query,
  ) => {
    const responseData = await commonFunction(
      { query, dealerId },
      'remainingVehicles',
    );
    return { message: Message.SUCCESSFUL, responseData };
  };

  /**
   * This function lists all user inquires of vehicles
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static userInquiries: Dealer.UserInquiries = async (dealerId, query) => {
    const responseData = await commonFunction(
      { query, dealerId },
      'userInquiries',
    );
    return { message: Message.SUCCESSFUL, responseData };
  };

  /**
   * approve user inquiry
   * @param body
   * @returns {ReturnValue}
   */
  static approveUserInquiries: Dealer.ApproveInquiry = async (body) => {
    await dealerServices.approveInquiry(body);
    return { message: Message.INQUIRY_HAS_APPROVED };
  };

  /**
   * refuse inquiry which has approved
   * @param body
   * @returns {ReturnValue}
   */
  static refuseUserInquiries: Dealer.RefuseInquiry = async (body) => {
    await dealerServices.refuseInquiry(body);
    return { message: Message.INQUIRY_HAS_REFUSED };
  };
}
