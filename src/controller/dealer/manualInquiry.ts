import {
  Message,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { manualInquiryServices } from '../../services';

export class manualInquiryController {
  /**
   * User will add vehicle inquiry as per choice
   * @param body
   * @returns {ReturnValue}
   */
  static add: Dealer.AddInquiry = async (body) => {
    const addManualInquiry: Dealer.ManualInquiryBody =
      await manualInquiryServices.addManualInquiry(body);
    return {
      message: Message.ADD_MANUAL_INQUIRY_SUCCESSFUL,
      responseData: addManualInquiry,
    };
  };

  /**
   * List all manual inquiry to all dealers
   * @param _id
   * @param body
   * @param filters
   * @returns {ReturnValue}
   */
  static list: Dealer.ListInquiry = async (_id, body, filters) => {
    let { minBudget, maxBudget, search, limit, sortField, sortType, page } =
      body;
    search = search
      ? searching.regexSearch(RegExp(search.trim().split(/\s+/).join('|')), [
          'brandsData.brandName',
          'modelsData.modelName',
          'variantsData.variantName',
          'usersData.userName',
        ])
      : '';
    let filter = searching.search(filters, '$and');
    const pagination: any = paginationAndSorting(
      { page, sortType, sortField, limit },
      '_id',
    );
    filter = {
      ...pagination,
      match: {
        ...filter,
        ...search,
        $expr: {
          $and: [
            { $gte: ['$priceRange', Number(minBudget) || 1] },
            { $lte: ['$priceRange', Number(maxBudget) || 10000000000] },
          ],
        },
      },
    };
    const listManualInquiry = await manualInquiryServices.listManualInquiry(
      filter,
      false,
    );
    const totalRecords = await manualInquiryServices.listManualInquiry(
      filter,
      true,
    );
    const responseData = paginateResponse(
      listManualInquiry,
      pagination,
      totalRecords.length,
    );
    return { message: Message.SUCCESSFUL, responseData };
  };
}
