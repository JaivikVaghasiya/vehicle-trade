import {
  Message,
  paginationAndSorting,
  searching,
  paginateResponse,
} from '../../utils';
import { Dealer, ReturnValue } from '../../types';
import { userActivityServices } from '../../services';
import { BadRequestError } from '../../error';

export class userActivityController {
  /**
   * stores userActivity
   * @param body
   * @returns {ReturnValue}
   */
  static add: Dealer.UserActivity = async (body) => {
    const addUserActivity = await userActivityServices.addUserActivity(body);
    if (!addUserActivity)
      throw new BadRequestError(Message.USER_ACTIVITY_ALREADY_EXITS);

    return {
      message: Message.SUCCESSFUL,
      responseData: { _id: addUserActivity._id },
    };
  };

  /**
   * list userActive to dealer if user do some activity on his vehicle
   * @param dealerId
   * @param query
   * @returns {ReturnValue}
   */
  static list: Dealer.ListActivity = async (dealerId, query) => {
    let filter: any = {};
    const search =
      query.hasOwnProperty('search') && query.search
        ? searching.regexSearch(
            RegExp(query.search.trim().split(/\s+/).join('|')),
            [
              'storeName',
              'storeContact',
              'variant.model.brand.brandName',
              'variant.model.modelName',
              'variant.variantName',
              'vehicleData.plateNumber',
              'userData.userName',
            ],
          )
        : '';
    const pagination: any = query ? paginationAndSorting(query, '_id') : {};
    filter = { ...pagination, search: { ...search, dealerId } };
    const totalRecords = (
      await userActivityServices.findDealerActivityCount(true, {
        'storeData.dealerId': dealerId,
      })
    ).length;
    query.limit = !query.limit ? totalRecords : query.limit;
    const userActivity = await userActivityServices.listUserActivity(
      filter,
      false,
    );
    const responseData = paginateResponse(
      userActivity,
      pagination,
      totalRecords,
    );
    return { message: Message.SUCCESSFUL, responseData };
  };

  /**
   * Dealer can delete these activities
   * @param ActivityId
   * @returns {ReturnValue}
   */
  static delete: Dealer.DeleteActivity = async (ActivityId) => {
    await userActivityServices.removeUserActivity(ActivityId);
    return { message: Message.ACTIVITY_REMOVED };
  };
}
