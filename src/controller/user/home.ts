import {
  Message,
  ObjectId,
  geoLocation,
  searching,
  APPROVAL_STATUS,
  logger,
} from '../../utils';
import { Dealer, ReturnValue, User } from '../../types';
import { BadRequestError } from '../../error';
import { vehicleServices, garageServices } from '../../services';

export class vehicleFilterController {
  /**
   * list of dealers's vehicle with multiple filters
   * @param query
   * @param userId
   * @param match
   * @returns {ReturnValue}
   */
  static getVehicleDetails: User.VehicleList = async (query, userId, match) => {
    let {
      minPrice,
      maxPrice,
      minYear,
      maxYear,
      minKiloMeters,
      maxKiloMeters,
      search,
      latitude,
      longitude,
    } = query;

    let regex = search ? RegExp(search.trim().split(/\s+/).join('|')) : '';

    let { country, state }: any =
      longitude && latitude
        ? await geoLocation(latitude, longitude)
        : {
            country: query.country,
            state: query.state,
          };
    match['locationData.storeLocation.country'] = country;
    match['locationData.storeLocation.state'] = state;
    search = searching.regexSearch(regex, [
      'brandData.brandName',
      'model.modelName',
      'variantData.variantName',
    ]);

    let filter = searching.search(match, '$and');
    filter = {
      isDeleted: false,
      ...filter,
      ...search,
      $expr: {
        $and: [
          { $lte: ['$price', Number(maxPrice) || 100000000] },
          { $gte: ['$price', Number(minPrice) || 0] },
          {
            $lte: [
              '$manufactureYear',
              Number(maxYear) || new Date().getFullYear(),
            ],
          },
          { $gte: ['$manufactureYear', Number(minYear) || 2000] },
          { $lte: ['$kiloMeters', Number(maxKiloMeters) || 90000] },
          { $gte: ['$kiloMeters', Number(minKiloMeters) || 0] },
        ],
      },
    };
    let getVehicleDetails: Dealer.Vehicle[] =
      await vehicleServices.vehicleListByFilter(filter, userId);
    return {
      message: Message.VEHICLE_LIST_SUCCESSFUL,
      responseData: getVehicleDetails,
    };
  };

  /**
   * retrieve a particular vehicle
   * @param vehicleId
   * @returns {ReturnValue}
   */
  static getVehicleById: User.VehicleListById = async (vehicleId) => {
    let vehicleData: Array<Dealer.Vehicle> =
      await vehicleServices.vehicleFullListByFilter(
        {
          _id: new ObjectId(vehicleId),
        },
        false,
      );
    if (!vehicleData) throw new BadRequestError(Message.VEHICLE_NOT_FOUND);
    return {
      message: Message.VEHICLE_LIST_SUCCESSFUL,
      responseData: vehicleData[0],
    };
  };

  /**
   * List nearer garages to user
   * @param body
   * @returns {ReturnValue}
   */
  static listNearGarage: User.VehicleListById = async (body) => {
    const responseData = await garageServices.listNearGarage(body);
    return {
      message: Message.VEHICLE_LIST_SUCCESSFUL,
      responseData,
    };
  };
}
