import { Message } from '../../utils';
import { ReturnValue, User } from '../../types';
import { favoriteServices } from '../../services';

export class favoritesController {
  /**
   * Add vehicle to favorite or whishList
   * @param body
   * @returns {ReturnValue}
   */
  static add: User.AddFavorite = async (body) => {
    const addFavorite: User.Favorite = await favoriteServices.addFavorite(body);
    return {
      message: Message.SUCCESSFUL,
      responseData: { _id: addFavorite._id },
    };
  };

  /**
   * List of all favorite vehicle
   * @param userId
   * @returns {ReturnValue}
   */
  static list: User.ListFavorite = async (userId) => {
    const favoriteList: User.Favorite[] | any =
      await favoriteServices.listFavorite(userId);
    return { message: Message.SUCCESSFUL, responseData: favoriteList };
  };

  /**
   * Delete a vehicle from favorite
   * @param favoriteId
   * @returns {ReturnValue}
   */
  static delete: User.DeleteFavorite = async (favoriteId) => {
    await favoriteServices.deleteFavorite(favoriteId);
    return { message: Message.FAVORITE_DATA_DELETED_SUCCESSFULLY };
  };
}
