import {
  Message,
  generateToken,
  sendOtp,
  sendEmail,
  verifyToken,
  ROLE,
  verifyOtp,
} from '../../utils';
import { ReturnValue, User } from '../../types';
import { userServices } from '../../services';
import { BadRequestError } from '../../error';
import moment from 'moment';

export class userController {
  /**
   * SignUp User
   * @param body
   * @param file
   * @returns {ReturnValue}
   */
  static signUp: User.SignUp = async (body, file) => {
    const userData = await userServices.signUpUser(body, file);
    return { message: Message.REGISTRATION_SUCCESSFUL, responseData: userData };
  };

  /**
   * Send otp to user by twilio
   * @param mobileNo
   * @returns {ReturnValue}
   */
  static sendOtp: User.FSendOtp = async (mobileNo) => {
    const data = await sendOtp(mobileNo);
    if (data) {
      return {
        message: Message.OTP_SENT_SUCCESSFULLY,
        responseData: { mobileNo: mobileNo, sid: data.serviceSid },
      };
    }
  };

  /**
   * Login user by pass and opt verification
   * @param body
   * @returns {ReturnValue}
   */
  static logIn: User.LoginUser = async (body) => {
    const userData = await userServices.loginUser(body);
    return { message: Message.LOGIN_SUCCESSFUL, responseData: userData };
  };

  /**
   * List all users
   * @param user
   * @returns {ReturnValue}
   */
  static list: User.UserList = async (user) => {
    const userData = {
      userId: user._id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      mobileNo: user.mobileNo,
      loggedOn: moment(user.createdAt).format('dddd, MMMM Do YYYY, h:mm:ss a'),
      profileImage: user.profileImage,
      isActive: user.isActive,
    };
    return {
      message: Message.USER_LIST_SUCCESSFULLY,
      responseData: userData,
    };
  };

  /**
   * User can update his/her profile
   * @param userId
   * @param body
   * @param files
   * @returns {ReturnValue}
   */
  static update: User.UpdateUser = async (userId, body, files) => {
    const userData: User.User = await userServices.updateUser(
      userId,
      body,
      files,
    );
    return {
      message: Message.USER_UPDATED_SUCCESSFULLY,
      responseData: { _id: userData._id },
    };
  };

  /**
   * Delete User
   * @param userId
   * @returns
   */
  static delete: User.DeleteUser = async (userId) => {
    await userServices.deleteUser(userId);
    return { message: Message.USER_DELETED_SUCCESSFULLY };
  };

  /**
   * Retrieve User details to edit
   * @param userId
   * @returns {ReturnValue}
   */
  static edit: User.UserById = async (userId) => {
    const findUser: User.User = await userServices.userById(userId);
    return { message: Message.SUCCESSFUL, responseData: findUser };
  };

  /**
   * User can forgot password by email or otp
   * @param body
   * @returns {ReturnValue}
   */
  static forgetPassword: User.ForgetPassword = async (body) => {
    const findUser = await userServices.userFindOneByFilter({
      isDeleted: false,
      email: body.email,
    });
    if (findUser && Number(findUser.role) === ROLE.ADMIN)
      throw new BadRequestError(Message.USER_NOT_FOUND);
    const token = await generateToken(
      findUser,
      process.env.RESET_PASSWORD_EXPIRE_TIME,
    );

    await sendEmail(body.email, token);
    return { message: Message.EMAIL_SENT_SUCCESSFUL };
  };

  /**
   * set New password
   * @param body
   * @returns {ReturnValue}
   */
  static resetPassword: User.ResetPassword = async (body) => {
    const decode = await verifyToken(body.token);

    if (decode == true) throw new BadRequestError(Message.SOMETHING_WENT_WRONG);
    const resetPassword = await userServices.resetPassword(body, decode);
    return {
      message: Message.PASSWORD_CHANGED_SUCCESSFULLY,
      responseData: resetPassword,
    };
  };

  /**
   * Verification of Opt using twilio
   * @param body
   * @returns {ReturnValue}
   */
  static verifyOtp: User.CVerifyOtp = async (body) => {
    const { mobileNo, otp, sid } = body;

    const findUser = await userServices.userFindOneByFilter({
      mobileNo: body.mobileNo,
    });

    if (findUser) {
      const verify: any = await verifyOtp(mobileNo, otp, sid);
      if (!verify || verify == undefined)
        throw new BadRequestError(Message.WRONG_OTP);

      return {
        message: Message.OTP_VERIFIED_SUCCESSFULLY,
        responseData: body.mobileNo,
      };
    }
  };
}
