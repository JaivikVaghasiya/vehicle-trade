import { ReturnValue, User } from '../../types';
import { commentServices } from '../../services';
import { Message } from '../../utils';

export class commentsController {
  /**
   * Add User comment on vehicle
   * @param body
   * @returns {ReturnValue}
   */
  static add: User.AddComment = async (body) => {
    const addComment: User.Comment = await commentServices.addComment(body);
    return {
      message: Message.COMMENTED_SUCCESSFULLY,
      responseData: addComment,
    };
  };

  /**
   * list all comments of a vehicle
   * @param vehicleId
   * @returns {ReturnValue}
   */
  static list: User.ListComment = async (vehicleId) => {
    const comments: User.Comment[] = await commentServices.listComment(
      vehicleId,
    );
    return { message: Message.SUCCESSFUL, responseData: comments };
  };

  /**
   * Update comment
   * @param commentId
   * @param body
   * @returns {ReturnValue}
   */
  static update: User.UpdateComment = async (commentId, body) => {
    await commentServices.updateComment(commentId, body);
    return { message: Message.COMMENT_UPDATED_SUCCESSFULLY };
  };

  /**
   * Delete comment
   * @param commentId
   * @returns {ReturnValue}
   */
  static delete: User.DeleteComment = async (commentId) => {
    await commentServices.deleteComment(commentId);
    return { message: Message.COMMENT_DELETED_SUCCESSFULLY };
  };
}
