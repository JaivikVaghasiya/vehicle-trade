import { userInquiryServices } from './../../services';
import { User } from '../../types';
import { Message } from '../../utils';

export class userInquiry {
  static add: User.AddInquiry = async (body) => {
    const addUserInquiry = await userInquiryServices.addUserInquiry(body);
    return {
      message: Message.SUCCESSFUL,
      responseData: { _id: addUserInquiry._id },
    };
  };

  static list: User.ListInquiry = async (userId) => {
    const getData = await userInquiryServices.listUserInquiry(userId);
    return { message: Message.SUCCESSFUL, responseData: getData };
  };

  static delete: User.DeleteInquiry = async (inquiryId) => {
    await userInquiryServices.removeUserInquiry(inquiryId);
    return { message: Message.INQUIRY_REMOVED };
  };
}
