import { Message } from '../../utils';
import { Mechanic, ReturnValue } from '../../types';
import { garageServices } from '../../services';

export class garageController {
  /**
   * add Mechanic garage
   * @param body
   * @returns {ReturnValue}
   */
  static add: Mechanic.AddGarage = async (body) => {
    const addGarage = await garageServices.addGarage(body);
    return {
      message: Message.ADD_GARAGE_SUCCESSFUL,
      responseData: { _id: addGarage._id },
    };
  };

  /**
   * List all garages of a mechanic
   * @param mechanicId
   * @returns {ReturnValue}
   */
  static list: Mechanic.ListGarage = async (mechanicId) => {
    let getGarage: Mechanic.Garage[] = await garageServices.listGarage(
      mechanicId,
    );
    return { message: Message.SUCCESSFUL, responseData: getGarage };
  };

  /**
   * Update a garage
   * @param garageId
   * @param body
   * @returns {ReturnValue}
   */
  static update: Mechanic.UpdateGarage = async (garageId, body) => {
    const updateGarage: Mechanic.Garage = await garageServices.updateGarage(
      garageId,
      body,
    );
    return {
      message: Message.GARAGE_UPDATED_SUCCESSFULLY,
      responseData: { _id: updateGarage._id },
    };
  };

  /**
   * Delete a garage
   * @param garageId
   * @returns {ReturnValue}
   */
  static delete: Mechanic.DeleteGarage = async (garageId) => {
    await garageServices.deleteGarage(garageId);
    return { message: Message.GARAGE_UPDATED_SUCCESSFULLY };
  };
}
