// import { logger, Message, ObjectId } from '../../utils';
// import { Functions } from '../../types/type';
// import { garageServices, garageServiceServices } from '../../services';

// export class garageServicesController {
//   static add: Functions.AddService = async (body) => {
//     const addService = await garageServiceServices.addService(body);
//     return {
//       message: Message.ADD_GARAGE_SERVICE_SUCCESSFUL,
//       responseData: { _id: addService._id },
//     };
//   };

//   static list = async (query) => {
//     let filter: any = { garageId: new ObjectId(query.garageId) };
//     filter = query.serviceType
//       ? { ...filter, serviceTypeId: new ObjectId(query.serviceType) }
//       : filter;
//     let getService: any = await garageServiceServices.listService(filter);
//     return { message: Message.SUCCESSFUL, responseData: getService };
//   };

//   static update: Functions.UpdateService | any = async (
//     garageServicesId,
//     body,
//   ) => {
//     const UpdateService: any = await garageServiceServices.UpdateService(
//       garageServicesId,
//       body,
//     );
//     return {
//       message: Message.SERVICE_UPDATED_SUCCESSFULLY,
//       responseData: { _id: UpdateService._id },
//     };
//   };

//   static activeInactiveService: Functions.UpdateStatus = async (payload) => {
//     await garageServiceServices.activeInactiveService(payload);
//     return { message: Message.SERVICE_UPDATED_SUCCESSFULLY };
//   };

//   static delete: Functions.DeleteService = async (payload) => {
//     await garageServiceServices.deleteService(payload);
//     return { message: Message.SERVICE_UPDATED_SUCCESSFULLY };
//   };
// }
