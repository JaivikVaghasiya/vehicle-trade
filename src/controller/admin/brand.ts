import {
  Message,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { brandServices } from '../../services';

export class brandController {
  /**
   * This function add Brand
   * @param body
   * @param files
   * @returns {ReturnValue}
   */
  static add: Admin.AddBrand = async (body, files) => {
    const addBrand: Admin.Brand | null = await brandServices.addBrand(
      body,
      files,
    );
    return {
      message: Message.BRAND_ADDED_SUCCESSFULLY,
      responseData: { _id: addBrand._id },
    };
  };

  /**
   * This function lists Brands
   * @param body
   * @returns {ReturnValue}
   */
  static list: Admin.ListBrand = async (body: any) => {
    let filter: any = {};
    let responseData;
    let match =
      body?.flag && body.flag.length
        ? { isActive: true, isDeleted: false, flag: { $in: body.flag } }
        : { isActive: true, isDeleted: false };
    let { isDropdown } = body;
    if (isDropdown) {
      filter = { ...match };
      responseData = { list: await brandServices.findAllBrandByFilter(filter) };
    } else {
      const pagination: any = body ? paginationAndSorting(body, '_id') : {};
      const search =
        body.hasOwnProperty('search') && body.search
          ? searching.regexSearch(
              RegExp(body.search.trim().split(/\s+/).join('|')),
              ['attribute.attributeName', 'brandName'],
            )
          : '';
      filter = { ...pagination, match: { ...search, ...match } };
      const brandData: Admin.Brand[] = await brandServices.listBrand(
        filter,
        true,
      );
      const totalRecords = (
        await brandServices.listBrand({ match: { ...match, ...search } }, false)
      ).length;
      responseData = paginateResponse(brandData, pagination, totalRecords);
    }
    return {
      message: Message.BRAND_LIST_SUCCESSFULLY,
      responseData,
    };
  };

  /**
   * This function update a Brand
   * @param brandId
   * @param body
   * @param files
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateBrand = async (brandId, body, files) => {
    const brandData: Admin.Brand = await brandServices.updateBrand(
      brandId,
      body,
      files,
    );
    return {
      message: Message.BRAND_UPDATED_SUCCESSFULLY,
      responseData: { _id: brandData._id },
    };
  };

  /**
   * This function deletes a brand
   * @param brandId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteBrand = async (brandId) => {
    await brandServices.deleteBrand(brandId);
    return { message: Message.BRAND_DELETED_SUCCESSFULLY };
  };
}
