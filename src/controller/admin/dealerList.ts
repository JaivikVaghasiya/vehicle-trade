import {
  Message,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { dealerListServices } from '../../services';

export class dealerListController {
  /**
   * This function is used to retrieve a list of dealerList
   * @param query
   * @param id
   * @returns {ReturnValue}
   */
  static list: Admin.ListDealer = async (query, id) => {
    const role = Number(query.role);
    let filter: any = {};
    const pagination: any = query ? paginationAndSorting(query, '_id') : {};
    const search =
      query.hasOwnProperty('search') && query.search
        ? searching.regexSearch(
            RegExp(query.search.trim().split(/\s+/).join('|')),
            ['firstName', 'lastName', 'email', 'mobileNo'],
          )
        : '';
    filter = { ...pagination, search: { ...search }, role };

    const dealerList = await dealerListServices.dealerListFindByFilter(
      filter,
      id,
    );
    const totalRecords = await dealerListServices.dealerListFindByFilter(
      filter,
      id,
    );
    const responseData = paginateResponse(
      dealerList,
      pagination,
      totalRecords.length,
    );
    return { message: Message.SUCCESSFUL, responseData };
  };

  /**
   * This function is used to retrieve a update of dealerList
   * @param body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateDealer = async (body) => {
    const UpdateDealer = await dealerListServices.deactivateDealer(body);
    return {
      message: Message.DEALER_UPDATED_SUCCESSFULLY,
      responseData: { _id: body.dealerId },
    };
  };
}
