import {
  Message,
  ObjectId,
  logger,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { modelServices } from '../../services';

export class modelController {
  /**
   * Add a new car model
   * @param body
   * @returns {ReturnValue}
   */
  static add: Admin.AddModel = async (body) => {
    const addModel: Admin.VehicleBrandModel = await modelServices.addModel(
      body,
    );
    return {
      message: Message.MODEL_ADDED_SUCCESSFULLY,
      responseData: { _id: addModel._id },
    };
  };

  /**
   * List car models
   * @param body
   * @returns {ReturnValue}
   */
  static list: Admin.ListModel = async (body: any) => {
    let filter: any = {};
    let responseData;
    let match =
      body?.brandId && body.brandId.length
        ? {
            isActive: true
            /**
             * Delete a model
             *
             * @param modelId The ID of the model to be deleted.
             * @returns {promise}  message
             */,
            isDeleted: false,
            brandId: {
              $in: body.brandId.map((brandId) => new ObjectId(brandId)),
            },
          }
        : { isActive: true, isDeleted: false };
    let { isDropdown } = body;
    if (isDropdown) {
      responseData = {
        list: await modelServices.listModel({ search: match }, false),
      };
    } else {
      const pagination: any = body
        ? paginationAndSorting(body, 'modelName')
        : {};
      const search =
        body.hasOwnProperty('search') && body.search
          ? searching.regexSearch(
              RegExp(body.search.trim().split(/\s+/).join('|')),
              ['brand.brandName', 'modelName'],
            )
          : '';
      filter = { ...pagination, search: { ...search, ...match } };
      const modelData: Admin.VehicleBrandModel[] =
        await modelServices.listModel(filter, true);
      let totalRecords = (
        await modelServices.listModel(
          { search: { ...search, ...match } },
          false,
        )
      ).length;
      responseData = paginateResponse(modelData, pagination, totalRecords);
    }
    return {
      message: Message.MODEL_LIST_SUCCESSFULLY,
      responseData,
    };
  };

  /**
   * Update a car model
   * @param modelId
   * @param body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateModel = async (modelId, body) => {
    const modelData: Admin.VehicleBrandModel = await modelServices.updateModel(
      modelId,
      body,
    );
    return {
      message: Message.MODEL_UPDATED_SUCCESSFULLY,
      responseData: {
        _id: modelData._id,
      },
    };
  };

  /**
   * Delete a car model
   * @param modelId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteModel = async (modelId) => {
    await modelServices.deleteModel(modelId);
    return { message: Message.MODEL_DELETED_SUCCESSFULLY };
  };
}
