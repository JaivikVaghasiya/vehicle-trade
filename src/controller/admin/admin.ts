import { Message, APPROVAL_STATUS, logger, geoLocation } from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { vehicleServices, adminServices, userServices } from '../../services';

export class adminController {
  /**
   * This function list of request to add vehicle by dealer
   * @param {String} vehicleStatus
   * @returns {ReturnType<Admin.VehicleRequests>}
   */
  static vehicleRequests: Admin.VehicleRequests = async (vehicleStatus) => {
    const getVehicle = await vehicleServices.vehicleListByFilter({
      vehicleStatus: Number(vehicleStatus),
      isActive: true,
      isDeleted: false,
    });

    return {
      message: Message.VEHICLE_LISTED_SUCCESSFULLY,
      responseData: getVehicle,
    };
  };

  /**
   * This function updates vehicle approval status
   * @param {Parameters<Admin.UpdateVehicleStatus>} body
   * @returns {ReturnValue}
   */
  static updateVehicleStatus: Admin.UpdateVehicleStatus = async (body) => {
    await adminServices.updateVehicleStatus(body);
    let message =
      body.vehicleStatus == APPROVAL_STATUS.APPROVED
        ? Message.VEHICLE_REQUEST_APPROVED_SUCCESSFULLY
        : Message.VEHICLE_REQUEST_REJECTED_SUCCESSFULLY;
    return { message };
  };

  /**
   * This function list dealer login request
   * @param {Parameters<Admin.DealerRequests>} status
   * @returns {ReturnValue}
   */
  static dealerRequests: Admin.DealerRequests = async (status) => {
    const dealerRequestsList = await userServices.userListByFilter({
      approvalStatus: Number(status),
      isActive: true,
      isDeleted: false,
    });
    return { message: Message.SUCCESSFUL, responseData: dealerRequestsList };
  };

  /**
   * This function update dealer request status as approve or reject
   * @param {Parameters<Admin.UpdateDealerStatus>} body
   * @returns {ReturnValue}
   */
  static updateDealerStatus: Admin.UpdateDealerStatus = async (body) => {
    await adminServices.updateDealerStatusService(body);
    let message =
      body.dealerStatus == APPROVAL_STATUS.APPROVED
        ? Message.DEALER_REQUEST_APPROVED_SUCCESSFULLY
        : Message.DEALER_REQUEST_REJECTED_SUCCESSFULLY;
    return { message };
  };

  /**
   * This function get address details by user location
   * @param {String} latitude latitude
   * @param {String} longitude longitude
   * @returns {ReturnValue}
   */
  static addressByLocation: Admin.AddressByLocation = async (
    latitude,
    longitude,
  ) => {
    const fullAddress = await geoLocation(latitude, longitude);
    return { message: Message.SUCCESSFUL, responseData: fullAddress };
  };
}
