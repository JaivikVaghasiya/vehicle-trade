import { Admin, ReturnValue } from '../../types';
import {
  Message,
  logger,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { attributeServices } from '../../services';

export class attributeController {
  /**
   * This function add Attributes
   * @param {Parameters<Admin.AddAttribute, "body">} body
   * @param {Parameters<Admin.AddAttribute, "files">} files
   * @returns {ReturnValue}
   */
  static add: Admin.AddAttribute = async (body, files) => {
    const addAttribute = await attributeServices.addAttribute(body, files);
    return {
      message: Message.ADD_ATTRIBUTE_SUCCESSFUL,
      responseData: { _id: addAttribute._id },
    };
  };

  /**
   * This function list Attributes
   * @param {Parameters<Admin.ListAttribute>} body
   * @returns {ReturnValue}
   */
  static list: Admin.ListAttribute = async (body: any) => {
    let filter: any = {};
    let responseData;
    let match =
      body?.flag && body.flag.length
        ? { isActive: true, isDeleted: false, flag: { $in: body.flag } }
        : { isActive: true, isDeleted: false };
    let { isDropdown } = body;
    if (isDropdown) {
      filter = { ...match };
      responseData = {
        list: await attributeServices.findAttributesByFilter(filter),
      };
    } else {
      const pagination: any = body ? paginationAndSorting(body, '_id') : {};
      const search =
        body.hasOwnProperty('search') && body.search
          ? searching.regexSearch(
              RegExp(body.search.trim().split(/\s+/).join('|')),
              [
                'attributeTypeData.attributeTypeName',
                'attributeName',
                'description',
              ],
            )
          : '';
      filter = { ...pagination, search: { ...search, ...match } };
      let getAttribute: Array<Admin.Attributes> =
        await attributeServices.listAttribute(filter, true);
      const totalRecords = (
        await attributeServices.listAttribute(
          { search: { ...search, ...match } },
          false,
        )
      ).length;
      responseData = paginateResponse(getAttribute, pagination, totalRecords);
    }
    return {
      message: Message.ATTRIBUTE_LISTED_SUCCESSFULLY,
      responseData,
    };
  };

  /**
   * This function update Attributes
   * @param {Parameters<Admin.UpdateAttribute, attributeId>} attributeId
   * @param {Parameters<Admin.UpdateAttribute, body>} body
   * @param {Parameters<Admin.UpdateAttribute, files>} files
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateAttribute = async (attributeId, body, files) => {
    const updateAttribute: Admin.Attributes =
      await attributeServices.updateAttribute(attributeId, body, files);
    return {
      message: Message.ATTRIBUTE_UPDATED_SUCCESSFULLY,
      responseData: updateAttribute._id,
    };
  };

  /**
   * This function delete Attributes
   * @param {Parameters<Admin.DeleteAttribute, attributeId>} attributeId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteAttribute = async (attributeId) => {
    await attributeServices.deleteAttribute(attributeId);
    return { message: Message.ATTRIBUTE_DELETED_SUCCESSFULLY };
  };

  static updateStatus: Admin.updateStatus = async (body) => {
    // await attributeServices.findOneAttributeByFilter
    return { message: Message.ATTRIBUTE_DELETED_SUCCESSFULLY };
  };
}
