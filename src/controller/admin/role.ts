import { Admin, ReturnValue } from '../../types';
import { roleServices } from '../../services';
import { Message } from '../../utils';

export class userRoleController {
  /**
   * add user roles
   * @param body
   * @returns {ReturnValue}
   */
  static add: Admin.AddRole = async (body) => {
    const addRole: Admin.Role = await roleServices.addRole(body);
    return {
      message: Message.ROLE_ADDED_SUCCESSFULLY,
      responseData: addRole,
    };
  };

  /**
   * list all user roles
   * @returns {ReturnValue}
   */
  static list: Admin.ListRole = async () => {
    const comments: Admin.Role[] = await roleServices.listRoles();
    return {
      message: Message.ROLE_LISTED_SUCCESSFULLY,
      responseData: comments,
    };
  };

  /**
   * updates a user role
   * @param body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateRole = async (body) => {
    await roleServices.updateRole(body.roleId, body);
    return { message: Message.ROLE_UPDATED_SUCCESSFULLY };
  };

  /**
   * deletes a user roles
   * @param roleId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteRole = async (roleId) => {
    await roleServices.deleteRole(roleId);
    return { message: Message.ROLE_DELETED_SUCCESSFULLY };
  };
}
