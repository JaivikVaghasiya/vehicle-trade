import { Message } from '../../utils';
import { Admin } from '../../types';
import { countryServices } from '../../services';

export class countryController {
  /**
   * This function is used to retrieve a add of country
   * @param body
   * @returns
   */
  static add: Admin.AddCountry = async (body) => {
    await countryServices.addCountry(body);
    return { message: Message.COUNTRY_ADDED_SUCCESSFULLY };
  };

  /**
   * This function is used to retrieve a list of country
   * @returns {promise}message,responseData
   */
  static list: Admin.ListCountry = async () => {
    const countryList: Admin.Country[] = await countryServices.listCountry();
    return {
      message: Message.COUNTRY_LIST_SUCCESSFULLY,
      responseData: countryList,
    };
  };

  /**
   * This function is used to retrieve a add of country
   * @param body
   * @param countryId
   * @returns
   */
  static update: Admin.UpdateCountry = async (countryId, body) => {
    const saveData: Admin.Country = await countryServices.updateCountry(
      countryId,
      body,
    );
    return {
      message: Message.COUNTRY_UPDATED_SUCCESSFULLY,
      responseData: { _id: saveData._id },
    };
  };

  /**
   * This function is used to retrieve a add of country
   * @param countryId
   * @returns
   */
  static delete: Admin.DeleteCountry = async (countryId) => {
    await countryServices.deleteCountry(countryId);
    return { message: Message.COUNTRY_DELETED_SUCCESSFULLY };
  };
}
