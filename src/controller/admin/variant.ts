import {
  Message,
  ObjectId,
  paginateResponse,
  paginationAndSorting,
  searching,
} from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { variantServices } from '../../services';

export class variantController {
  /**
   * add a brand variant
   * @param body
   * @returns {ReturnValue}
   */
  static add: Admin.AddVariant = async (body) => {
    const addVariant: Admin.Variant = await variantServices.addVariant(body);
    return {
      message: Message.VARIANT_ADDED_SUCCESSFULLY,
      responseData: { _id: addVariant._id },
    };
  };

  /**
   * list all variants of a brand or multiple brands
   * @param body
   * @returns {ReturnValue}
   */
  static list: Admin.ListVariant = async (body: any) => {
    console.log('body', body);
    let filter: any = {};
    let responseData;
    let match =
      body?.modelId && body.modelId.length
        ? {
            isActive: true,
            isDeleted: false,
            modelId: {
              $in: body.modelId.map((modelId) => new ObjectId(modelId)),
            },
          }
        : { isActive: true, isDeleted: false };
    let { isDropdown } = body;
    if (isDropdown) {
      responseData = {
        list: await variantServices.listVariant({ match }, false),
      };
    } else {
      const pagination: any = body
        ? paginationAndSorting(body, 'modelName')
        : {};
      const search =
        body.hasOwnProperty('search') && body.search
          ? searching.regexSearch(
              RegExp(body.search.trim().split(/\s+/).join('|')),
              ['variantName', 'model.modelName', 'brand.brandName'],
            )
          : '';
      filter = { ...pagination, match: { ...search, ...match } };
      const variantData: Admin.Variant[] = await variantServices.listVariant(
        filter,
        true,
      );
      const totalRecords = (
        await variantServices.listVariant(
          { match: { ...search, ...match } },
          false,
        )
      ).length;
      responseData = paginateResponse(variantData, pagination, totalRecords);
    }
    // query.limit = !query.limit ? totalRecords : query.limit;
    return {
      message: Message.VARIANT_LIST_SUCCESSFULLY,
      responseData,
    };
  };

  /**
   * updates a variant of a brand
   * @param variantId
   * @param body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateVariant = async (variantId, body) => {
    const variantData: Admin.Variant = await variantServices.updateVariant(
      variantId,
      body,
    );
    return {
      message: Message.VARIANT_UPDATED_SUCCESSFULLY,
      responseData: { _id: variantData._id },
    };
  };

  /**
   * delete a variant
   * @param variantId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteVariant = async (variantId) => {
    await variantServices.deleteVariant(variantId);
    return { message: Message.VARIANT_DELETED_SUCCESSFULLY };
  };
}
