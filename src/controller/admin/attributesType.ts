import { Message } from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { attributeTypeServices } from '../../services';

export class attributeTypeController {
  /**
   * This static function adds a new attribute type.
   * @param {Parameters<Admin.AddAttributeType>} body
   * @returns {ReturnValue}
   */
  static add: Admin.AddAttributeType = async (body) => {
    body.flag = `${body.attributeTypeName
      .split(/\s+/)
      .join('-')
      .toLocaleLowerCase()}-car-trade`;
    const addAttribute = await attributeTypeServices.addAttributeType(body);
    return {
      message: Message.ADD_ATTRIBUTE_SUCCESSFUL,
      responseData: { _id: addAttribute._id },
    };
  };

  /**
   * This function list a new attribute type.
   * @returns {ReturnValue}
   */
  static list = async () => {
    const listAttributeTypes = await attributeTypeServices.findAttributeTypes({
      isDeleted: false,
    });
    return {
      message: Message.LIST_ATTRIBUTE_TYPE_SUCCESSFUL,
      responseData: listAttributeTypes,
    };
  };

  /**
   * This static function updates a new attribute type.
   * @param {Parameters<Admin.UpdateAttributeType, attributeTypeId>} attributeTypeId
   * @param {Parameters<Admin.UpdateAttributeType, body>} body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateAttributeType = async (attributeTypeId, body) => {
    const checkAttribute: Admin.AttributesTypes =
      await attributeTypeServices.updateAttributeType(attributeTypeId, body);
    return {
      message: Message.ATTRIBUTE_UPDATED_SUCCESSFULLY,
      responseData: { _id: checkAttribute._id },
    };
  };

  /**
   * This function deletes attributes type
   * @param attributeTypeId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteAttributeType = async (attributeTypeId) => {
    await attributeTypeServices.deleteAttributeType(attributeTypeId);
    return { message: Message.ATTRIBUTE_DELETED_SUCCESSFULLY };
  };
}
