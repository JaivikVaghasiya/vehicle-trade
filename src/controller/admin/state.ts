import { Message, ObjectId, logger } from '../../utils';
import { Admin, ReturnValue } from '../../types';
import { countryServices, stateServices } from '../../services';
import { BadRequestError } from '../../error';

export class stateController {
  /**
   * add a state of country
   * @param body
   * @returns {ReturnValue}
   */
  static add: Admin.AddState = async (body) => {
    const addState: Admin.State = await stateServices.addState(body);
    return {
      message: Message.STATE_ADD_SUCCESSFULLY,
      ResponseData: { _id: addState._id },
    };
  };

  /**
   * updates a state
   * @param stateId
   * @param body
   * @returns {ReturnValue}
   */
  static update: Admin.UpdateState = async (stateId, body) => {
    const saveData: Admin.State = await stateServices.updateState(
      stateId,
      body,
    );
    return {
      message: Message.STATE_UPDATED_SUCCESSFULLY,
      responseData: { _id: saveData._id },
    };
  };

  /**
   * delete a state
   * @param stateId
   * @returns {ReturnValue}
   */
  static delete: Admin.DeleteState = async (stateId) => {
    await stateServices.deleteState(stateId);
    return { message: Message.STATE_DELETED_SUCCESSFULLY };
  };

  /**
   * list all state of a country
   * @param param0
   * @returns {ReturnValue}
   */
  static list: Admin.ListState = async ({ countryId, countryName }) => {
    if (!countryId && !countryName)
      throw new BadRequestError(Message.COUNTRY_NAME_IS_REQUIRED);
    const countryData = await countryServices.findCountryByFilter({
      countryName: { $regex: countryName || 'India', $options: 'i' },
    });
    let filter = countryId
      ? { countryId: new ObjectId(countryId) }
      : { countryId: countryData._id };
    const stateList: Admin.State[] = await stateServices.listState(filter);
    return {
      message: Message.STATE_LIST_SUCCESSFULLY,
      responseData: stateList,
    };
  };
}
