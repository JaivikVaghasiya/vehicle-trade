import { Schema, model, Model } from 'mongoose';
import { Mechanic } from '../../types';

//Defining Schema
const garageSchema = new Schema<Mechanic.GarageService>(
  {
    garageId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    services: {
      type: [
        {
          serviceId: Schema.Types.ObjectId,
          price: Number,
          isActive: {
            type: Boolean,
            default: true,
          },
          isDeleted: {
            type: Boolean,
            default: false,
          },
        },
      ],
      required: true,
    },
    serviceTypeId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const garageServiceModel: Model<Mechanic.GarageService> = model(
  'garage-services',
  garageSchema,
);
