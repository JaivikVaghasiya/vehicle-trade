import { Schema, model, Model } from 'mongoose';
import { Mechanic } from '../../types';

//Defining Schema
const garageSchema = new Schema<Mechanic.Garage>(
  {
    mechanicId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    garageName: {
      type: String,
      required: true,
    },
    location: {
      type: { type: String, default: 'Point' },
      coordinates: {
        type: [Number],
        index: { type: '2dsphere', sparse: false },
      },
      address: { type: String, required: true },
      countryCode: { type: String, required: true },
      country: { type: String, required: true },
      state: { type: String, required: true },
      district: { type: String, required: true },
      area: { type: String, required: false },
      city: { type: String, required: false },
      road: { type: String, required: false },
      postCode: { type: String, required: true },
    },
    garageContact: {
      type: String,
      required: true,
      trim: true,
    },
    garageEmail: {
      type: String,
      required: true,
    },
    garageCloseTime: {
      type: String,
      required: true,
    },
    garageOpenTime: {
      type: String,
      required: true,
    },
    employeeName: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const garageModel: Model<Mechanic.Garage> = model(
  'garage',
  garageSchema,
);
