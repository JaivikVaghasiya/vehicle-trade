import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';

//Defining Schema
const imageSchema = new Schema<Dealer.Image>(
  {
    vehicleId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    image: {
      type: String,
    },
    mainImage: {
      type: String,
    },
    interiorImages: {
      type: String,
    },
    exteriorImages: {
      type: String,
    },
    profileImage: {
      type: String,
    },
    policyImage: {
      type: String,
    },
    RCbookImage: {
      type: String,
    },
    document: {
      type: String,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const imageSchemaModel: Model<Dealer.Image> = model(
  'image',
  imageSchema,
);
