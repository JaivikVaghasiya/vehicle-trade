import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';

//Defining Schema
const userActivitySchema = new Schema<Dealer.UserActivity>(
  {
    dealerStoreId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      required: false,
      default: null,
    },
    vehicleId: {
      type: Schema.Types.ObjectId,
      required: true,
    },

    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const userActivityModel: Model<Dealer.UserActivity> = model(
  'userActivity',
  userActivitySchema,
);
