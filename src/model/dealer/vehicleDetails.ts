import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';

//Defining Schema
const vehicleDetailSchema = new Schema<Dealer.VehicleDetails>(
  {
    vehicleId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    fuelType: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    transmissionType: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    bodyType: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    dealerStoreId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const vehicleDetailsModel: Model<Dealer.VehicleDetails> = model(
  'VehicleDetails',
  vehicleDetailSchema,
);
