import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';
import { APPROVAL_STATUS } from '../../utils';

//Defining Schema
const vehicleSchema = new Schema<Dealer.Vehicle>(
  {
    brandId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'brands',
    },
    modelId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'models',
    },
    attributeId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'attributes',
    },
    variantId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'variants',
    },
    plateNumber: {
      type: String,
      required: true,
    },
    ownerType: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    manufactureYear: {
      type: Number,
      required: true,
    },
    vehicleStatus: {
      type: Number,
      default: APPROVAL_STATUS.PENDING,
      required: true,
    },
    colorCode: {
      type: String,
      required: true,
    },
    kiloMeters: {
      type: Number,
      required: true,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    sellDetails: {
      isSell: {
        type: Boolean,
        default: false,
      },
      userId: {
        type: Schema.Types.ObjectId,
        required: false,
        default: null,
      },
      sellingDate: {
        type: Number,
        required: false,
        default: null,
      },
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const vehicleModel: Model<Dealer.Vehicle> = model(
  'Vehicle',
  vehicleSchema,
);
