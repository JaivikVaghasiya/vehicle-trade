import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';

//Defining Schema
const dealerStoreSchema = new Schema<Dealer.DealerStore>(
  {
    dealerId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    storeName: {
      type: String,
      required: true,
    },
    storeLocation: {
      type: {
        address: { type: String, required: true },
        countryCode: { type: String, required: false },
        country: { type: String, required: true },
        state: { type: String, required: true },
        district: { type: String, required: true },
        area: { type: String, required: false },
        city: { type: String, required: false },
        road: { type: String, required: false },
        postCode: { type: String, required: true },
      },
      required: true,
    },
    storeContact: {
      type: String,
      required: true,
      trim: true,
    },
    storeEmail: {
      type: String,
      required: true,
    },
    storeCloseTime: {
      type: String,
      required: true,
    },
    storeOpenTime: {
      type: String,
      required: true,
    },
    storeDocument: {
      type: String,
      default: null,
    },
    employeeName: {
      type: String,
      default: null,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const dealerStorModel: Model<Dealer.DealerStore> = model(
  'dealerStore',
  dealerStoreSchema,
);
