import { Schema, model, Model } from 'mongoose';
import { Dealer } from '../../types';

//Defining Schema
const manualInquirySchema = new Schema<Dealer.ManualInquiryBody>(
  {
    brandId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    modelId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    userId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    fuelTypeId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    variantId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    transmissionTypeId: {
      type: Schema.Types.ObjectId,
      default: null,
    },
    inquiryStatus: [
      {
        storeId: {
          type: Schema.Types.ObjectId,
          default: null,
        },
        dueDate: {
          type: Number,
          default: null,
        },
      },
    ],
    location: {
      address: { type: String, required: true },
      countryCode: { type: String, required: true },
      country: { type: String, required: true },
      state: { type: String, required: true },
      district: { type: String, required: true },
      area: { type: String, required: false },
      city: { type: String, required: false },
      road: { type: String, required: false },
      postCode: { type: String, required: true },
    },
    priceRange: {
      type: Number,
      default: null,
    },
    isDelivered: {
      type: Boolean,
      default: null,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    manufactureYear: {
      type: Number,
      default: null,
    },
    ownerType: {
      type: Number,
      default: null,
    },
  },
  {
    timestamps: true,
  },
);

export const manualInquiryModel: Model<Dealer.ManualInquiryBody> = model(
  'manualInquiry',
  manualInquirySchema,
);
