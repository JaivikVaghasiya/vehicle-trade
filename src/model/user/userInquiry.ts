import { Schema, model, Model } from 'mongoose';
import { User } from '../../types';

const userInquirySchema = new Schema<User.UserInquiry>(
  {
    vehicleId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    dealerStoreId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      required: false,
      default: false,
    },
    isRead: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const userInquiryModel: Model<User.UserInquiry> = model(
  'user-inquiry',
  userInquirySchema,
);
