import { Schema, Model, model } from 'mongoose';
import { User } from '../../types';

const favoriteSchema = new Schema<User.Favorite>(
  {
    vehicleId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  { timestamps: true, versionKey: false },
);
export const favoriteModel: Model<User.Favorite> = model(
  'favorite',
  favoriteSchema,
);
