import { Schema, model, Model } from 'mongoose';
import { User } from '../../types';
import { ROLE, APPROVAL_STATUS } from '../../utils';

//Defining Schema
const userSchema = new Schema<User.User>(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    mobileNo: {
      type: String,
      required: true,
    },
    profileImage: {
      default: null,
      type: String,
    },
    password: {
      default: null,
      type: String,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    role: {
      type: Number,
      required: true,
      enum: [ROLE.DEALER, ROLE.USER, ROLE.MECHANIC],
    },
    approvalStatus: {
      type: Number,
      enum: [
        APPROVAL_STATUS.APPROVED,
        APPROVAL_STATUS.REJECTED,
        APPROVAL_STATUS.PENDING,
      ],
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const userModel: Model<User.User> = model('User', userSchema);
