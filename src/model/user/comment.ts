import { Schema, Model, model } from 'mongoose';
import { User } from '../../types';
const commentsSchema = new Schema<User.Comment>(
  {
    userId: {
      type: Schema.Types.ObjectId,
      require: true,
    },
    vehicleId: {
      type: Schema.Types.ObjectId,
      require: true,
    },
    comment: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  { timestamps: true, versionKey: false },
);
export const commentsModel: Model<User.Comment> = model(
  'comments',
  commentsSchema,
);
