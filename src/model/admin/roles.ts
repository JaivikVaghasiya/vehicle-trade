import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

//role Schema
const roleSchema = new Schema<Admin.Role>(
  {
    role: {
      type: String,
      required: true,
    },
    enum: {
      type: Number,
      required: false,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const roleModel: Model<Admin.Role> = model('role', roleSchema);
