import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

const modelSchema = new Schema<Admin.VehicleBrandModel>(
  {
    modelName: {
      type: String,
      required: true,
    },
    brandId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const vehicleModelsModel: Model<Admin.VehicleBrandModel> = model(
  'model',
  modelSchema,
);
