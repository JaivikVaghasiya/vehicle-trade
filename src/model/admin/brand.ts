import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

const brandSchema = new Schema<Admin.Brand>(
  {
    attributeId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    brandName: {
      type: String,
      required: true,
    },
    flag: {
      type: String,
      required: true,
    },
    logo: {
      type: String,
      default: null,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const brandModel: Model<Admin.Brand> = model('brand', brandSchema);
