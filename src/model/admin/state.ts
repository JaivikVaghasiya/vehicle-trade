import { Schema, model, Model, Types } from 'mongoose';
import { Admin } from '../../types';

//Defining Schema
const stateSchema = new Schema<Admin.State>(
  {
    stateName: {
      type: String,
      required: true,
      trim: true,
    },
    stateIsoCode: {
      type: String,
      required: true,
      trim: true,
    },
    countryId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const stateModel: Model<Admin.State> = model('state', stateSchema);
