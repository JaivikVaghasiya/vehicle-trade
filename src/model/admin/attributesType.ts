import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

//Defining Schema
const AttributesTypesSchema = new Schema<Admin.AttributesTypes>(
  {
    attributeTypeName: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      default: null,
    },
    flag: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const AttributesTypesModel: Model<Admin.AttributesTypes> = model(
  'AttributesType',
  AttributesTypesSchema,
);
