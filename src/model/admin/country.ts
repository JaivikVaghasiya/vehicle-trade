import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

//Defining Schema
const countrySchema = new Schema<Admin.Country>(
  {
    countryName: {
      type: String,
      required: true,
      trim: true,
    },
    countryIsoCode: {
      type: String,
      required: true,
      trim: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const countryModel: Model<Admin.Country> = model(
  'country',
  countrySchema,
);
