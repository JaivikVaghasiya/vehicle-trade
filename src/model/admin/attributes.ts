import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

//Defining Schema
const attributeSchema = new Schema<Admin.Attributes>(
  {
    attributeName: {
      type: String,
      required: true,
    },
    attributeTypeId: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'AttributesType',
    },
    attributeImage: {
      type: String,
      default: null,
    },
    description: {
      type: String,
      default: null,
    },
    flag: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const attributeModel: Model<Admin.Attributes> = model(
  'Attribute',
  attributeSchema,
);
