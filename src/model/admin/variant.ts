import { Schema, model, Model } from 'mongoose';
import { Admin } from '../../types';

//Defining Schema
const variantSchema = new Schema<Admin.Variant>(
  {
    variantName: {
      type: String,
      required: true,
      trim: true,
    },
    brandId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    modelId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
      default: true,
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false,
    },
    createdAt: {
      type: Number,
    },
    updatedAt: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  },
);

export const variantModel: Model<Admin.Variant> = model(
  'variant',
  variantSchema,
);
