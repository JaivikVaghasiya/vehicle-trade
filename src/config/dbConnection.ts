import { connect, set } from 'mongoose';
import { logger } from '../utils';

/**
 * Make Connection with database
 */
export const ConnectToDataBase: any = () => {
  const dataBase_URL = process.env.DATABASE_URL;
  try {
    set('strictQuery', false);
    connect(dataBase_URL, () => {
      logger.info('Database connection established');
    });
  } catch (error: any) {
    logger.info('Database connection failed', error);
  }
};
