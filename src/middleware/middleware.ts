import { validationResult } from 'express-validator';
import { RequestError } from '../error';
import { userModel } from '../model/index.js';
import {
  HttpStatus,
  Message,
  ROLE,
  send,
  ROUTES,
  verifyToken,
  logger,
} from '../utils';
import { Api, User, Utils, AsyncHandler } from '../types';

/**
 * Authenticate User
 * @param req
 * @param res
 * @param next
 * @returns next
 */
export const authenticateUser: Api = async (req: any, res, next) => {
  try {
    const path = req.baseUrl + req.route.path;
    logger.info(req.method + ' ' + path);
    const { authorization } = req.headers;
    if (ROUTES.AUTH_LESS_ROUTES.includes(path) && !authorization) {
      return next();
    }
    if (!authorization) {
      logger.warn('Invalid Token or Blank authentication');
      return send(res, HttpStatus.UNAUTHORIZE, Message.UNAUTHORIZE);
    }
    const token: string = authorization.split(' ')[1];
    let decode: Utils.DecodedToken | boolean | any = verifyToken(token);
    if (decode == true) {
      logger.warn('Token expiration error');
      return send(res, HttpStatus.UNAUTHORIZE, Message.INVALID_TOKEN);
    }
    if (
      decode.role == ROLE.ADMIN &&
      (decode._id.toString() != process.env.ADMIN_ID ||
        !ROUTES.ADMIN_ROUTES.includes(path))
    ) {
      logger.warn('Unauthorize with ADMIN routes');
      return send(res, HttpStatus.UNAUTHORIZE, Message.UNAUTHORIZE);
    }
    if (decode.role == ROLE.DEALER && !ROUTES.DEALER_ROUTES.includes(path)) {
      logger.warn('Unauthorize with DEALER routes');
      return send(res, HttpStatus.UNAUTHORIZE, Message.UNAUTHORIZE);
    }
    if (decode.role == ROLE.USER && !ROUTES.CONSUMER_ROUTES.includes(path)) {
      logger.warn('Unauthorize with USER routes');
      return send(res, HttpStatus.UNAUTHORIZE, Message.UNAUTHORIZE);
    }
    if (
      decode.role == ROLE.MECHANIC &&
      !ROUTES.MECHANIC_ROUTES.includes(path)
    ) {
      logger.warn('Unauthorize with MECHANIC routes');
      return send(res, HttpStatus.UNAUTHORIZE, Message.UNAUTHORIZE);
    }
    const user: User.User = await userModel.findOne({ _id: decode._id });
    if (!user) {
      return send(res, HttpStatus.UNAUTHORIZE, Message.USER_NOT_FOUND);
    }

    req.authUser = user;
    next();
  } catch (error: any) {
    next(error);
  }
};

/**
 * This function use to check validation
 * @param {any} req req
 * @returns {any}
 */
export function getValidationErrors(req) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    const errors = result.array();

    const firstError: any = errors[0];
    let code = HttpStatus.BAD_REQUEST;
    let msg = 'Unknown error';

    if (typeof firstError.msg === 'object') {
      code = firstError.msg.code || code;
      msg = firstError.msg.message || msg;
    } else {
      if (firstError.msg === 'Invalid value') {
        msg = `${firstError.path} is invalid.`;
      } else {
        msg = firstError.msg;
      }
    }
    throw new RequestError(code, msg, errors ? errors : undefined);
  } else {
    return null;
  }
}

/**
 * async middleware function
 * @param {any} fn function
 * @returns {any}
 */
export const asyncHandler: AsyncHandler = (fn) => {
  return function asyncUtilWrap(req: any, res, next) {
    const error = getValidationErrors(req);
    if (error) {
      return send(
        res,
        HttpStatus.BAD_REQUEST,
        Message.VALIDATION_FAILED,
        error,
      );
    } else {
      const fnReturn = fn(req, res, next);
      return Promise.resolve(fnReturn).catch(next);
    }
  };
};
