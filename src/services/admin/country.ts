import { countryModel } from '../../model';
import { Message } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData } from '../../services';

const findCountryByFilter = async (filter) => {
  return await countryModel.findOne(filter);
};
const addCountry = async (body) => {
  const { countryName }: Admin.Country = body;
  const foundItem = await countryModel.findOne({ countryName });
  if (foundItem) throw new BadRequestError(Message.COUNTRY_ALREADY_EXITS);
  return await createData([countryModel, body]);
};

const listCountry = async () => {
  return await countryModel.find({ isActive: true });
};

const updateCountry = async (countryId, body) => {
  const data: any = await findCountryByFilter({ _id: countryId });
  if (!data) throw new NotFoundError(Message.COUNTRY_NOT_FOUND);
  const checkCountry: any = await findCountryByFilter({
    _id: { $ne: countryId },
    countryName: body.countryName,
  });
  if (!checkCountry) throw new NotFoundError(Message.COUNTRY_ALREADY_EXITS);
  data.countryName = body.countryName;
  data.countryIsoCode = body.countryIsoCode;
  return await data.save();
};

const deleteCountry = async (countryId) => {
  const findCountry = await countryModel.findOne({
    _id: countryId,
  });
  if (!findCountry) throw new NotFoundError(Message.COUNTRY_NOT_FOUND);
  findCountry.isDeleted = true;
  return await countryId.save();
};

export {
  addCountry,
  listCountry,
  deleteCountry,
  findCountryByFilter,
  updateCountry,
};
