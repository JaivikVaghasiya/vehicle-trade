import { vehicleModelsModel } from '../../model';
import { Message, ObjectId, logger } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData } from '../../services';

const findModelByFilter = async (filter) => {
  return await vehicleModelsModel.findOne(filter);
};
const findAllModelByFilter = async (filter) => {
  return await vehicleModelsModel.find(filter);
};

const addModel = async (body) => {
  const checkModel: Admin.VehicleBrandModel = await findModelByFilter({
    modelName: body.modelName,
    isDeleted: false,
  });
  if (checkModel) throw new BadRequestError(Message.MODEL_ALREADY_EXITS);
  return await createData([vehicleModelsModel, body]);
};

const listModel = async (filter, isFilter) => {
  const { perPage, skipRecord, sortObj, search } = filter;
  logger.info('\n' + 'filter >>>', filter);

  return await vehicleModelsModel.aggregate(
    [
      {
        $lookup: {
          from: 'brands',
          localField: 'brandId',
          foreignField: '_id',
          as: 'brand',
        },
      },
      {
        $match:search
      },
      {
        $project: {
          modelName: 1,
          brandName: { $arrayElemAt: [`$brand.brandName`, 0] },
          brandId: 1,
          brandLogo: {
            $concat: [
              process.env.BASE_URL,
              { $arrayElemAt: ['$brand.logo', 0] },
            ],
          },
        },
      },
      ...(isFilter
        ? [{ $sort: sortObj }, { $skip: skipRecord }, { $limit: perPage }]
        : []),
    ],
    { collation: { locale: 'en' } },
  );
};

const updateModel = async (modelId, body) => {
  const modelData: Admin.VehicleBrandModel = await findModelByFilter({
    _id: modelId,
    isActive: true,
    isDeleted: false,
  });
  if (!modelData) throw new NotFoundError(Message.MODEL_NOT_FOUND);
  const checkModel: Admin.VehicleBrandModel = await findModelByFilter({
    _id: { $ne: modelId },
    modelName: body.modelName,
    isDeleted: false,
    isActive: true,
  });
  if (checkModel) throw new BadRequestError(Message.MODEL_ALREADY_EXITS);
  modelData.modelName = body.modelName;
  modelData.brandId = body.brandId;
  return await modelData.save();
};

const deleteModel = async (modelId) => {
  const findModel: Admin.VehicleBrandModel = await findModelByFilter({
    _id: modelId,
    isDeleted: false,
    isActive: true,
  });
  if (!findModel) throw new NotFoundError(Message.MODEL_NOT_FOUND);
  findModel.isDeleted = true;
  return await findModel.save();
};

export {
  findModelByFilter,
  listModel,
  addModel,
  updateModel,
  deleteModel,
  findAllModelByFilter,
};
