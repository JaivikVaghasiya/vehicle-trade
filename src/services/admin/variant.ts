import { variantModel } from '../../model';
import { Message, ObjectId } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData } from '../../services';
import { match } from 'assert';

const findVariantByFilter = async (filter) => {
  return await variantModel.findOne(filter);
};
const findAllVariantByFilter = async (filter) => {
  return await variantModel.find(filter);
};

const addVariant = async (body) => {
  const checkVariant: Admin.Variant = await findVariantByFilter({
    modelId: body.modelId,
    variantName: body.variantName,
    isDeleted: false,
  });
  if (checkVariant) throw new BadRequestError(Message.VARIANT_ALREADY_EXITS);
  return await createData([variantModel, body]);
};

const listVariant = async (filter, isFilter) => {
  const { perPage, skipRecord, sortObj, match } = filter;
  return await variantModel.aggregate(
    [
      {
        $lookup: {
          from: 'models',
          localField: 'modelId',
          foreignField: '_id',
          as: 'model',
        },
      },
      {
        $lookup: {
          from: 'brands',
          localField: 'brandId',
          foreignField: '_id',
          as: 'brand',
        },
      },
      {
        $match: match
      
      },
      {
        $project: {
          variantName: 1,
          brandName: { $arrayElemAt: [`$brand.brandName`, 0] },
          modelName: { $arrayElemAt: [`$model.modelName`, 0] },
          modelId: 1,
          brandId: 1,
        },
      },
      ...(isFilter
        ? [{ $sort: sortObj }, { $skip: skipRecord }, { $limit: perPage }]
        : []),
    ],
    { collation: { locale: 'en' } },
  );
};

const updateVariant = async (variantId, body) => {
  const { variantName, modelId, brandId }: Admin.Variant = body;
  const variantData: Admin.Variant = await findVariantByFilter({
    _id: variantId,
    isActive: true,
    isDeleted: false,
  });
  if (!variantData) throw new NotFoundError(Message.VARIANT_NOT_FOUND);
  const checkVariant: Admin.Variant = await findVariantByFilter({
    _id: { $ne: variantId },
    variantName: variantName,
    isDeleted: false,
    isActive: true,
  });
  if (checkVariant) throw new BadRequestError(Message.VARIANT_ALREADY_EXITS);
  variantData.variantName = variantName;
  variantData.modelId = modelId;
  variantData.brandId = brandId;
  return await variantData.save();
};

const deleteVariant = async (variantId) => {
  const findVariant: Admin.Variant = await findVariantByFilter({
    _id: variantId,
    isDeleted: false,
    isActive: true,
  });
  if (!findVariant) throw new NotFoundError(Message.VARIANT_NOT_FOUND);
  findVariant.isDeleted = true;
  return await findVariant.save();
};

export {
  findVariantByFilter,
  addVariant,
  updateVariant,
  listVariant,
  deleteVariant,
  findAllVariantByFilter,
};
