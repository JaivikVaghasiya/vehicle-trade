import { stateModel } from '../../model';
import { Message, ObjectId, logger } from '../../utils';
import { BadRequestError, NotFoundError } from '../../error';
import { createData } from '../../services';

const findStateByFilter = async (filter) => {
  return await stateModel.findOne(filter);
};

const addState = async (body) => {
  const foundItem = await findStateByFilter({ stateName: body.stateName });
  if (foundItem) throw new BadRequestError(Message.STATE_ALREADY_EXITS);
  return await createData([stateModel, body]);
};

const listState = async (filter) => {
  return await stateModel.aggregate([
    {
      $match: filter,
    },
    {
      $lookup: {
        from: 'countries',
        localField: 'countryId',
        foreignField: '_id',
        as: 'country',
      },
    },
    {
      $project: {
        stateName: 1,
        stateIsoCode: 1,
        countryId: 1,
        country: { $arrayElemAt: ['$country.countryName', 0] },
        countryIsoCode: { $arrayElemAt: ['$country.countryIsoCode', 0] },
      },
    },
  ]);
};

const updateState = async (stateId, body) => {
  const { stateName, stateIsoCode, countryId } = body;
  const stateData: any = await findStateByFilter({ _id: stateId });
  if (!stateData) throw new NotFoundError(Message.STATE_NOT_FOUND);
  const checkState: any = await findStateByFilter({
    _id: { $ne: stateId },
    stateName,
  });
  if (!checkState) throw new NotFoundError(Message.STATE_NOT_FOUND);
  stateData.stateName = stateName;
  stateData.stateIsoCode = stateIsoCode;
  stateData.countryId = countryId;
  return await stateData.save();
};

const deleteState = async (stateId) => {
  const findState = await findStateByFilter({ _id: stateId });
  if (!findState) throw new BadRequestError(Message.STATE_NOT_FOUND);
  findState.isDeleted = true;
  return await findState.save();
};

export { findStateByFilter, addState, updateState, deleteState, listState };
