import { AttributesTypesModel } from '../../model';
import { Message, logger } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';

const findOneAttributeTypeByFilter = async (filter) => {
  return await AttributesTypesModel.findOne(filter);
};
const findAttributeTypes = async (filter) => {
  return await AttributesTypesModel.aggregate([
    { $match: filter },
    {
      $project: {
        _id: 1,
        attributeTypeName: 1,
        flag: 1,
        description: {
          $cond: [{ $ifNull: ['$description', false] }, '$description', null],
        },
      },
    },
  ]);
};

const addAttributeType = async (body) => {
  logger.info('\n' + 'body >>>', body);
  const checkAttributeType = await findOneAttributeTypeByFilter({
    attributeTypeName: body.attributeTypeName,
  });
  if (checkAttributeType)
    throw new BadRequestError(Message.ATTRIBUTE_TYPE_ALREADY_EXITS);
  const addAttribute: Admin.AttributesTypes = await AttributesTypesModel.create(
    {
      attributeTypeName: body.attributeTypeName,
      description: body.description,
      flag: body.flag,
    },
  );
  return addAttribute;
};

const updateAttributeType = async (attributeTypeId, body) => {
  const checkAttribute: Admin.AttributesTypes =
    await findOneAttributeTypeByFilter({
      _id: attributeTypeId,
    });
  if (!checkAttribute)
    throw new NotFoundError(Message.ATTRIBUTES_TYPE_NOT_FOUND);
  const checkUser: Admin.AttributesTypes = await findOneAttributeTypeByFilter({
    _id: { $ne: attributeTypeId },
    attributeTypeName: body.attributeTypeName,
    isDeleted: false,
  });
  if (checkUser) throw new BadRequestError(Message.ATTRIBUTE_ALREADY_EXITS);
  checkAttribute.attributeTypeName = body.attributeTypeName;
  checkAttribute.description = body.description;
  checkAttribute.flag = checkAttribute.flag;
  return await checkAttribute.save();
};

const deleteAttributeType = async (attributeTypeId) => {
  const findAttributeType: Admin.AttributesTypes =
    await findOneAttributeTypeByFilter({
      _id: attributeTypeId,
    });
  if (!findAttributeType)
    throw new NotFoundError(Message.ATTRIBUTE_TYPE_NOT_FOUND);
  findAttributeType.isDeleted = true;
  return await findAttributeType.save();
};

export {
  findOneAttributeTypeByFilter,
  addAttributeType,
  updateAttributeType,
  deleteAttributeType,
  findAttributeTypes,
};
