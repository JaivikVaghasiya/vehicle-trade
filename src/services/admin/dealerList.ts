import {
  dealerStorModel,
  userModel,
  vehicleDetailsModel,
  vehicleModel,
} from '../../model';

const dealerListFindByFilter = async (filter, _id) => {
  const { search, sortObj, skipRecord, perPage, role } = filter;
  return await userModel.aggregate(
    [
      {
        $lookup: {
          from: 'dealerstores',
          localField: '_id',
          foreignField: 'dealerId',
          as: 'dealerData',
        },
      },
      {
        $lookup: {
          from: 'vehicledetails',
          localField: 'dealerData._id',
          foreignField: 'dealerStoreId',
          as: 'vehicleDetailsData',
        },
      },
      {
        $lookup: {
          from: 'vehicles',
          let: { id: '$vehicleDetailsData.vehicleId' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $in: ['$_id', '$$id'] },
                    { $eq: ['$sellDetails.isSell', true] },
                  ],
                },
              },
            },
            {
              $project: {
                _id: 0,
              },
            },
          ],
          as: 'vehicleData',
        },
      },
      {
        $match: {
          role: role,
          ...search,
          isDeleted: false,
        },
      },
      {
        $project: {
          firstName: 1,
          lastName: 1,
          email: 1,
          mobileNo: 1,
          profileImage: 1,
          isActive: 1,
          isDeleted: 1,
          storeName: '$dealerData.storeName',
          revenue: { $sum: '$vehicleData.price' },
          saleCount: { $size: '$vehicleData' },
        },
      },
      { $sort: sortObj },
      { $skip: skipRecord },
      { $limit: perPage },
    ],
    { collation: { locale: 'en' } },
  );
};

const deactivateDealer = async (body) => {
  await userModel.updateOne(
    { _id: body.dealerId },
    { $set: { isActive: body.isActive } },
  );
  let deactivateUser = await userModel.findOne({ _id: body.dealerId });

  await dealerStorModel.updateMany(
    { dealerId: deactivateUser._id },
    { $set: { isActive: body.isActive } },
  );

  let dealerStoreId = await dealerStorModel.find({ dealerId: body.dealerId });
  let vehicleIdData = await vehicleDetailsModel.find({
    dealerStoreId,
  });
  const vehicleId = vehicleIdData.map((value) => value.vehicleId);
  await vehicleModel.updateMany(
    { _id: vehicleId },
    { $set: { isActive: body.isActive } },
  );
};

export { dealerListFindByFilter, deactivateDealer };
