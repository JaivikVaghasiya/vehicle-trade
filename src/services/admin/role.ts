import { roleModel } from '../../model';
import { Message } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData } from '../../services';

const findOneRoleByFilter = async (filter) => {
  return await roleModel.findOne(filter);
};

const addRole = async (body) => {
  const { role }: Admin.Role = body;
  const foundItem = await findOneRoleByFilter({ role });
  if (foundItem) throw new BadRequestError(Message.ROLE_ALREADY_EXIST);
  const increment = (await roleModel.find({})).length;
  body.enum = increment + 1;
  return await createData([roleModel, body]);
};

const listRoles = async () => {
  return await roleModel.find({ isActive: true, isDeleted: false });
};

const updateRole = async (roleId, body) => {
  const data: any = await findOneRoleByFilter({ _id: roleId });
  if (!data) throw new NotFoundError(Message.ROLE_NOT_FOUND);
  const checkRole: any = await findOneRoleByFilter({
    _id: { $ne: roleId },
    role: body.role,
  });
  if (!checkRole) throw new NotFoundError(Message.ROLE_ALREADY_EXIST);
  data.role = body.role;
  return await data.save();
};

const deleteRole = async (roleId) => {
  const findRole = await roleModel.findOne({
    _id: roleId,
  });
  if (!findRole) throw new NotFoundError(Message.ROLE_NOT_FOUND);
  findRole.isDeleted = true;
  return await roleId.save();
};

export { addRole, listRoles, deleteRole, findOneRoleByFilter, updateRole };
