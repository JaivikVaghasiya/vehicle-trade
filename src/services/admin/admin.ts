import { BadRequestError } from '../../error';
import { Dealer } from '../../types';
import { Message, ObjectId } from '../../utils';
import { vehicleDetailsServices } from '..';
import { userFindOneByFilter } from '../user/user';

const updateVehicleStatus = async (body) => {
  const checkVehicle: Dealer.Vehicle =
    await vehicleDetailsServices.findOneVehicleByFilter({
      _id: body.vehicleId,
    });
  if (!checkVehicle) throw new BadRequestError(Message.VEHICLE_NOT_FOUND);
  checkVehicle.vehicleStatus = body.vehicleStatus;
  await checkVehicle.save();
  return checkVehicle;
};

const updateDealerStatusService = async (body) => {
  const checkDealer: Dealer.Vehicle | any = await userFindOneByFilter({
    _id: new ObjectId(body.userId),
  });
  if (!checkDealer) throw new BadRequestError(Message.USER_NOT_FOUND);
  checkDealer.approvalStatus = body.dealerStatus;
  await checkDealer.save();
  return checkDealer;
};

export { updateVehicleStatus, updateDealerStatusService };
