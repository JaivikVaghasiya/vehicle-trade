import { attributeModel } from '../../model';
import {
  Message,
  ObjectId,
  deleteFile,
  logger,
  uploadImage,
} from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData, attributeTypeServices } from '..';

const findOneAttributeByFilter = async (filter) => {
  return await attributeModel.findOne(filter);
};

const findAttributesByFilter = async (filter) => {
  return await attributeModel.find(filter, {
    attributeImage: { $concat: [process.env.BASE_URL, '$attributeImage'] },
    attributeName: 1,
    flag: 1,
    attributeTypeId: 1,
    description: 1,
  });
};

const addAttribute = async (body, files) => {
  const { attributeName, attributeTypeId }: Admin.Attributes = body;
  let checkAttribute: Admin.Attributes = await findOneAttributeByFilter({
    attributeName,
  });
  if (checkAttribute) throw new BadRequestError(Message.ATTRIBUTES_ALREADY_ADD);
  const checkAttributeType =
    await attributeTypeServices.findOneAttributeTypeByFilter({
      _id: attributeTypeId,
    });
  if (!checkAttributeType)
    throw new BadRequestError(Message.ATTRIBUTES_NOT_FOUND);
  body.flag = checkAttributeType.flag;
  let array = [];
  files &&
    files.image &&
    array.push({ key: 'attributeImage', value: files.image });
  const addAttributeTypes = await createData(
    [attributeModel, body],
    array,
    true,
  );
  return await addAttributeTypes.save();
};

const listAttribute = async (filter, isFilter) => {
  const { perPage, skipRecord, sortObj, search } = filter;
  return await attributeModel.aggregate(
    [
      {
        $lookup: {
          from: 'attributestypes',
          localField: 'attributeTypeId',
          foreignField: '_id',
          as: 'attributeTypeData',
        },
      },
      {
        $match: {
          isActive: true,
          isDeleted: false,
          ...search,
        },
      },
      {
        $project: {
          attributeTypeName: {
            $arrayElemAt: [`$attributeTypeData.attributeTypeName`, 0],
          },
          attributeName: 1,
          attributeTypeId: 1,
          attributeImage: {
            $concat: [process.env.BASE_URL, '$attributeImage'],
          },
          flag: 1,
          description: {
            $cond: [{ $ifNull: ['$description', false] }, '$description', null],
          },
        },
      },
      ...(isFilter
        ? [{ $sort: sortObj }, { $skip: skipRecord }, { $limit: perPage }]
        : []),
    ],
    { collation: { locale: 'en' } },
  );
};

const updateAttribute = async (attributeId, body, files) => {
  let findAttribute: Admin.Attributes = await findOneAttributeByFilter({
    _id: attributeId,
  });
  if (!findAttribute) throw new NotFoundError(Message.ATTRIBUTES_NOT_FOUND);
  let checkAttribute: Admin.Attributes = await findOneAttributeByFilter({
    _id: { $ne: attributeId },
    attributeName: body.attributeName,
    isDeleted: false,
  });
  if (checkAttribute)
    throw new BadRequestError(Message.ATTRIBUTE_ALREADY_EXITS);
  findAttribute.attributeName =
    body.attributeName || findAttribute.attributeName;
  findAttribute.description = body.description || findAttribute.description;
  findAttribute.flag = body.flag || findAttribute.flag;

  if (files) {
    await deleteFile(findAttribute.attributeImage, findAttribute._id);
    let image = uploadImage(
      [{ value: files.image, key: 'image' }],
      findAttribute._id,
    );
    findAttribute.attributeImage = `${image[0].image}`;
  }
  return await findAttribute.save();
};

export const deleteAttribute = async (attributeId) => {
  const findAttribute: Admin.Attributes = await findOneAttributeByFilter({
    _id: attributeId,
  });
  if (!findAttribute) throw new NotFoundError(Message.ATTRIBUTES_NOT_FOUND);
  findAttribute.isDeleted = true;
  return await findAttribute.save();
};

export {
  addAttribute,
  findOneAttributeByFilter,
  listAttribute,
  updateAttribute,
  findAttributesByFilter,
};
