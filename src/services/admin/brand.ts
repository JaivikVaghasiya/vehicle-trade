import { brandModel } from '../../model';
import {
  deleteFile,
  uploadImage,
  Message,
  ObjectId,
  logger,
} from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { attributeServices, createData } from '../../services';

const findBrandByFilter = async (filter) => {
  return await brandModel.findOne(filter);
};

const findAllBrandByFilter = async (filter) => {
  return await brandModel.find(filter, {
    brandLogo: { $concat: [process.env.BASE_URL, '$logo'] },
    brandName: 1,
    flag: 1,
    attributeId: 1,
  });
};
const addBrand = async (body, files) => {
  const checkBrand: Admin.Brand | null = await findBrandByFilter({
    brandName: body.brandName,
    isDeleted: false,
  });
  if (checkBrand) throw new BadRequestError(Message.BRAND_ALREADY_EXITS);
  const checkVehicleType = await attributeServices.findOneAttributeByFilter({
    _id: body.attributeId,
  });
  if (!checkVehicleType)
    throw new BadRequestError(Message.VEHICLE_TYPE_NOT_FOUND);
  body.flag = `vehicle-type-${checkVehicleType.attributeName
    .split(/\s+/)
    .join('-')
    .toLocaleLowerCase()}`;
  logger.info('\n' + 'body >>>', body);
  let array = [];
  files && files.logo && array.push({ key: 'logo', value: files.logo });
  return await createData([brandModel, body], array, true);
};

const listBrand = async (filter, isFilter) => {
  const { perPage, skipRecord, sortObj, match } = filter;
  console.log("match>>", match)
  console.log('sortObj>>>', sortObj);
  return await brandModel.aggregate(
    [
      {
        $lookup: {
          from: 'attributes',
          localField: 'attributeId',
          foreignField: '_id',
          as: 'attribute',
        },
      },
      {
        $match: {
          isActive: true,
          isDeleted: false,
          ...match,
        },
      },
      {
        $project: {
          vehicleType: { $arrayElemAt: [`$attribute.attributeName`, 0] },
          brandName: 1,
          attributeId: 1,
          attributeName: 1,
          brandLogo: { $concat: [process.env.BASE_URL, '$logo'] },
        },
      },
      ...(isFilter
        ? [{ $sort: sortObj }, { $skip: skipRecord }, { $limit: perPage }]
        : []),
    ],
    { collation: { locale: 'en' } },
  );
};

const updateBrand = async (brandId, body, files) => {
  const { attributeId, brandName } = body;
  const brandData: Admin.Brand = await findBrandByFilter({
    _id: brandId,
    isActive: true,
  });
  if (!brandData) throw new NotFoundError(Message.BRAND_NOT_FOUND);
  const checkBrand: Admin.Brand = await findBrandByFilter({
    _id: { $ne: brandId },
    brandName,
    isDeleted: false,
    isActive: true,
  });
  if (checkBrand) throw new BadRequestError(Message.BRAND_ALREADY_EXITS);
  const checkVehicleType = await attributeServices.findOneAttributeByFilter({
    _id: body.attributeId,
  });
  logger.info('\n' + 'checkVehicleType >>>', checkVehicleType);
  if (!checkVehicleType)
    throw new BadRequestError(Message.VEHICLE_TYPE_NOT_FOUND);
  body.flag = `vehicle-type-${checkVehicleType.attributeName
    .split(/\s+/)
    .join('-')
    .toLocaleLowerCase()}`;
  logger.info('\n' + 'body >>>', body);
  brandData.brandName = brandName;
  brandData.attributeId = attributeId;
  brandData.flag = brandData.flag;

  if (files) {
    await deleteFile(brandData.logo, brandData._id);
    const logo = uploadImage(
      [{ value: files.logo, key: 'image' }],
      brandData._id,
    );
    brandData.logo = logo[0].image;
  }
  return await brandData.save();
};

const deleteBrand = async (brandId) => {
  const findBrand: Admin.Brand = await findBrandByFilter({
    _id: brandId,
    isDeleted: false,
    isActive: true,
  });
  if (!findBrand) throw new NotFoundError(Message.BRAND_NOT_FOUND);
  findBrand.isDeleted = true;
  return await findBrand.save();
};

export {
  addBrand,
  findBrandByFilter,
  listBrand,
  updateBrand,
  deleteBrand,
  findAllBrandByFilter,
};
