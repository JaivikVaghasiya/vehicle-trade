export const vehicleLookup = [
  {
    $lookup: {
      from: 'vehicledetails',
      localField: '_id',
      foreignField: 'dealerStoreId',
      as: 'vehicleDetails',
    },
  },
   { $unwind: '$vehicleDetails' },
  {
    $lookup: {
      from: 'vehicles',
      localField: 'vehicleDetails.vehicleId',
      foreignField: '_id',
      as: 'vehicleData',
    },
  },
   { $unwind: '$vehicleData' },
];

export const commonLookup = [
  {
    $lookup: {
      from: 'images',
      localField: 'vehicleData._id',
      foreignField: 'vehicleId',
      as: 'imageData',
    },
  },
  {
    $lookup: {
      from: 'variants',
      let: { id: '$vehicleData.variantId' },
      pipeline: [
        { $match: { $expr: { $and: { $eq: ['$$id', '$_id'] } } } },
        { $project: { _id: 0, modelId: 1, variantName: 1 } },
        {
          $lookup: {
            from: 'models',
            let: { id: '$modelId' },
            pipeline: [
              { $match: { $expr: { $and: { $eq: ['$_id', '$$id'] } } } },
              { $project: { _id: 0, modelName: 1, brandId: 1 } },
              {
                $lookup: {
                  from: 'brands',
                  let: { id: '$brandId' },
                  pipeline: [
                    {
                      $match: {
                        $expr: { $and: { $eq: ['$_id', '$$id'] } },
                      },
                    },
                    { $project: { _id: 0, brandName: 1 } },
                  ],
                  as: 'brand',
                },
              },
              { $unwind: '$brand' },
            ],
            as: 'model',
          },
        },
        { $unwind: '$model' },
      ],
      as: 'variant',
    },
  },
  { $unwind: '$variant' },
];

export const storeProject = {
  storeName: 1,
  storeContact: 1,
  storeEmail: 1,
  storeLocation: 1,
};

export const commonProject = {
  vehicleId: '$vehicleData._id',
  vehicleName: {
    $concat: [
      '$variant.model.brand.brandName',
      ' ',
      '$variant.model.modelName',
      ' ',
      '$variant.variantName',
    ],
  },
  brandId: '$vehicleData.brandId',
  modelId: '$vehicleData.modelId',
  variantId: '$vehicleData.variantId',
  attributeId: '$vehicleData.attributeId',
  kiloMeters: '$vehicleData.kiloMeters',
  ownerType: '$vehicleData.ownerType',
  price: '$vehicleData.price',
  colorCode: '$vehicleData.colorCode',
  manufactureYear: '$vehicleData.manufactureYear',
  plateNumber: '$vehicleData.plateNumber',
  mainImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$imageData.mainImage', 0] },
    ],
  },
};

export const userCommonProject = {
  userName: {
    $concat: ['$userData.firstName', ' ', '$userData.lastName'],
  },
  userEmail: '$userData.email',
  userPhone: '$userData.mobileNo',
  userProfileImage: {
    $cond: [
      { $gt: [{ $strLenCP: '$userData.profileImage' }, 0] },
      {
        $concat: [process.env.BASE_URL, '$userData.profileImage'] || null,
      },
      null,
    ],
  },
  userId: '$userData._id',
};

export const userCommonLookup = [
  {
    $lookup: {
      from: 'users',
      localField: 'vehicleData.sellDetails.userId',
      foreignField: '_id',
      as: 'userData',
    },
  },
  { $unwind: '$userData' },
];

export const group = {
  $group: {
    _id: '$_id',
    storeName: { $first: '$storeName' },
    storeContact: { $first: '$storeContact' },
    storeEmail: { $first: '$storeEmail' },
    storeLocation: { $first: '$storeLocation' },
    vehicleData: {
      $push: '$data',
    },
  },
};

export const countFilter = (start, end) => {
  return {
    $filter: {
      input: '$vehicleData',
      as: 'vehicle',
      cond: {
        $and: [
          {
            $gte: ['$$vehicle.sellDetails.sellingDate', start],
          },
          {
            $lte: ['$$vehicle.sellDetails.sellingDate', end],
          },
        ],
      },
    },
  };
};
