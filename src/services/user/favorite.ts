import { favoriteModel } from '../../model';
import { Message, logger } from '../../utils';
import { User, Dealer } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData, vehicleDetailsServices, vehicleServices } from '..';

const findOneFavoriteByFilter = async (filter) => {
  return await favoriteModel.findOne(filter);
};

const addFavorite = async (body: any) => {
  const findVehicle: Dealer.Vehicle =
    await vehicleDetailsServices.findOneVehicleByFilter({
      _id: body.vehicleId,
    });
  if (!findVehicle) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  const checkFavorite = await findOneFavoriteByFilter({
    vehicleId: body.vehicleId,
    userId: body.userId,
  });
  if (checkFavorite)
    throw new BadRequestError(Message.ALREADY_ADDED_TO_FAVORITE);
  return await createData([favoriteModel, body]);
};

const listFavorite = async (userId) => {
  let filter = {
    'favoriteData.userId': userId,
  };
  let vehicleData: Array<Dealer.Vehicle> =
    await vehicleServices.vehicleListByFilter(filter, userId);
  return vehicleData;
};

const deleteFavorite = async (favoriteId) => {
  const checkFavorite: User.Favorite | null = await findOneFavoriteByFilter({
    _id: favoriteId,
  });
  if (!checkFavorite) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  checkFavorite.isFavorite = false;
  return await checkFavorite.save();
};

export { findOneFavoriteByFilter, addFavorite, deleteFavorite, listFavorite };
