import { NotFoundError } from '../../error';
import { User } from '../../types';
import { commentsModel } from '../../model';
import { createData } from '..';
import { Message } from '../../utils';

const findOneCommentByFilter = async (filter) => {
  return await commentsModel.findOne(filter);
};

const addComment = async (body) => {
  return await createData([commentsModel, body]);
};

const listComment = async (vehicleId) => {
  return await commentsModel.aggregate([
    {
      $match: { vehicleId },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'userId',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $project: {
        userName: { $arrayElemAt: ['$user.name', 0] },
        email: { $arrayElemAt: ['$user.email', 0] },
        comment: 1,
        updatedAt: 1,
      },
    },
  ]);
};

const updateComment = async (commentId, body) => {
  const commentsData: User.Comment = await findOneCommentByFilter({
    _id: commentId,
  });
  if (!commentsData) throw new NotFoundError(Message.COMMENTS_NOT_FOUND);
  commentsData.comment = body.comment;
  return await commentsData.save();
};

const deleteComment = async (commentId) => {
  const commentData: User.Comment = await findOneCommentByFilter({
    _id: commentId,
  });
  if (!commentData) throw new NotFoundError(Message.COMMENTS_NOT_FOUND);
  commentData.isDeleted = true;
  return await commentData.save();
};

export {
  findOneCommentByFilter,
  addComment,
  listComment,
  updateComment,
  deleteComment,
};
