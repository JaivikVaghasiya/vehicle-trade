import {
  createData,
  dealerStoreServices,
  vehicleDetailsServices,
} from './../../services';
import { NotFoundError } from '../../error';
import { Message, ObjectId } from '../../utils';
import { userInquiryModel } from './../../model';

const addUserInquiry = async (body) => {
  const { vehicleId, dealerStoreId } = body;
  const checkVehicle = await vehicleDetailsServices.findOneVehicleByFilter({
    _id: vehicleId,
  });
  if (!checkVehicle) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  const checkDealer = await dealerStoreServices.findDealerStoreByFilter({
    _id: dealerStoreId,
  });
  if (!checkDealer) throw new NotFoundError(Message.DEALER_NOT_FOUND);
  return await createData([userInquiryModel, body]);
};

const listUserInquiry = async (userId) => {
  return await userInquiryModel.aggregate([
    {
      $lookup: {
        from: 'vehicles',
        localField: 'vehicleId',
        foreignField: '_id',
        as: 'vehicleData',
      },
    },
    { $unwind: '$vehicleData' },
    {
      $match: {
        userId: new ObjectId(userId),
        isDeleted: false,
      },
    },
    {
      $lookup: {
        from: 'brands',
        localField: 'vehicleData.brandId',
        foreignField: '_id',
        as: 'brandData',
      },
    },
    {
      $lookup: {
        from: 'models',
        localField: 'vehicleData.modelId',
        foreignField: '_id',
        as: 'modelData',
      },
    },
    {
      $lookup: {
        from: 'variants',
        localField: 'vehicleData.variantId',
        foreignField: '_id',
        as: 'variantData',
      },
    },
    {
      $lookup: {
        from: 'images',
        localField: 'vehicleId',
        foreignField: 'vehicleId',
        as: 'imageData',
      },
    },
    {
      $project: {
        vehicleName: {
          $concat: [
            { $arrayElemAt: ['$brandData.brandName', 0] },
            ' ',
            { $arrayElemAt: ['$modelData.modelName', 0] },
            ' ',
            { $arrayElemAt: ['$variantData.variantName', 0] },
          ],
        },
        vehicleId: '$vehicleData._id',
        vehicleColor: '$vehicleData.colorCode',
        ownerType: '$vehicleData.ownerType',
        plateNumber: '$vehicleData.plateNumber',
        userId: userId,
        approveStatus: {
          $cond: [
            {
              $and: [
                {
                  $eq: ['$vehicleData.sellDetails.userId', userId],
                },
              ],
            },
            true,
            {
              $cond: {
                if: { $eq: ['$vehicleData.sellDetails.isSell', true] },
                then: false,
                else: 'pending',
              },
            },
          ],
        },
        mainImage: {
          $concat: [
            process.env.BASE_URL,
            { $arrayElemAt: ['$imageData.mainImage', 0] },
          ],
        },
        isSoldOut: '$vehicleData.sellDetails.isSell',
      },
    },
  ]);
};

const removeUserInquiry = async (id) => {
  const checkInquiry = await userInquiryModel.findOne({ _id: id });
  if (!checkInquiry) throw new NotFoundError(Message.INQUIRY_NOT_FOUND);
  checkInquiry.isDeleted = true;
  return await checkInquiry.save();
};

export { addUserInquiry, removeUserInquiry, listUserInquiry };
