import { imageSchemaModel } from '../model';
import { logger, uploadImage,generateToken } from '../utils';

type CreateData = (
  [model, data]: any,
  file?: any,
  condition?: boolean,
  id?: any,
) => any;

export const createData: CreateData = async (
  [model, data],
  file,
  condition,
  id,
) => {
  try {
    const addData = new model(data);
    id = addData._id || (id && id);
    if (file && file.length > 0) {
      if (condition) {
        let image = await uploadImage(file, id);
        let key = file[0].key ? file[0].key : 'image';
        addData[key] = `${image[0][key]}`;
      } else {
        let image = await uploadImage(file, id);
        await imageSchemaModel.insertMany(image);
      }
    }
    return await addData.save();
  } catch (error) {
    throw error;
  }
};

// Admin Services
export * as adminServices from './admin/admin';
export * as attributeServices from './admin/attribute';
export * as attributeTypeServices from './admin/attributeType';
export * as brandServices from './admin/brand';
export * as countryServices from './admin/country';
export * as modelServices from './admin/model';
export * as stateServices from './admin/state';
export * as variantServices from './admin/variant';
export * as roleServices from './admin/role';
export * as dealerListServices from './admin/dealerList';


// Dealer Services
export * as dealerServices from './dealer/dealer';
export * as vehicleServices from './dealer/vehicle';
export * as vehicleDetailsServices from './dealer/vehicleDetails';
export * as dealerStoreServices from './dealer/dealerStore';
export * as dealerDashboardServices from './dealer/dealerDashboard';
export * as userActivityServices from './dealer/userActivity';
export * as manualInquiryServices from './dealer/manualInquiry';

// User Services
export * as userServices from './user/user';
export * as commentServices from './user/comment';
export * as favoriteServices from './user/favorite';
export * as userInquiryServices from './user/userInquiry';



// Garage Services
export * as garageServices from './mechanic/garage';
export * as garageServiceServices from './mechanic/garageServices';
