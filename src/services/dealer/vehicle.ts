import {
  vehicleModel,
  imageSchemaModel,
  vehicleDetailsModel,
} from '../../model';
import { Message, uploadImage, logger, deleteFile } from '../../utils';
import { Dealer } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import { createData, vehicleDetailsServices } from '..';

const vehicleFullListByFilter = async (filter: any, isWithIds) => {
  return await vehicleModel.aggregate(
    isWithIds
      ? [
          { $match: filter },
          ...vehicleDetailsServices.vehicleNameLookup,
          ...vehicleDetailsServices.attributeTypeLookup,
          ...vehicleDetailsServices.imageFields,
          {
            $project: {
              ...vehicleDetailsServices.vehicleProject,
              ...vehicleDetailsServices.vehicleDetailsProject,
              ...vehicleDetailsServices.imageProject,
              brandId: 1,
              modelId: 1,
              variantId: 1,
              attributeId: 1,
              transmissionTypeId: {
                $arrayElemAt: ['$vehicleData.transmissionType', 0],
              },
              bodyTypeId: { $arrayElemAt: ['$vehicleData.bodyType', 0] },
              fuelTypeId: { $arrayElemAt: ['$vehicleData.fuelType', 0] },
              dealerStoreId: {
                $arrayElemAt: ['$vehicleData.dealerStoreId', 0],
              },
            },
          },
        ]
      : [
          { $match: filter },
          ...vehicleDetailsServices.vehicleNameLookup,
          ...vehicleDetailsServices.attributeTypeLookup,
          {
            $project: {
              ...vehicleDetailsServices.vehicleProject,
              ...vehicleDetailsServices.vehicleDetailsProject,
              ...vehicleDetailsServices.simpleImageProject,
            },
          },
        ],
  );
};

const vehicleListByFilter = async (filter, userId?) => {
  let vehicleData: Array<Dealer.Vehicle> = await vehicleModel.aggregate([
    ...vehicleDetailsServices.vehicleNameLookup,
    {
      $lookup: {
        from: 'favorites',
        localField: '_id',
        foreignField: 'vehicleId',
        as: 'favoriteData',
      },
    },
    { $unwind: '$locationData' },
    {
      $match: filter ? filter : {},
    },
    {
      $project: {
        ...vehicleDetailsServices.vehicleProject,
        isFavorite: {
          $cond: [
            { $eq: [{ $arrayElemAt: ['$favoriteData.userId', 0] }, userId] },
            true,
            false,
          ],
        },
        location: {
          $concat: [
            '$locationData.storeLocation.state',
            ', ',
            '$locationData.storeLocation.country',
          ],
        },
      },
    },
  ]);
  return vehicleData;
};

const addVehicle = async (vehicleBody, vehicleDetailsBody, files) => {
  await vehicleDetailsServices.vehicleValidations({
    ...vehicleBody,
    ...vehicleDetailsBody,
  });
  const { mainImage, interiorImages, exteriorImages } = files;
  let array = [];
  if (files) {
    files.policyImage &&
      array.push({ key: 'policyImage', value: files.policyImage });
    files.RCbookImage &&
      array.push({ key: 'RCbookImage', value: files.RCbookImage });
  }

  const vehicleData = await createData(
    [vehicleModel, vehicleBody],
    [
      { value: mainImage, key: 'mainImage' },
      { value: interiorImages, key: 'interiorImages' },
      { value: exteriorImages, key: 'exteriorImages' },
    ],
    false,
  );
  logger.info('\n' + 'vehicleData >>>', vehicleData);
  if (!vehicleData) throw new BadRequestError(Message.SOMETHING_WENT_WRONG);

  //Vehicle Details
  await createData(
    [
      vehicleDetailsModel,
      {
        vehicleId: vehicleData._id,
        ...vehicleDetailsBody,
      },
    ],
    array,
    false,
  );
  return vehicleData;
};

const updateVehicle = async (
  vehicleId,
  vehicleBody,
  vehicleDetailsBody,
  files,
) => {
  await vehicleDetailsServices.vehicleValidations(
    {
      ...vehicleBody,
      ...vehicleDetailsBody,
    },
    vehicleId,
  );
  const checkVehicle = await vehicleDetailsServices.findOneVehicleByFilter({
    _id: vehicleId,
  });
  if (!checkVehicle) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  const checkVehicleDetails: Dealer.VehicleDetails | any =
    await vehicleDetailsServices.findOneVehicleDetailByFilter({
      vehicleId,
    });
  if (!checkVehicleDetails) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);

  checkVehicle.brandId = vehicleBody.brandId;
  checkVehicle.modelId = vehicleBody.modelId;
  checkVehicle.variantId = vehicleBody.variantId;
  checkVehicle.attributeId = vehicleBody.attributeId;
  checkVehicle.ownerType = vehicleBody.ownerType;
  checkVehicle.price = vehicleBody.price;
  checkVehicle.colorCode = vehicleBody.colorCode;
  checkVehicle.manufactureYear = vehicleBody.manufactureYear;
  checkVehicle.kiloMeters = vehicleBody.kiloMeters;
  checkVehicle.plateNumber = vehicleBody.plateNumber;
  checkVehicleDetails.fuelType = vehicleDetailsBody.fuelType;
  checkVehicleDetails.transmissionType = vehicleDetailsBody.transmissionType;
  checkVehicleDetails.bodyType = vehicleDetailsBody.bodyType;
  checkVehicleDetails.dealerStoreId = vehicleDetailsBody.dealerStoreId;

  let vehicleImages: any = [];
  let documentImages: any = [];
  if (files) {
    files.policyImage &&
      documentImages.push({ key: 'policyImage', value: files.policyImage });

    files.RCbookImage &&
      documentImages.push({ key: 'RCbookImage', value: files.RCbookImage });

    // files.mainImage &&
    if (files.mainImage) {
      const image = await imageSchemaModel.findOne({
        _id: vehicleBody?.mainImageId,
      });
      if (!image) throw new NotFoundError(Message.IMAGE_ID_IS_REQUIRED);
      await deleteFile(image.mainImage, vehicleId);
      await imageSchemaModel.deleteOne({
        _id: vehicleBody.mainImageId,
      });
      vehicleImages.push({ key: 'mainImage', value: files.mainImage });
    }
    files.interiorImages &&
      vehicleImages.push({
        key: 'interiorImages',
        value: files.interiorImages,
      });

    files.exteriorImages &&
      vehicleImages.push({
        key: 'exteriorImages',
        value: files.exteriorImages,
      });
  }
  if (vehicleImages.length > 0) {
    let image = uploadImage(vehicleImages, checkVehicle._id);
    await imageSchemaModel.insertMany(image);
  }
  if (documentImages.length > 0) {
    let image = uploadImage(documentImages, checkVehicleDetails.vehicleId);
    await imageSchemaModel.insertMany(image);
  }
  await checkVehicleDetails.save();
  return await checkVehicle.save();
};

const deleteVehicle = async (vehicleId) => {
  const checkVehicle: Dealer.Vehicle =
    await vehicleDetailsServices.findOneVehicleByFilter({
      _id: vehicleId,
    });
  if (!checkVehicle) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  checkVehicle.isDeleted = true;
  return await checkVehicle.save();
};

const deleteVehicleImage = async (imageId) => {
  let filePath: any = await imageSchemaModel.findOne(
    {
      _id: imageId,
    },
    {
      _id: 0,
      vehicleId: 0,
      isActive: 0,
      createdAt: 0,
      updatedAt: 0,
      isDeleted: false,
    },
  );
  if (!filePath) throw new NotFoundError(Message.IMAGE_NOT_FOUND);
  const checkVehicle = await imageSchemaModel.findOneAndDelete({
    _id: imageId,
  });
  if (!checkVehicle) throw new NotFoundError(Message.IMAGE_NOT_FOUND);
  return { checkVehicle, filePath };
};

export {
  vehicleFullListByFilter,
  vehicleListByFilter,
  addVehicle,
  updateVehicle,
  deleteVehicle,
  deleteVehicleImage,
};
