import { manualInquiryModel } from '../../model/dealer/manualInquiry';
import { createData } from '../../services';
import { logger } from '../../utils';

const addManualInquiry = async (body) => {
  return await createData([manualInquiryModel, body]);
};

const getTotalRecords = async () => {
  return await manualInquiryModel.find({ isDeleted: false });
};

const listManualInquiry = async (filter, isTotalRecord) => {
  const { perPage, skipRecord, sortObj, match } = filter;
  return await manualInquiryModel.aggregate(
    [
      {
        $lookup: {
          from: 'brands',
          localField: 'brandId',
          foreignField: '_id',
          as: 'brandsData',
        },
      },
      {
        $lookup: {
          from: 'models',
          localField: 'modelId',
          foreignField: '_id',
          as: 'modelsData',
        },
      },
      {
        $lookup: {
          from: 'attributes',
          localField: 'fuelTypeId',
          foreignField: '_id',
          as: 'fuelTypeData',
        },
      },
      {
        $lookup: {
          from: 'variants',
          localField: 'variantId',
          foreignField: '_id',
          as: 'variantsData',
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'userId',
          foreignField: '_id',
          as: 'usersData',
        },
      },
      {
        $match: match ? match : {},
      },
      {
        $project: {
          brandId: 1,
          modelId: 1,
          fuelTypeId: 1,
          variantId: 1,
          userId: 1,
          transmissionType: 1,
          bodyType: 1,
          brandName: { $arrayElemAt: ['$brandsData.brandName', 0] },
          modelName: { $arrayElemAt: ['$modelsData.modelName', 0] },
          fuelTypeName: { $arrayElemAt: ['$fuelTypeData.attributeName', 0] },
          variantName: { $arrayElemAt: ['$variantsData.variantName', 0] },
          userName: {
            $concat: [
              { $arrayElemAt: ['$usersData.firstName', 0] },
              ' ',
              { $arrayElemAt: ['$usersData.lastName', 0] },
            ],
          },
          mobileNo: { $arrayElemAt: ['$usersData.mobileNo', 0] },
          ownerType: 1,
          manufactureYear: 1,
          priceRange: 1,
          email: { $arrayElemAt: ['$usersData.email', 0] },
        },
      },
      ...(isTotalRecord
        ? []
        : [{ $sort: sortObj }, { $skip: skipRecord }, { $limit: perPage }]),
    ],
    { collation: { locale: 'en' } },
  );
};

export { addManualInquiry, listManualInquiry, getTotalRecords };
