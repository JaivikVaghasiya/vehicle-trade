import {
  brandModel,
  dealerStorModel,
  userInquiryModel,
  userModel,
  vehicleDetailsModel,
} from '../../model';
import moment from 'moment';
import { countFilter } from '../common';
import { ObjectId, logger } from '../../utils';

export const weeklySell = async (dealerId, startOfWeek, endOfWeek) => {
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfWeek],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', endOfWeek],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },

    {
      $project: {
        _id: 1,
        dealerId: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
    {
      $group: {
        _id: '$dealerId',
        total: { $sum: '$count' },
      },
    },
  ]);
};

export const monthlySellStore = async (dealerId, firstOfMonth, lastOfMonth) => {
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', firstOfMonth],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', lastOfMonth],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              year: '$year',
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        year: { $first: '$year' },
        month: { $first: '$month' },
        // dealerId: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
    {
      $group: {
        _id: '$_id',
        count: { $sum: '$count' },
        time: { $first: moment(firstOfMonth * 1000).format('MMM YYYY') },
      },
    },
  ]);
};

export const weeklySellStore = async (userId, startOfWeek, endOfWeek) => {
  return await dealerStorModel.aggregate([
    { $match: { dealerId: userId } },
    {
      $lookup: {
        from: 'vehicledetails',
        localField: '_id',
        foreignField: 'dealerStoreId',
        as: 'vehicleDetails',
      },
    },
    {
      $lookup: {
        from: 'vehicles',
        localField: 'vehicleDetails.vehicleId',
        foreignField: '_id',
        as: 'vehicleData',
      },
    },
    {
      $project: {
        weeklySealCount: {
          $filter: {
            input: '$vehicleData',
            as: 'vehicle',
            cond: {
              $and: [
                {
                  $gte: ['$$vehicle.sellDetails.sellingDate', startOfWeek],
                },
                {
                  $lte: ['$$vehicle.sellDetails.sellingDate', endOfWeek],
                },
              ],
            },
          },
        },
      },
    },
    {
      $project: {
        count: {
          $size: '$weeklySealCount',
        },
      },
    },
  ]);
};

export const dailySellStore = async (dealerId, startOfDay, endOfDay) => {
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },

          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfDay],
                        },
                        {
                          $lt: ['$sellDetails.sellingDate', endOfDay],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
  ]);
};

export const daySell = async (dealerId, startOfDay, endOfDay) => {
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfDay],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', endOfDay],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        dealerId: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
    {
      $group: {
        _id: '$dealerId',
        total: { $sum: '$count' },
      },
    },
  ]);
};

export const monthlyDayStore = async (
  dealerId,
  startOfMonth,
  endOfMonth,
  storeId,
) => {
  let filter = {};
  if (storeId != null) {
    filter = {
      dealerStoreId: storeId ? new ObjectId(storeId) : { $exists: true },
    };
  }
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },

          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfMonth],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', endOfMonth],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
              dealerStoreId: 1,
            },
          },
          {
            $match: filter,
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        day: { $first: '$day' },
        year: { $first: '$year' },
        month: { $first: '$month' },
        dealerId: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
    {
      $group: {
        _id: '$dealerId',
        count: { $sum: '$count' },
        time: { $first: moment(startOfMonth * 1000).format('MMM DD YYYY') },
      },
    },
  ]);
};

export const weeklyDayStore = async (
  dealerId,
  startOfDay,
  endOfDay,
  storeId,
) => {
  let filter = {};
  if (storeId != null) {
    filter = {
      dealerStoreId: storeId ? new ObjectId(storeId) : { $exists: true },
    };
  }
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },

          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfDay],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', endOfDay],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
              dealerStoreId: 1,
            },
          },
          {
            $match: filter,
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        weekName: { $first: '$week' },
        day: { $first: '$day' },
        year: { $first: '$year' },
        month: { $first: '$month' },
        dealerId: 1,
        count: { $sum: '$vehicleDetails.vehicle' },
      },
    },
    {
      $group: {
        _id: '$dealerId',
        count: { $sum: '$count' },
        weekDayName: { $first: moment(startOfDay * 1000).format('dddd') },
        date: { $first: moment(startOfDay * 1000).format('MMM DD YYYY') },
      },
    },
  ]);
};

export const yearlySell = async (dealerId, startDate, endDate, storeId) => {
  let filter = {};
  if (storeId != null) {
    filter = {
      dealerStoreId: storeId ? new ObjectId(storeId) : { $exists: true },
    };
  }
  return await dealerStorModel.aggregate([
    { $match: { dealerId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          { $match: { $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } } } },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        { $gte: ['$sellDetails.sellingDate', startDate] },
                        { $lte: ['$sellDetails.sellingDate', endDate] },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
              dealerStoreId: 1,
            },
          },
          {
            $match: filter,
          },
        ],
        as: 'vehicledetails',
      },
    },
    {
      $project: {
        _id: 1,
        dealerId: 1,
        count: { $sum: '$vehicledetails.vehicle' },
        year: { $first: '$year' },
        month: { $first: '$month' },
      },
    },
    {
      $group: {
        _id: '$dealerId',
        count: { $sum: '$count' },
        date: { $first: moment(startDate * 1000).format('MMM YYYY') },
      },
    },
  ]);
};

export const allCurrentSellCount = async (
  dealerId,
  monthlySell,
  yearlySell,
  weeklySell,
  dailySell,
) => {
  const data = await userModel.aggregate([
    {
      $lookup: {
        from: 'dealerstores',
        localField: '_id',
        foreignField: 'dealerId',
        as: 'storeData',
      },
    },
    {
      $lookup: {
        from: 'vehicledetails',
        localField: 'storeData._id',
        foreignField: 'dealerStoreId',
        as: 'vehicleDetails',
      },
    },
    {
      $lookup: {
        from: 'vehicles',
        localField: 'vehicleDetails.vehicleId',
        foreignField: '_id',
        as: 'vehicleData',
      },
    },
    { $match: { _id: dealerId } },

    {
      $addFields: {
        weeklySellCount: countFilter(
          weeklySell.startOfWeek,
          weeklySell.endOfWeek,
        ),
        monthlySellCount: countFilter(
          monthlySell.startOfMonth,
          monthlySell.endOfMonth,
        ),
        yearlySellCount: countFilter(
          yearlySell.startOfYear,
          yearlySell.endOfYear,
        ),
        dailySellCount: countFilter(dailySell.startOfDay, dailySell.endOfDay),
      },
    },
    {
      $project: {
        data: [
          { label: 'Weekly Sales', count: { $size: '$weeklySellCount' } },
          { label: 'Monthly Sales', count: { $size: '$monthlySellCount' } },
          { label: 'Yearly Sales', count: { $size: '$yearlySellCount' } },
          { label: 'Daily Sales', count: { $size: '$dailySellCount' } },
        ],
      },
    },
  ]);
  return data[0].data;
};

export const inquiryCountByFilter = async (filter) => {
  const data = await userInquiryModel.aggregate([
    {
      $lookup: {
        from: 'vehicles',
        localField: 'vehicleId',
        foreignField: '_id',
        as: 'vehicleData',
      },
    },
    {
      $lookup: {
        from: 'vehicledetails',
        localField: 'vehicleData._id',
        foreignField: 'vehicleId',
        as: 'vehicleDetails',
      },
    },
    { $match: filter },
  ]);
  return data.length;
};

export const allBrandInquiryCount = async (filter) => {
  logger.info('\n' + 'filter >>>', filter);
  return await brandModel.aggregate([
    {
      $lookup: {
        from: 'vehicles',
        localField: '_id',
        foreignField: 'brandId',
        as: 'vehicleData',
      },
    },
    {
      $lookup: {
        from: 'vehicledetails',
        localField: 'vehicleData._id',
        foreignField: 'vehicleId',
        as: 'vehicleDetails',
      },
    },
    {
      $lookup: {
        from: 'user-inquiries',
        localField: 'vehicleData._id',
        foreignField: 'vehicleId',
        as: 'inquiryData',
      },
    },
    {
      $project: {
        brandName: 1,
        count: { $size: '$inquiryData' },
      },
    },

    { $match: filter },
  ]);
};
export const monthSellStore = async (storeId, firstOfMonth, lastOfMonth) => {
  return await dealerStorModel.aggregate([
    { $match: { _id: storeId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', firstOfMonth],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', lastOfMonth],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              storeName: 1,
              day: { $first: '$day' },
              year: { $first: '$year' },
              month: { $first: '$month' },
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        storeName: 1,
        monthlySellCount: {
          count: { $sum: '$vehicleDetails.vehicle' },
          date: moment(firstOfMonth * 1000).format('MM  DD YYYY'),
        },
      },
    },
  ]);
};

export const yearSellStore = async (storeId, startDate, endDate) => {
  return await dealerStorModel.aggregate([
    { $match: { _id: storeId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          { $match: { $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } } } },
          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        { $gte: ['$sellDetails.sellingDate', startDate] },
                        { $lte: ['$sellDetails.sellingDate', endDate] },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              storeName: 1,
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
              year: { $first: '$year' },
              month: { $first: '$month' },
            },
          },
        ],
        as: 'vehicledetails',
      },
    },
    {
      $project: {
        _id: 1,
        storeName: 1,
        // dealerId: 1,
        yearlySellCount: {
          count: { $sum: '$vehicledetails.vehicle' },
          date: moment(startDate * 1000).format('MM YYYY'),
        },
      },
    },
  ]);
};

export const weekSellStore = async (storeId, startOfDay, endOfDay) => {
  return await dealerStorModel.aggregate([
    { $match: { _id: storeId } },
    {
      $lookup: {
        from: 'vehicledetails',
        let: { id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $and: { $eq: ['$$id', '$dealerStoreId'] } },
            },
          },

          {
            $lookup: {
              from: 'vehicles',
              let: { id: '$vehicleId' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $and: [
                        { $eq: ['$_id', '$$id'] },
                        {
                          $gte: ['$sellDetails.sellingDate', startOfDay],
                        },
                        {
                          $lte: ['$sellDetails.sellingDate', endOfDay],
                        },
                      ],
                    },
                  },
                },
              ],
              as: 'vehiclesData',
            },
          },
          {
            $project: {
              _id: 1,
              storeName: 1,
              week: { $first: '$week' },
              day: { $first: '$day' },
              year: { $first: '$year' },
              month: { $first: '$month' },
              vehicle: { $size: '$vehiclesData' },
              variantName: 1,
            },
          },
        ],
        as: 'vehicleDetails',
      },
    },
    {
      $project: {
        _id: 1,
        storeName: 1,
        weeklySellCount: {
          count: { $sum: '$vehicleDetails.vehicle' },
          weekDayName: moment(startOfDay * 1000).format('dddd'),
          date: moment(startOfDay * 1000).format('MM DD YYYY'),
        },
      },
    },
  ]);
};
