import {
  createData,
  dealerStoreServices,
  vehicleDetailsServices,
  vehicleServices,
} from './../../services';
import { NotFoundError } from '../../error';
import { Message } from '../../utils';
import { userActivityModel, dealerStorModel } from './../../model';
import {
  commonLookup,
  commonProject,
  userCommonProject,
  group,
} from '../common';

const addUserActivity = async (body) => {
  const { vehicleId, dealerStoreId } = body;
  const checkVehicle = await vehicleDetailsServices.findOneVehicleByFilter({
    _id: vehicleId,
  });
  if (!checkVehicle) throw new NotFoundError(Message.VEHICLE_NOT_FOUND);
  const checkDealer = await dealerStoreServices.findDealerStoreByFilter({
    _id: dealerStoreId,
  });

  if (!checkDealer) throw new NotFoundError(Message.DEALER_NOT_FOUND);
  return await createData([userActivityModel, body]);
};
const findDealerActivityCount = async (isSell, filter) => {
  return await userActivityModel.aggregate([
    {
      $lookup: {
        from: 'dealerstores',
        localField: 'dealerStoreId',
        foreignField: '_id',
        as: 'storeData',
      },
    },
    {
      $match: {
        ...filter,
      },
    },
  ]);
};

export const listUserActivity = async (filter, groupCondition) => {
  const { perPage, skipRecord, sortObj, search } = filter;
  return await dealerStorModel.aggregate(
    [
      {
        $lookup: {
          from: 'useractivities',
          localField: '_id',
          foreignField: 'dealerStoreId',
          as: 'userActivities',
        },
      },
      {
        $lookup: {
          from: 'vehicles',
          localField: 'userActivities.vehicleId',
          foreignField: '_id',
          as: 'vehicleData',
        },
      },
      { $unwind: '$vehicleData' },
      ...commonLookup,
      {
        $lookup: {
          from: 'users',
          localField: 'userActivities.userId',
          foreignField: '_id',
          as: 'userData',
        },
      },
      { $unwind: '$userData' },
      {
        $match: {
          ...search,
        },
      },
      {
        $addFields: {
          data: {
            activityId: { $arrayElemAt: ['$userActivities._id', 0] },
            ...commonProject,
            ...userCommonProject,
            userProfileImage: {
              $concat: [process.env.BASE_URL, '$userData.profileImage'],
            },
          },
        },
      },
      {
        $project: {
          ...commonProject,
          ...userCommonProject,
          activityId: { $arrayElemAt: ['$userActivities._id', 0] },
          userProfileImage: {
            $concat: [process.env.BASE_URL, '$userData.profileImage'],
          },
          updatedAt: 1,
          isSoldOut: '$vehicleData.sellDetails.isSell',
        },
      },
      { $sort: sortObj },
      { $skip: skipRecord },
      { $limit: perPage },
    ],
    { collation: { locale: 'en' } },
  );
};

const removeUserActivity = async (id) => {
  const checkActivity: any = await userActivityModel.findOne({
    _id: id,
  });
  if (!checkActivity) throw new NotFoundError(Message.ACTIVITY_NOT_FOUND);
  checkActivity.isDeleted = true;
  return await checkActivity.remove();
};

export { addUserActivity, findDealerActivityCount, removeUserActivity };
