import { vehicleDetailsModel, vehicleModel } from '../../model';
import { Message } from '../../utils';
import { Admin } from '../../types';
import { BadRequestError, NotFoundError } from '../../error';
import {
  attributeServices,
  brandServices,
  dealerStoreServices,
  modelServices,
  variantServices,
} from '..';

const findOneVehicleDetailByFilter = async (filter) => {
  return await vehicleDetailsModel.findOne(filter);
};

const findOneVehicleByFilter = async (filter) => {
  return await vehicleModel.findOne(filter);
};

const findVehicleDetailsCount = async (filter) => {
  return await vehicleDetailsModel.find(filter).countDocuments();
};

const vehicleValidations = async (body, vehicleId?) => {
  const {
    attributeId,
    brandId,
    modelId,
    variantId,
    plateNumber,
    fuelType,
    transmissionType,
    bodyType,
    dealerStoreId,
  } = body;
  let filter = vehicleId
    ? { _id: { $ne: vehicleId }, plateNumber: plateNumber }
    : { plateNumber: plateNumber };

  const checkPlateNumber: any = await findOneVehicleByFilter(filter);
  if (checkPlateNumber)
    throw new BadRequestError(Message.PLATE_NUMBER_ALREADY_IN_USE);

  const checkVehicleType = await attributeServices.findOneAttributeByFilter({
    _id: attributeId,
  });
  if (!checkVehicleType)
    throw new NotFoundError(Message.VEHICLE_TYPE_NOT_FOUND);

  const checkBrand: Admin.Brand = await brandServices.findBrandByFilter({
    _id: brandId,
    attributeId: attributeId,
  });
  if (!checkBrand) throw new NotFoundError(Message.INVALID_BRAND_NAME);

  const checkModel: Admin.VehicleBrandModel =
    await modelServices.findModelByFilter({
      _id: modelId,
      brandId: brandId,
    });
  if (!checkModel) throw new BadRequestError(Message.INVALID_MODEL_NAME);

  const checkVariant: Admin.Variant = await variantServices.findVariantByFilter(
    {
      _id: variantId,
      brandId: brandId,
      modelId: modelId,
    },
  );
  if (!checkVariant) throw new BadRequestError(Message.INVALID_VARIANT_NAME);

  const checkFuelType = await attributeServices.findOneAttributeByFilter({
    _id: fuelType,
  });
  if (!checkFuelType) throw new BadRequestError(Message.INVALID_FUEL_TYPE);

  const checkTransmissionType =
    await attributeServices.findOneAttributeByFilter({
      _id: transmissionType,
    });
  if (!checkTransmissionType)
    throw new BadRequestError(Message.INVALID_TRANSMISSION_TYPE);

  const checkBodyType = await attributeServices.findOneAttributeByFilter({
    _id: bodyType,
  });
  if (!checkBodyType) throw new BadRequestError(Message.INVALID_BODY_TYPE);

  const checkDealerStore: any =
    await dealerStoreServices.findDealerStoreByFilter({ _id: dealerStoreId });
  if (!checkDealerStore)
    throw new BadRequestError(Message.DEALER_STORE_NOT_FOUND);
};

const vehicleNameLookup = [
  {
    $lookup: {
      from: 'vehicledetails',
      localField: '_id',
      foreignField: 'vehicleId',
      as: 'vehicleData',
    },
  },
  {
    $lookup: {
      from: 'brands',
      localField: 'brandId',
      foreignField: '_id',
      as: 'brandData',
    },
  },
  {
    $lookup: {
      from: 'models',
      localField: 'modelId',
      foreignField: '_id',
      as: 'modelData',
    },
  },
  {
    $lookup: {
      from: 'variants',
      localField: 'variantId',
      foreignField: '_id',
      as: 'variantData',
    },
  },
  {
    $lookup: {
      from: 'attributes',
      localField: 'vehicleData.fuelType',
      foreignField: '_id',
      as: 'fuelType',
    },
  },
  {
    $lookup: {
      from: 'attributes',
      localField: 'attributeId',
      foreignField: '_id',
      as: 'vehicleType',
    },
  },
  {
    $lookup: {
      from: 'images',
      localField: '_id',
      foreignField: 'vehicleId',
      as: 'imageData',
    },
  },
  {
    $lookup: {
      from: 'dealerstores',
      localField: 'vehicleData.dealerStoreId',
      foreignField: '_id',
      as: 'locationData',
    },
  },
];

const attributeTypeLookup = [
  {
    $lookup: {
      from: 'attributes',
      localField: 'vehicleData.bodyType',
      foreignField: '_id',
      as: 'bodyType',
    },
  },
  {
    $lookup: {
      from: 'attributes',
      localField: 'vehicleData.transmissionType',
      foreignField: '_id',
      as: 'transmissionType',
    },
  },
];

const imageFields = [
  {
    $addFields: {
      mainImage: {
        $map: {
          input: '$imageData',
          as: 'image',
          in: {
            imageId: '$$image._id',
            image: {
              $concat: [process.env.BASE_URL, '$$image.mainImage'],
            },
          },
        },
      },
      interior: {
        $map: {
          input: '$imageData',
          as: 'image',
          in: {
            imageId: '$$image._id',
            image: {
              $concat: [process.env.BASE_URL, '$$image.interiorImages'],
            },
          },
        },
      },
      exterior: {
        $map: {
          input: '$imageData',
          as: 'image',
          in: {
            imageId: '$$image._id',
            image: {
              $concat: [process.env.BASE_URL, '$$image.exteriorImages'],
            },
          },
        },
      },
      RCbook: {
        $map: {
          input: '$imageData',
          as: 'image',
          in: {
            imageId: '$$image._id',
            image: {
              $concat: [process.env.BASE_URL, '$$image.RCbookImage'],
            },
          },
        },
      },
      policy: {
        $map: {
          input: '$imageData',
          as: 'image',
          in: {
            imageId: '$$image._id',
            image: {
              $concat: [process.env.BASE_URL, '$$image.policyImage'],
            },
          },
        },
      },
    },
  },
];

const imageProject = {
  mainImage: {
    $first: {
      $filter: {
        input: '$mainImage',
        as: 'image',
        cond: { $ne: ['$$image.image', null] },
      },
    },
  },
  RCbookImage: {
    $ifNull: [
      {
        $first: {
          $filter: {
            input: '$RCbook',
            as: 'image',
            cond: { $ne: ['$$image.image', null] },
          },
        },
      },
      null,
    ],
  },
  policyImage: {
    $ifNull: [
      {
        $first: {
          $filter: {
            input: '$policy',
            as: 'image',
            cond: { $ne: ['$$image.image', null] },
          },
        },
      },
      null,
    ],
  },
  interiorImages: {
    $filter: {
      input: '$interior',
      as: 'image',
      cond: { $ne: ['$$image.image', null] },
    },
  },
  exteriorImages: {
    $filter: {
      input: '$exterior',
      as: 'image',
      cond: { $ne: ['$$image.image', null] },
    },
  },
};

const simpleImageProject = {
  bodyTypeImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$bodyType.attributeImage', 0] },
    ],
  },
  RCbookImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$imageData.RCbookImage', 0] },
    ],
  },
  policyImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$imageData.policyImage', 0] },
    ],
  },
  mainImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$imageData.mainImage', 0] },
    ],
  },
  interiorImages: {
    $map: {
      input: '$imageData.interiorImages',
      as: 'image',
      in: {
        $concat: [process.env.BASE_URL, '$$image'],
      },
    },
  },
  exteriorImages: {
    $map: {
      input: '$imageData.exteriorImages',
      as: 'image',
      in: {
        $concat: [process.env.BASE_URL, '$$image'],
      },
    },
  },
};

const vehicleProject = {
  vehicleName: {
    $concat: [
      { $arrayElemAt: ['$brandData.brandName', 0] },
      ' ',
      { $arrayElemAt: ['$modelData.modelName', 0] },
      ' ',
      { $arrayElemAt: ['$variantData.variantName', 0] },
    ],
  },
  ownerType: 1,
  price: 1,
  manufactureYear: 1,
  colorCode: 1,
  kiloMeters: 1,
  plateNumber: 1,
  brandName: { $arrayElemAt: ['$brandData.brandName', 0] },
  brandLogo: {
    $concat: [process.env.BASE_URL, { $arrayElemAt: ['$brandData.logo', 0] }],
  },
  variantName: { $arrayElemAt: ['$variantData.variantName', 0] },
  modelName: { $arrayElemAt: ['$modelData.modelName', 0] },
  fuelType: { $arrayElemAt: ['$fuelType.attributeName', 0] },
  vehicleType: { $arrayElemAt: ['$vehicleType.attributeName', 0] },
  mainImage: {
    $concat: [
      process.env.BASE_URL,
      { $arrayElemAt: ['$imageData.mainImage', 0] },
    ],
  },
};

const vehicleDetailsProject = {
  bodyType: { $arrayElemAt: ['$bodyType.attributeName', 0] },
  transmissionType: {
    $arrayElemAt: ['$transmissionType.attributeName', 0],
  },
  location: { $arrayElemAt: ['$locationData.storeLocation', 0] },
};

export {
  findOneVehicleDetailByFilter,
  findVehicleDetailsCount,
  findOneVehicleByFilter,
  vehicleValidations,
  vehicleNameLookup,
  attributeTypeLookup,
  vehicleProject,
  vehicleDetailsProject,
  imageProject,
  imageFields,
  simpleImageProject,
};
