import { dealerStorModel } from '../../model';
import {
  Message,
  deleteFile,
  ObjectId,
  uploadImage,
  logger,
} from '../../utils';
import { Dealer, User } from '../../types';
import { createData, userServices } from '../../services';
import { BadRequestError, NotFoundError } from '../../error';

const findDealerStoreByFilter = async (filter) => {
  return await dealerStorModel.findOne(filter);
};
const findAllDealerStore = async (filter) => {
  return await dealerStorModel.find(filter);
};

const addStore = async (body, files) => {
  const checkDealer: User.User | null = await userServices.userFindOneByFilter({
    _id: body.dealerId,
  });
  if (!checkDealer) throw new BadRequestError(Message.DEALER_NOT_FOUND);
  const checkStoreEmail: Dealer.DealerStore = await findDealerStoreByFilter({
    storeEmail: body.storeEmail,
  });
  if (checkStoreEmail) throw new BadRequestError(Message.EMAIL_ALREADY_IN_USE);
  const checkStoreContact: Dealer.DealerStore = await findDealerStoreByFilter({
    storeContact: body.storeContact,
  });
  if (checkStoreContact)
    throw new BadRequestError(Message.CONTACT_NUMBER_ALREADY_IN_USE);
  let array = [];
  files &&
    files.storeDocument &&
    array.push({ key: 'storeDocument', value: files.storeDocument });
  return await createData([dealerStorModel, body], array, true);
};

const listStore = async (dealerId) => {
  const checkUser: User.User = await userServices.userFindOneByFilter({
    _id: dealerId,
    isDeleted: false,
  });
  if (!checkUser) throw new NotFoundError(Message.DEALER_NOT_FOUND);
  return await dealerStorModel.aggregate([
    {
      $match: {
        dealerId: new ObjectId(dealerId),
        isActive: true,
        isDeleted: false,
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'dealerId',
        foreignField: '_id',
        as: 'dealerData',
      },
    },
    {
      $project: {
        dealerId: 1,
        storeName: 1,
        storeLocation: 1,
        storeContact: 1,
        storeEmail: 1,
        employeeName: 1,
        storeOpenTime: 1,
        storeCloseTime: 1,
        storeDocument: {
          $concat: [process.env.BASE_URL, '$storeDocument'],
        },
        dealerName: {
          $concat: [
            { $arrayElemAt: ['$dealerData.firstName', 0] },
            ' ',
            { $arrayElemAt: ['$dealerData.lastName', 0] },
          ],
        },
      },
    },
  ]);
};

const updateStore = async (storeId, body, files) => {
  const checkDealerStore: Dealer.DealerStore | null =
    await findDealerStoreByFilter({
      _id: storeId,
    });
  const checkStoreEmail: Dealer.DealerStore = await findDealerStoreByFilter({
    _id: { $ne: storeId },
    storeEmail: body.storeEmail,
  });
  if (checkStoreEmail) throw new BadRequestError(Message.EMAIL_ALREADY_IN_USE);
  const checkStoreContact: Dealer.DealerStore = await findDealerStoreByFilter({
    _id: { $ne: storeId },
    storeContact: body.storeContact,
  });
  if (checkStoreContact)
    throw new BadRequestError(Message.CONTACT_NUMBER_ALREADY_IN_USE);
  if (!checkDealerStore)
    throw new NotFoundError(Message.DEALER_STORE_NOT_FOUND);
  checkDealerStore.employeeName =
    body.employeeName || checkDealerStore.employeeName;
  checkDealerStore.storeName = body.storeName || checkDealerStore.storeName;
  checkDealerStore.storeLocation =
    body.storeLocation || checkDealerStore.storeLocation;
  checkDealerStore.storeContact =
    body.storeContact || checkDealerStore.storeContact;
  checkDealerStore.storeEmail = body.storeEmail || checkDealerStore.storeEmail;
  checkDealerStore.storeOpenTime =
    body.storeOpenTime || checkDealerStore.storeOpenTime;
  checkDealerStore.storeCloseTime =
    body.storeCloseTime || checkDealerStore.storeCloseTime;

  if (files) {
    await deleteFile(checkDealerStore.storeDocument, checkDealerStore._id);
    let image = await uploadImage(
      [{ value: files.storeDocument, key: 'image' }],
      checkDealerStore._id,
    );
    checkDealerStore.storeDocument = image[0].image;
  }
  return await checkDealerStore.save();
};

const deleteStore = async (storeId) => {
  const checkDealer: Dealer.DealerStore | null = await findDealerStoreByFilter({
    _id: storeId,
  });
  if (!checkDealer) throw new NotFoundError(Message.DEALER_NOT_FOUND);
  checkDealer.isDeleted = true;
  return await checkDealer.save();
};

export {
  findDealerStoreByFilter,
  addStore,
  updateStore,
  listStore,
  deleteStore,
  findAllDealerStore,
};
