import {
  vehicleLookup,
  commonLookup,
  commonProject,
  userCommonLookup,
  userCommonProject,
  storeProject,
} from '../common';
import { dealerStorModel, userInquiryModel } from '../../model';
import { Message, logger } from '../../utils';
import moment from 'moment';
import { BadRequestError } from '../../error';
import { dealerStoreServices, vehicleDetailsServices } from '..';

export const soldVehicle = async (filter, isTotalRecord) => {
  const { pagination, match } = filter;
  return await dealerStorModel.aggregate(
    [
      ...vehicleLookup,
      ...commonLookup,
      {
        $match: {
          'vehicleData.isActive': true,
          'vehicleData.isDeleted': false,
          'vehicleData.sellDetails.isSell': { $eq: true },
          ...match,
        },
      },
      ...userCommonLookup,
      {
        $project: {
          ...storeProject,
          ...commonProject,
          ...userCommonProject,
          sellDate: {
            $multiply: ['$vehicleData.sellDetails.sellingDate', 1000],
          },
        },
      },
      ...(isTotalRecord
        ? []
        : [
            { $sort: pagination.sortObj },
            { $skip: pagination.skipRecord },
            { $limit: pagination.perPage },
          ]),
    ],
    { collation: { locale: 'en' } },
  );
};

export const remainingVehicles = async (filter, isTotalRecord) => {
  const { pagination, match } = filter;
  return await dealerStorModel.aggregate(
    [
      ...vehicleLookup,
      ...commonLookup,
      {
        $match: {
          'vehicleData.isActive': true,
          'vehicleData.isDeleted': false,
          'vehicleData.sellDetails.isSell': { $eq: false },
          ...match,
        },
      },
      {
        $addFields: {
          data: { ...commonProject },
        },
      },
      {
        $project: {
          ...commonProject,
          ...storeProject,
        },
      },
      ...(isTotalRecord
        ? []
        : [
            { $sort: pagination.sortObj },
            { $skip: pagination.skipRecord },
            { $limit: pagination.perPage },
          ]),
    ],
    { collation: { locale: 'en' } },
  );
};

export const userInquiries = async (filter, isTotalRecord) => {
  const { pagination, match } = filter;
  return await dealerStorModel.aggregate(
    [
      {
        $lookup: {
          from: 'user-inquiries',
          localField: '_id',
          foreignField: 'dealerStoreId',
          as: 'userInquiry',
        },
      },
      {
        $lookup: {
          from: 'vehicles',
          localField: 'userInquiry.vehicleId',
          foreignField: '_id',
          as: 'vehicleData',
        },
      },
      { $unwind: '$vehicleData' },
      ...commonLookup,
      {
        $lookup: {
          from: 'users',
          localField: 'userInquiry.userId',
          foreignField: '_id',
          as: 'userData',
        },
      },
      { $unwind: '$userData' },
      {
        $match: {
          ...match,
          'vehicleData.isDeleted': false,
          'vehicleData.sellDetails.isSell': { $eq: false },
        },
      },
      {
        $addFields: {
          data: {
            ...commonProject,
            ...userCommonProject,
            userProfileImage: {
              $concat: [process.env.BASE_URL, '$userData.profileImage'],
            },
            createdAt: '$createdAt',
          },
        },
      },
      {
        $project: {
          ...storeProject,
          ...commonProject,
          ...userCommonProject,
          userProfileImage: {
            $concat: [process.env.BASE_URL, '$userData.profileImage'],
          },
          createdAt: 1,
        },
      },
      ...(isTotalRecord
        ? []
        : [
            { $sort: pagination.sortObj },
            { $skip: pagination.skipRecord },
            { $limit: pagination.perPage },
          ]),
    ],
    { collation: { locale: 'en' } },
  );
};

export const approveInquiry = async (body) => {
  const { vehicleId, userId } = body;
  const approveToUser: any =
    await vehicleDetailsServices.findOneVehicleByFilter({ _id: vehicleId });
  if (!approveToUser) throw new BadRequestError(Message.VEHICLE_NOT_FOUND);
  approveToUser.sellDetails.isSell = true;
  approveToUser.sellDetails.userId = userId;
  approveToUser.sellDetails.sellingDate = moment().unix();
  return await approveToUser.save();
};

export const refuseInquiry = async (body) => {
  const { vehicleId } = body;
  const refuseToUser: any = await vehicleDetailsServices.findOneVehicleByFilter(
    { _id: vehicleId },
  );
  if (!refuseToUser) throw new BadRequestError(Message.VEHICLE_NOT_FOUND);
  refuseToUser.sellDetails.isSell = false;
  refuseToUser.sellDetails.userId = null;
  refuseToUser.sellDetails.sellingDate = null;
  return await refuseToUser.save();
};
