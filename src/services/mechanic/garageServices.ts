import { garageServiceModel } from '../../model';
import { Message, logger } from '../../utils';
import { Mechanic } from '../../types';
import { createData, garageServices } from '../../services';
import { BadRequestError, NotFoundError } from '../../error';

const findOneGarageServiceByFilter = async (filter) => {
  return await garageServiceModel.findOne(filter);
};

const addService = async (body) => {
  const checkGarage: Mechanic.Garage | null =
    await garageServices.findOneGarageByFilter({
      _id: body.garageId,
    });
  if (!checkGarage) throw new BadRequestError(Message.SERVICE_NOT_FOUND);
  const checkService = await findOneGarageServiceByFilter({
    garageId: body.garageId,
    serviceTypeId: body.serviceTypeId,
  });
  if (checkService) throw new BadRequestError(Message.SERVICE_ALREADY_EXITS);
  return await createData([garageServiceModel, body]);
};

const listService = async (filter) => {
  const data = await garageServiceModel.aggregate([
    {
      $lookup: {
        from: 'attributes',
        localField: 'services.serviceId',
        foreignField: '_id',
        as: 'serviceData',
      },
    },
    {
      $lookup: {
        from: 'attributestypes',
        localField: 'serviceTypeId',
        foreignField: '_id',
        as: 'attributeTypeData',
      },
    },
    { $match: { ...filter, 'services.isActive': true } },
    {
      $addFields: {
        services: {
          $map: {
            input: '$services',
            as: 'data',
            in: {
              $mergeObjects: [
                {
                  price: '$$data.price',
                  isActive: '$$data.isActive',
                  isDeleted: '$$data.isDeleted',
                  _id: '$$data._id',
                  serviceId: '$$data.serviceId',
                },
                {
                  serviceName: {
                    $arrayElemAt: [
                      '$serviceData.attributeName',
                      {
                        $indexOfArray: ['$serviceData._id', '$$data.serviceId'],
                      },
                    ],
                  },
                },
              ],
            },
          },
        },
      },
    },
    {
      $project: {
        garageId: 1,
        serviceTypeId: 1,
        serviceType: {
          $arrayElemAt: ['$attributeTypeData.attributeTypeName', 0],
        },
        services: {
          $filter: {
            input: '$services',
            as: 'check',
            cond: {
              $and: [
                {
                  $eq: ['$$check.isActive', true],
                },
                {
                  $eq: ['$$check.isDeleted', false],
                },
              ],
            },
          },
        },
        isActive: 1,
        isDeleted: 1,
      },
    },
  ]);
  return data;
};

const UpdateService = async (garageServicesId, body) => {
  const checkGarage: Mechanic.GarageService | null =
    await findOneGarageServiceByFilter({
      _id: garageServicesId,
    });
  if (!checkGarage) throw new BadRequestError(Message.SERVICE_NOT_FOUND);
  const updateService = await garageServiceModel.updateOne(
    { _id: garageServicesId },
    { $set: body },
  );
  if (updateService.modifiedCount > 0) {
    return { _id: garageServicesId };
  }
  throw new BadRequestError(Message.SERVICE_UPDATE_FAILED);
};

const deleteService = async (payload) => {
  const { garageServicesId, serviceId } = payload;
  const checkGarageService: Mechanic.GarageService | null =
    await findOneGarageServiceByFilter({
      _id: garageServicesId,
    });
  if (!checkGarageService) throw new NotFoundError(Message.SERVICE_NOT_FOUND);
  const deleteService = await garageServiceModel.updateOne(
    { _id: garageServicesId },
    { $set: { 'services.$[elem].isDeleted': true } },
    { arrayFilters: [{ 'elem.serviceId': { $eq: serviceId } }] },
  );
  if (deleteService.modifiedCount > 0) {
    return { _id: garageServicesId };
  }
  throw new BadRequestError(Message.SERVICE_DELETE_FAILED);
};

const activeInactiveService = async (payload) => {
  const { garageServicesId, serviceId, isActive } = payload;
  const checkGarageService: Mechanic.GarageService | null =
    await findOneGarageServiceByFilter({
      _id: garageServicesId,
    });
  if (!checkGarageService) throw new NotFoundError(Message.SERVICE_NOT_FOUND);
  const updateStatus = await garageServiceModel.updateOne(
    { _id: garageServicesId },
    { $set: { 'services.$[elem].isActive': isActive } },
    { arrayFilters: [{ 'elem.serviceId': { $eq: serviceId } }] },
  );
  if (updateStatus.modifiedCount > 0) {
    return { _id: garageServicesId };
  }
  throw new BadRequestError(Message.STATUS_UPDATE_FAILED);
};

export {
  findOneGarageServiceByFilter,
  addService,
  listService,
  UpdateService,
  deleteService,
  activeInactiveService,
};
