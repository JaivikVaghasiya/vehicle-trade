import { garageModel } from '../../model';
import { Message, ObjectId, logger } from '../../utils';
import { User, Mechanic } from '../../types';
import { createData, userServices } from '../../services';
import { BadRequestError, NotFoundError } from '../../error';

const findOneGarageByFilter = async (filter) => {
  return await garageModel.findOne(filter);
};
const findGarageByFilter = async (filter) => {
  return await garageModel.find(filter);
};

const addGarage = async (body) => {
  const checkGarage: User.User | null = await userServices.userFindOneByFilter({
    _id: body.mechanicId,
  });
  if (!checkGarage) throw new BadRequestError(Message.MECHANIC_NOT_FOUND);
  return await createData([garageModel, body]);
};

const listGarage = async (mechanicId) => {
  const checkUser: User.User = await userServices.userFindOneByFilter({
    _id: mechanicId,
    isDeleted: false,
  });
  if (!checkUser) throw new NotFoundError(Message.USER_NOT_FOUND);
  return await garageModel.aggregate([
    {
      $match: {
        mechanicId: new ObjectId(mechanicId),
        isActive: true,
        isDeleted: false,
      },
    },
    {
      $lookup: {
        from: 'users',
        localField: 'mechanicId',
        foreignField: '_id',
        as: 'garageData',
      },
    },
    {
      $project: {
        mechanicId: 1,
        garageName: 1,
        location: 1,
        garageContact: 1,
        garageEmail: 1,
        employeeName: 1,
        mechaniName: {
          $concat: [
            { $arrayElemAt: ['$garageData.firstName', 0] },
            ' ',
            { $arrayElemAt: ['$garageData.lastName', 0] },
          ],
        },
      },
    },
  ]);
};

const updateGarage = async (garageId, body) => {
  const checkGarage: Mechanic.Garage | null = await findOneGarageByFilter({
    _id: garageId,
  });
  if (!checkGarage) throw new BadRequestError(Message.GARAGE_NOT_FOUND);

  checkGarage.employeeName = body.employeeName || checkGarage.employeeName;
  checkGarage.garageName = body.garageName || checkGarage.garageName;
  checkGarage.location = body.location || checkGarage.location;
  checkGarage.garageContact = body.garageContact || checkGarage.garageContact;
  checkGarage.garageEmail = body.garageEmail || checkGarage.garageEmail;
  checkGarage.garageOpenTime =
    body.garageOpenTime || checkGarage.garageOpenTime;
  checkGarage.garageCloseTime =
    body.garageCloseTime || checkGarage.garageCloseTime;
  return await checkGarage.save();
};

const deleteGarage = async (mechanicId) => {
  const checkGarage: Mechanic.Garage | null = await findOneGarageByFilter({
    _id: mechanicId,
  });
  if (!checkGarage) throw new NotFoundError(Message.DEALER_NOT_FOUND);
  checkGarage.isDeleted = true;
  return await checkGarage.save();
};

const listNearGarage = async (body) => {
  const { latitude, longitude } = body;
  console.log('latitude, longitude :>> ', latitude, longitude);
  const data = await garageModel.aggregate([
    {
      $geoNear: {
        near: [Number(latitude), Number(longitude)],
        distanceField: 'distance',
        spherical: true,
        distanceMultiplier: 0.001,
      },
    },
    {
      $project: {
        garageContact: 1,
        garageEmail: 1,
        garageCloseTime: 1,
        garageOpenTime: 1,
        employeeName: 1,
        location: 1,
        distance: 1,
        mapLink: {
          $concat: [
            'https://google.com/maps?q=',
            { $toString: { $arrayElemAt: ['$location.coordinates', 0] } },
            ',',
            { $toString: { $arrayElemAt: ['$location.coordinates', 1] } },
          ],
        },
      },
    },
  ]);
  return data;
};

export {
  findOneGarageByFilter,
  addGarage,
  listGarage,
  updateGarage,
  deleteGarage,
  findGarageByFilter,
  listNearGarage,
};
