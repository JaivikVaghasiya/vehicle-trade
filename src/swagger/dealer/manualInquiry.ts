// __________________add variant___________________
/**
 * @swagger
 * /vehicletrade/user/manual/inquiry/add:
 *  post:
 *      summary: add new manual Inquiry
 *      tags: [Manual Inquiry]
 *      description: add Inquiry
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          fuelTypeId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          variantId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          transmissionType:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          userId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          ownerType:
 *                              type: string
 *                              example: 2
 *                          manufactureYear:
 *                              type: string
 *                              example: 2010
 *                          priceRange:
 *                              type: string
 *                              example: 1000000
 *                          location:
 *                              type: object
 *                              properties:
 *                                  address:
 *                                     type: string
 *                                     example: Gota
 *                                  country:
 *                                     type: string
 *                                     example: Gota
 *                                  countryCode:
 *                                     type: number
 *                                     example: +91
 *                                  state:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  district:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  area:
 *                                     type: string
 *                                     example: Gota
 *                                  city:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  road:
 *                                     type: string
 *                                     example: SG highway
 *                                  postCode:
 *                                     type: number
 *                                     example: 380009
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: variant added successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
// _________________list manual Inquiry________________
/**
 * @swagger
 * /vehicletrade/user/manual/inquiry/list:
 *  post:
 *      summary: List manual Inquiry
 *      tags: [Manual Inquiry]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          modelId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          fuelTypeId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          variantId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          transmissionTypeId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          bodyTypeId:
 *                              type: string
 *                              example: ['63b7c478a66e0602c01887fd','63b7c478a66e0602c01887fd']
 *                          ownerType:
 *                              type: number
 *                              example: 2
 *                          minManufactureYear:
 *                              type: number
 *                              example: 2022
 *                          maxManufactureYear:
 *                              type: number
 *                              example: 2022
 *                          minBudget:
 *                              type: number
 *                              example: 50000
 *                          maxBudget:
 *                              type: number
 *                              example: 5000000
 *                          page:
 *                              type: number
 *                              example: 1
 *                          limit:
 *                              type: number
 *                              example: 10
 *                          sortField:
 *                              type: string
 *                              example: BrandName
 *                          sortType:
 *                              type: string
 *                              example: 'asc'
 *                          search:
 *                              type: string
 *                              example: 'Tata'
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      brandName:
 *                                          type: string
 *                                          example: Tata
 *                                      vehicleType:
 *                                          type: string
 *                                          example: Car
 *                                      brandLogo:
 *                                          type: string
 *                                          example: Car
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */