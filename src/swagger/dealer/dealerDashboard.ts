// _________________list daySell________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/day/sell:
 *  get:
 *      summary: list daySell vehicle
 *      tags: [DealerDashboard]
 *      description: list daySell list
 *      parameters:
 *        - in: query
 *          name: startOfDay
 *          schema:
 *              type: string
 *              example: 1682640000000
 *        - in: query
 *          name: endOfDay
 *          schema:
 *              type: string
 *              example: 1682683200000
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 644a118b819efd2947e44bc5
 *                                      total:
 *                                          type: string
 *                                          example: 2
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list monthlySellStore________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/month/sell/store:
 *  get:
 *      summary:  list monthlySellStore vehicle
 *      tags: [DealerDashboard]
 *      description: list monthlySellStore list
 *      parameters:
 *        - in: query
 *          name: firstOfMonth
 *          schema:
 *              type: string
 *              example: 1680330184000
 *        - in: query
 *          name: lastOfMonth
 *          schema:
 *              type: string
 *              example: 1682835784000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 644a1514819efd2947e44bdc
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                                  properties:
 *                                      storeName:
 *                                          type: string
 *                                          example: Umang Motars
 *                                      monthlySellCount:
 *                                        type: array
 *                                        items:
 *                                          type: object
 *                                          properties:
 *                                              count:
 *                                                  type: number
 *                                                  example: 2
 *                                              date:
 *                                                  type: date
 *                                                  example: 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list monthSell________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/monthly/day:
 *  get:
 *      summary: list monthlySell vehicle
 *      tags: [DealerDashboard]
 *      description: list monthlySell list
 *      parameters:
 *        - in: query
 *          name: startOfMonth
 *          schema:
 *              type: string
 *              example: 1682919900000
 *        - in: query
 *          name: endOfMonth
 *          schema:
 *              type: string
 *              example: 1685511900000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 646c8a76a42e56dce364869d
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 644a118b819efd2947e44bc5
 *                                      count:
 *                                          type: string
 *                                          example: 2
 *                                      time:
 *                                          type: string
 *                                          example: Apr 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list weeklySell________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/weekly/day:
 *  get:
 *      summary: list weekSell vehicle
 *      tags: [DealerDashboard]
 *      description: list weekSell list
 *      parameters:
 *        - in: query
 *          name: startOfWeek
 *          schema:
 *              type: string
 *              example: 1684734300000
 *        - in: query
 *          name: endOfWeek
 *          schema:
 *              type: string
 *              example: 1685079900000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 646c8a76a42e56dce364869d
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 644a118b819efd2947e44bc5
 *                                      count:
 *                                          type: string
 *                                          example: 2
 *                                      weekDayName:
 *                                          type: string
 *                                          example: monday
 *                                      date:
 *                                          type: string
 *                                          example: Apr 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list weeklySellStore________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/week/sell/store:
 *  get:
 *      summary: list weekSellStore vehicle
 *      tags: [DealerDashboard]
 *      description: list weekSellStore list
 *      parameters:
 *        - in: query
 *          name: startOfWeek
 *          schema:
 *              type: string
 *              example: 1682926829000
 *        - in: query
 *          name: endOfWeek
 *          schema:
 *              type: string
 *              example: 1683445229000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 644a1514819efd2947e44bdc
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      storeName:
 *                                          type: string
 *                                          example: Umang Motars
 *                                      weeklySellCount:
 *                                        type: array
 *                                        items:
 *                                          type: object
 *                                          properties:
 *                                              count:
 *                                                  type: number
 *                                                  example: 2
 *                                              date:
 *                                                  type: date
 *                                                  example: 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list yearlySell________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/yearly/sell:
 *  get:
 *      summary: list yearlySell vehicle
 *      tags: [DealerDashboard]
 *      description: list yearlySell list
 *      parameters:
 *        - in: query
 *          name: startOfYear
 *          schema:
 *              type: string
 *              example: 1672553203000
 *        - in: query
 *          name: endOfYear
 *          schema:
 *              type: string
 *              example: 1704002803000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 646c8a76a42e56dce364869d
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 644a118b819efd2947e44bc5
 *                                      count:
 *                                          type: string
 *                                          example: 2
 *                                      date:
 *                                          type: string
 *                                          example: 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// _________________list yearlySellStore________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/year/sell/store:
 *  get:
 *      summary: list yearSellStore vehicle
 *      tags: [DealerDashboard]
 *      description: list yearSellStore list
 *      parameters:
 *        - in: query
 *          name: startOfYear
 *          schema:
 *              type: string
 *              example: 1672553203000
 *        - in: query
 *          name: endOfYear
 *          schema:
 *              type: string
 *              example: 1704002803000
 *        - in: query
 *          name: storeId
 *          schema:
 *              type: string
 *              example: 644a1514819efd2947e44bdc
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      storeName:
 *                                          type: string
 *                                          example: Umang Motars
 *                                      yearlySellCount:
 *                                        type: array
 *                                        items:
 *                                          type: object
 *                                          properties:
 *                                              count:
 *                                                  type: number
 *                                                  example: 2
 *                                              date:
 *                                                  type: date
 *                                                  example: 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// _________________list allCurrentSellCount________________
/**
 * @swagger
 * /vehicletrade/dealer/dashboard/all/Current/Sell/Count:
 *   get:
 *      summary: list all CurrentSellCount vehicle
 *      description: Get all CurrentSellCount
 *      tags: [DealerDashboard]
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      label:
 *                                          type: string
 *                                          example: Weekly Sales
 *                                      count:
 *                                          type: string
 *                                          example: 2
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

/**
 * @swagger
 * /vehicletrade/dealer/dashboard/inquiry/count:
 *  post:
 *      tags: [DealerDashboard]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          modelId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          variantId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          fuelTypeId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          transmissionTypeId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          bodyTypeId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *                          storeId:
 *                              type: array
 *                              items:
 *                                  type: string
 *                                  example: 63b7c478a66e0602c01887fd
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
