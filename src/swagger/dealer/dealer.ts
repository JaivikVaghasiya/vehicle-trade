/**
 * @swagger
 * /vehicletrade/dealer/sold:
 *  get:
 *      tags: [Dealer]
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *              example : 1
 *        - in: query
 *          name: limit
 *          description: Showing result in limit
 *          schema:
 *              type: integer
 *              enum : [10,20,30]
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          fuelTypeId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          variantId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          transmissionType:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          userId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          ownerType:
 *                              type: string
 *                              example: 2
 *                          manufactureYear:
 *                              type: string
 *                              example: 2010
 *                          priceRange:
 *                              type: string
 *                              example: 1000000
 *                          location:
 *                              type: object
 *                              properties:
 *                                  address:
 *                                     type: string
 *                                     example: Gota
 *                                  country:
 *                                     type: string
 *                                     example: Gota
 *                                  countryCode:
 *                                     type: number
 *                                     example: +91
 *                                  state:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  district:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  area:
 *                                     type: string
 *                                     example: Gota
 *                                  city:
 *                                     type: string
 *                                     example: Ahmedabad
 *                                  road:
 *                                     type: string
 *                                     example: SG highway
 *                                  postCode:
 *                                     type: number
 *                                     example: 380009
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// _________________list variant________________
/**
 * @swagger
 * /vehicletrade/dealer/on-sell:
 *  get:
 *      tags: [Dealer]
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

/**
 * @swagger
 * /vehicletrade/dealer/approve/user/inquiry:
 *  put:
 *      tags: [Dealer]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          vehicleId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          userId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete variant__________
/**
 * @swagger
 * /vehicletrade/dealer/user/inquiry:
 *  get:
 *      tags: [Dealer]
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________put refuseInquire__________
/**
 * @swagger
 * /vehicletrade/dealer/refuse/user/inquiry:
 *  put:
 *      tags: [Dealer]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          vehicleId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          userId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
