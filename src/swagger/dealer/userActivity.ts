// __________________add userActivity___________________
/**
 * @swagger
 * /vehicletrade/dealer/activity/add:
 *  post:
 *      summary: add new userActivity
 *      tags: [userActivity]
 *      description: add userActivity
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          vehicleId:
 *                              type: string
 *                              example: 644a15d9819efd2947e44c06
 *                          dealerStoreId:
 *                              type: string
 *                              example: 644a1514819efd2947e44bdc
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: userActivity added successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
// _________________list userActivity________________
/**
 * @swagger
 * /vehicletrade/dealer/activity/list:
 *  get:
 *      tags: [userActivity]
 *      parameters:
 *        - in: query
 *          name: attributeId
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *              enum: [10, 20, 25, 30, 40, 50, 60]
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      brandName:
 *                                          type: string
 *                                          example: Tata
 *                                      vehicleType:
 *                                          type: string
 *                                          example: Car
 *                                      brandLogo:
 *                                          type: string
 *                                          example: Car
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
// __________delete userActivity__________
/**
 * @swagger
 * /vehicletrade/dealer/activity/delete/{userActivityId}:
 *  delete:
 *      tags: [userActivity]
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
