/**
 * @swagger
 * /vehicletrade/user/login:
 *  post:
 *      tags: [Login]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          mobileNo:
 *                              type: string
 *                              example: 1212121212
 *                          otp:
 *                              type: string
 *                              example: 9090
 *      responses:
 *          200:
 *              description: for Admin Login.
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                                  example: cartrade@gmail.com
 *                              password:
 *                                  type: string
 *                                  example: cartrade@123
 *          400:
 *              description: for DEALER Login.
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              mobileNo:
 *                                  type: string
 *                                  example: 1234567899
 *                              otp:
 *                                  type: string
 *                                  example: 8080
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
