// __________________add variant___________________
/**
 * @swagger
 * /vehicletrade/admin/variant/add:
 *  post:
 *      summary: add new vehicle variant
 *      tags: [Variant]
 *      description: add variant
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          variantName:
 *                              type: string
 *                              example: XZ+
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: variant added successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// _________________list variant________________
/**
 * @swagger
 * /vehicletrade/admin/variant/list:
 *  get:
 *      tags: [Variant]
 *      parameters:
 *        - in: query
 *          name: modelId
 *          schema:
 *              type: string
 *              example: 62e2593894e09cd3eceb43cc
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      brandId:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      modelId:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      modelName:
 *                                          type: string
 *                                          example: Tata
 *                                      brandName:
 *                                          type: string
 *                                          example: Car
 *                                      variantName:
 *                                          type: string
 *                                          example: xz+
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________update variant___________________
/**
 * @swagger
 * /vehicletrade/admin/variant/update/{variantId}:
 *  put:
 *      summary: add new vehicle variant
 *      tags: [Variant]
 *      description: add variant
 *      parameters:
 *        - in: path
 *          name: variantId
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          productId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          variantName:
 *                              type: string
 *                              example: Tata
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: variant updated successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete variant__________
/**
 * @swagger
 * /vehicletrade/admin/variant/delete/{variantId}:
 *  delete:
 *      tags: [Variant]
 *      parameters:
 *        - in: path
 *          name: variantId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Variant deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
