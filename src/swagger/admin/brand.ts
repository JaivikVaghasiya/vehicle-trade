// __________________add category___________________
/**
 * @swagger
 * /vehicletrade/admin/brand/add:
 *  post:
 *      summary: add new vehicle brand
 *      tags: [Brand]
 *      description: add brand
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          attributeId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          brandName:
 *                              type: string
 *                              example: Tata
 *                          logo:
 *                              type: string
 *                              format:  binary
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: Brand added successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// _________________list category________________
/**
 * @swagger
 * /vehicletrade/admin/brand/list:
 *  post:
 *      tags: [Brand]
 *      parameters:
 *        - in: query
 *          name: attributeId
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *              enum: [10, 20, 25, 30, 40, 50, 60]
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390
 *                                      brandName:
 *                                          type: string
 *                                          example: Tata
 *                                      vehicleType:
 *                                          type: string
 *                                          example: Car
 *                                      brandLogo:
 *                                          type: string
 *                                          example: Car
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________update category___________________
/**
 * @swagger
 * /vehicletrade/admin/brand/update/{brandId}:
 *  put:
 *      summary: add new vehicle brand
 *      tags: [Brand]
 *      description: add brand
 *      parameters:
 *        - in: path
 *          name: brandId
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      requestBody:
 *          required: true
 *          content:
 *              multipart/form-data:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          attributeId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          brandName:
 *                              type: string
 *                              example: Tata
 *                          logo:
 *                              type: string
 *                              format:  binary
 *      responses:
 *          200:
 *              description:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: Brand updated successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete category__________
/**
 * @swagger
 * /vehicletrade/admin/brand/delete/{brandId}:
 *  delete:
 *      tags: [Brand]
 *      parameters:
 *        - in: path
 *          name: brandId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Category deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
