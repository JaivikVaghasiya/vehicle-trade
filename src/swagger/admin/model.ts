
// __________________add category___________________
/**
 * @swagger
 * /vehicletrade/admin/model/add:
 *  post:
 *      summary: add new vehicle model
 *      tags: [Model]
 *      description: add model
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelName:
 *                              type: string
 *                              example: Tata
 *      responses:
 *          200:
 *              description: 
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: model added successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// _________________list category________________
/**
 * @swagger
 * /vehicletrade/admin/model/list:
 *  get:
 *      tags: [Model]
 *      parameters:
 *        - in: query
 *          name: brandId
 *          schema:
 *              type: string
 *              example: 62e2593894e09cd3eceb43cc
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                              items:
 *                                  properties:
 *                                      _id: 
 *                                          type: string
 *                                          example: 63b916de232ffa00c9f20390               
 *                                      modelName: 
 *                                          type: string
 *                                          example: Tata               
 *                                      brandName: 
 *                                          type: string
 *                                          example: Car               
 *                                      brandLogo: 
 *                                          type: string
 *                                          example: Car               
 *
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */

// __________________update category___________________
/**
 * @swagger
 * /vehicletrade/admin/model/update/{modelId}:
 *  put:
 *      summary: add new vehicle model
 *      tags: [Model]
 *      description: add model
 *      parameters:
 *        - in: path
 *          name: modelId
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          brandId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          modelName:
 *                              type: string
 *                              example: Tata
 *      responses:
 *          200:
 *              description: 
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                                  example: true
 *                              message:
 *                                  type: string
 *                                  example: model updated successfully
 *                              responseData:
 *                                  type: object
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 62e2593894e09cd3eceb43cc
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */

// __________delete category__________
/**
 * @swagger
 * /vehicletrade/admin/model/delete/{modelId}:
 *  delete:
 *      tags: [Model]
 *      parameters:
 *        - in: path
 *          name: modelId
 *          required: true
 *          description: Delete your data
 *          schema:
 *              type: string
 *              example: 63b7c478a66e0602c01887fd
 *      responses:
 *          200:
 *              description: When response is correct
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 200
 *                              responseMessage:
 *                                  type: string
 *                                  example: Category deleted Successfully
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */