// _________________Dealer List________________
/**
 * @swagger
 * /vehicletrade/admin/dealer/list:
 *  get:
 *      summary: Listing of Dealers
 *      tags: [Dealer List]
 *      description: Dealer List
 *      parameters:
 *        - in: query
 *          name: page
 *          description: Redirect to entered page number
 *          schema:
 *              type: integer
 *        - in: query
 *          name: limit
 *          description: Return data of per page
 *          schema:
 *              type: integer
 *        - in: query
 *          name: sortField
 *          description: Enter item which will you want to order...
 *          schema:
 *              type: string
 *        - in: query
 *          name: sortType
 *          description: Enter value asc to ascending order and desc to descending order
 *          schema:
 *              type: string
 *              enum: ["asc", "desc"]
 *        - in: query
 *          name: search
 *          description: You can search items from here..
 *          schema:
 *              type: string
 *              enum: ["Tata"]
 *        - in: query
 *          name: role
 *          description: User role
 *          schema:
 *              type: string
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 */
/**
 * @swagger
 * /vehicletrade/admin/dealer/update:
 *  put:
 *      summary: Update Dealer
 *      tags: [Dealer List]
 *      description: Update Dealer List
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          dealerId:
 *                              type: string
 *                              example: 63b7c478a66e0602c01887fd
 *                          isActive:
 *                              type: boolean
 *                              example: true
 *      responses:
 *          200:
 *              description: Successful
 *              content:
 *                application/json:
 *                    schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                              example: true
 *                          message:
 *                              type: string
 *                              example: Successful
 *                          responseData:
 *                                  properties:
 *                                      _id:
 *                                          type: string
 *                                          example: 644a118b819efd2947e44bc5
 *                                      count:
 *                                          type: string
 *                                          example: 2
 *                                      time:
 *                                          type: string
 *                                          example: Apr 01 2023
 *          500:
 *              description: When Some error occurred..
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              responseCode:
 *                                  type: integer
 *                                  example: 500
 *                              responseMessage:
 *                                  type: string
 *                                  example: Internal server error
 *
 */
