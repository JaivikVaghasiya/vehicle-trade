import { Message, HttpStatus, send, logger } from '../utils';
import { Utils } from '../types';

/**
 * Common function for error handling
 */
export const errorHandler: Utils.ErrorHandler = (error, req, res, _next) => {
  logger.error('error :>>>', error);
  let detail: { [index: string]: any } = {};
  let log: { [index: string]: any } = {};

  let status = HttpStatus.SERVER_ERROR;
  let msg = Message.SOMETHING_WENT_WRONG;

  if (error.data || (Array.isArray(error.data) && error.data.length > 0)) {
    status = error.code;
    msg = error.message || msg;
    log = error.data;
  } else {
    if (Object.values(Message).includes(error.message)) {
      msg = error.message || msg;
      status = error.code || msg;
    }
    detail.message = error.message || Message.INTERNAL_SERVER_ERROR;
    detail.stack = error.stack;
    log.title = error.name;
    log.routeName = req.path;
    log.path = error.lineNumber;
    log.errorData = detail;
  }

  return send(res, status, msg, log);
};
