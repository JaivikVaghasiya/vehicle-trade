import { HttpStatus } from '../utils';

class RequestError extends Error {;
  code: number;
  data: any;
  public constructor( code: number, msg: string, data?: any) {
    super(msg);
    this.code = code;
    this.data = data;
  }
}

class BadRequestError extends RequestError {
  constructor(msg: string, data?: any) {
    super(HttpStatus.BAD_REQUEST, msg, data);
  }
}

class NotFoundError extends RequestError {
  constructor(msg: string, data?: any) {
    super(HttpStatus.NOT_FOUND, msg, data);
  }
}

export { RequestError,BadRequestError, NotFoundError };
