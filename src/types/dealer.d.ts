import { NextFunction, Request, Response } from 'express';
import {
  Common,
  ObjectId,
  Document,
  ReturnValue,
  PaginateQuery,
} from './utils';

export declare namespace Dealer {
  export interface DecodedToken {
    _id: string;
    iat: number;
    role: number | string;
    exp: number;
  }

  export interface DealerStoreBody {
    dealerId: ObjectId;
    storeName: string;
    employeeName: string;
    storeLocation:
      | {
          address: string;
          countryCode: string;
          country: string;
          state: string;
          district: string;
          area: string;
          city: string;
          road: string;
          postCode: number;
        }
      | any;
    storeContact: string;
    storeEmail: string;
    storeCloseTime: string;
    storeOpenTime: string;
    storeDocument?: string;
  }

  export interface DealerStore extends Document, Common, DealerStoreBody {}

  export interface VehicleBody {
    brandId: string | ObjectId;
    modelId: string | ObjectId;
    attributeId: string | ObjectId;
    variantId: string | ObjectId;
    plateNumber: string;
    ownerType: number;
    price: number;
    manufactureYear: number;
    colorCode: string;
    kiloMeters: number;
  }

  export interface Vehicle extends Document, Common, VehicleBody {
    sellDetails: {
      isSell: boolean;
      userId: ObjectId | null;
      sellingDate: number | null;
    };
    vehicleStatus: number;
  }

  export interface VehicleDetails extends Document, Common {
    vehicleId: ObjectId;
    fuelType: ObjectId;
    transmissionType: ObjectId;
    bodyType: ObjectId;
    dealerStoreId: ObjectId;
  }

  export interface VehicleDetailsBody {
    fuelType: ObjectId;
    transmissionType: ObjectId;
    bodyType: ObjectId;
    dealerStoreId: ObjectId;
  }

  export interface VehicleImageType extends Document, Common {
    type: string;
    vehicleId: ObjectId;
  }

  export interface VehicleImage extends Document, Common {
    image: string;
    vehicleId: ObjectId;
    vehicleImageTypeId: ObjectId;
  }

  export interface Image {
    vehicleId: ObjectId;
    image: string;
    mainImage: string;
    interiorImages: string;
    exteriorImages: string;
    profileImage: string;
    RCbookImage: string;
    policyImage: string;
    document: string;
    isActive: boolean;
    createdAt: number;
    updatedAt: number;
  }

  export interface VehicleListFilters {
    minPrice: string;
    maxPrice: string;
    minYear: string;
    maxYear: string;
    minKiloMeters: string;
    maxKiloMeters: string;
    search: string | any;
    latitude: string;
    longitude: string;
    vehicleType: string;
    brandName: string;
    modelName: string;
    variantName: string;
    fuelType: string;
    bodyType: string;
    transmissionType: string;
    country: string;
    state: string;
  }

  export interface ManualInquiryBody extends Document, Common {
    _id: string | Object;
    brandId: string | Object;
    modelId: string | Object;
    userId: string | Object;
    fuelTypeId: string | Object;
    variantId: string | Object;
    transmissionTypeId: string | Object;
    manufactureYear: number;
    isDelivered: boolean;
    isDeleted: boolean;
    priceRange: number;
    ownerType: number;
    inquiryStatus: {
      dueDate: number;
      storeId: ObjectId | string;
    };
    location: {
      location: {
        address: string;
        countryCode: string;
        country: string;
        state: string;
        district: string;
        area: string;
        city: string;
        road: string;
        postCode: number;
        mapLink: string;
      };
    };
  }

  type SoldVehicle = (dealerId: string, query: PaginateQuery) => ReturnValue;
  type RemainingVehicles = (
    dealerId: string,
    query: PaginateQuery,
  ) => ReturnValue;
  type UserInquiries = (dealerId: string, query: PaginateQuery) => ReturnValue;
  type ApproveInquiry = (body: {
    userId: string;
    vehicleId: string;
  }) => ReturnValue;
  type RefuseInquiry = (body: {
    userId: string;
    vehicleId: string;
  }) => ReturnValue;
  // Vehicle
  type AddVehicle = (
    vehicleBody: VehicleBody,
    vehicleDetailsBody: VehicleDetailsBody,
    files: any,
  ) => ReturnValue;
  type UpdateVehicle = (
    vehicleId: string,
    vehicleBody: VehicleBody,
    vehicleDetailsBody: VehicleDetailsBody,
    files: any,
  ) => ReturnValue;
  type ListVehicle = (vehicleId: string) => ReturnValue;
  type DeleteVehicle = (vehicleId: string) => ReturnValue;
  type DeleteVehicleImage = (imageId: string) => ReturnValue;
  type AddStore = (body: DealerStoreBody, files: any) => ReturnValue;
  type ListStore = (dealerId: string) => ReturnValue;
  type UpdateStore = (
    storeId: string,
    body: DealerStoreBody,
    files: any,
  ) => ReturnValue;
  type DeleteStore = (storeId: string) => ReturnValue;

  //dealerDashboard
  type weeklySell = (
    dealerId: string,
    query: { startOfWeek: string; endOfWeek: string },
  ) => ReturnValue;

  type monthlySellStore = (dealerId: any, query: any) => ReturnValue;

  type weeklySellStore = (
    dealerId: string,
    query: { startOfWeek: string; endOfWeek: string },
  ) => ReturnValue;

  type dailySellStore = (
    dealerId: string,
    query: { startOfDay: string; endOfDay: string },
  ) => ReturnValue;

  type daySell = (
    dealerId: string,
    query: { startOfDay: string; endOfDay: string },
  ) => ReturnValue;

  //userActivity
  type UserActivity = (body: {
    userId: string;
    vehicleId: string;
    dealerStoreId: string;
  }) => ReturnValue;
  type ListActivity = (dealerId: ObjectId, query: PaginateQuery) => ReturnValue;
  type DeleteActivity = (ActivityId: string) => ReturnValue;

  //allCurrentSellCount
  type CurrentSellCount = (dealerId: string) => ReturnValue;

  //monthlyDayStore
  type monthlyDayStore = (dealerId: any, query: any) => ReturnValue;
  //weeklyDayStore
  type weeklyDaySell = (dealerId: any, query: any) => ReturnValue;
  //yearlySell
  type yearlySell = (dealerId: any, query: any) => ReturnValue;
  //monthsellstore
  type monthSellStore = (dealerId: any, query: any) => ReturnValue;
  //yearSellStore
  type yearSellStore = (dealerId: any, query: any) => ReturnValue;
  //weeklySellStore
  type weekSellStore = (dealerId: any, query: any) => ReturnValue;

  type AddInquiry = (body: any) => ReturnValue;
  type ListInquiry = (dealerId: any, body: any, filter: any) => ReturnValue;
  type Listing = (dealerId: any, query: any) => ReturnValue;
}
