import { Common, ObjectId, Document, ReturnValue } from './utils';

export declare namespace Mechanic {
  export interface GarageBody {
    mechanicId: ObjectId;
    garageName: string;
    employeeName: string;
    location: {
      type?: string;
      coordinates: string[];
      address: string;
      countryCode: string;
      country: string;
      state: string;
      district: string;
      area: string;
      city: string;
      road: string;
      postCode: number;
    };

    garageContact: string;
    garageEmail: string;
    garageCloseTime: string;
    garageOpenTime: string;
  }

  export interface Garage extends Document, Common, GarageBody {}

  export interface GarageService extends Document, Common {
    garageId: ObjectId;
    services: [
      {
        serviceId: ObjectId;
        price: Number;
        isActive: boolean;
        isDeleted: boolean;
      },
    ];
    serviceTypeId: ObjectId;
  }

  // Garage Store
  type AddGarage = (body: GarageBody) => ReturnValue;
  type ListGarage = (mechanicId: string) => ReturnValue;
  type UpdateGarage = (garageId: string, body: GarageBody) => ReturnValue;
  type DeleteGarage = (garageId: string) => ReturnValue;

  //Garage Services
  type AddService = (body: any) => ReturnValue;
  type listService = (garageServicesId: string) => ReturnValue;
  type UpdateService = (garageServicesId: string, body: any) => ReturnValue;
  type UpdateStatus = (payload: {
    garageServicesId: ObjectId;
    isActive: boolean;
    serviceId: ObjectId;
  }) => ReturnValue;
  type DeleteService = (payload: {
    garageServicesId: ObjectId;
    serviceId: ObjectId;
    i;
  }) => ReturnValue;
}
