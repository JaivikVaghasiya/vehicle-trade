import { Common, ObjectId, Document, ReturnValue } from './utils';
import { Dealer } from './dealer';
import { type } from 'os';

export declare namespace User {
  export interface User extends Document, Common {
    firstName: string;
    lastName: string;
    profileImage?: string;
    email: string;
    mobileNo: string;
    role: number;
    approvalStatus: number;
    password: string;
  }

  export type VerifyOtp = (mobileNo: string, code: string, sid: string) => any;
  export type SendOtp = (mobileNo: string) => any;
  export type foregate = (email: string, message: string) => any;

  export interface Favorite extends Document, Common {
    vehicleId: ObjectId;
    userId: ObjectId;
    isFavorite: boolean;
  }
  export interface Comment extends Document, Common {
    vehicleId: ObjectId;
    userId: ObjectId;
    comment: string;
  }

  export interface UserInquiry extends Document, Common {
    vehicleId: ObjectId;
    userId: ObjectId;
    dealerStoreId: ObjectId;
    isRead: boolean;
  }

  export interface UserActivity extends Document, Common {
    dealerStoreId: ObjectId;
    userId: ObjectId;
    vehicleId: ObjectId;
  }

  type SignUp = (body: User, file: any) => ReturnValue;
  type LoginUser = (body: any) => ReturnValue;
  type FSendOtp = (mobileNo: string) => ReturnValue;
  type UserList = (user: any) => ReturnValue;
  type UserById = (userId: string) => ReturnValue;
  type UpdateUser = (userId: string, body: User, files: any) => ReturnValue;
  type DeleteUser = (userId: string) => ReturnValue;
  type forgetPassword = (body: any) => ReturnValue;
  type resetPassword = (body: any) => ReturnValue;

  // User Comment
  type AddComment = (body: {
    vehicleId: string;
    comment: string;
    userId: string;
  }) => ReturnValue;
  type ListComment = (vehicleId: string) => ReturnValue;
  type UpdateComment = (
    commentId: string,
    body: { vehicleId: string; comment: string; userId: string },
  ) => ReturnValue;
  type DeleteComment = (commentId: string) => ReturnValue;

  // Favorite
  type ListFavorite = (userId: string) => ReturnValue;
  type AddFavorite = (body: {
    userId: string;
    vehicleId: string;
  }) => ReturnValue;
  type DeleteFavorite = (favoriteId: string) => ReturnValue;

  // User Home
  type VehicleList = (
    query: Partial<Dealer.VehicleListFilters>,
    userId: string | null,
    match: any,
  ) => ReturnValue;
  type VehicleListById = (vehicleId: string) => ReturnValue;

  // User Inquiry
  type ListInquiry = (userId: string) => ReturnValue;
  type AddInquiry = (body: {
    userId: string;
    vehicleId: string;
    dealerStoreId: string;
  }) => ReturnValue;
  type DeleteInquiry = (inquiryId: string) => ReturnValue;
  // Forget Password
  type ForgetPassword = (body: any) => ReturnValue;
  // Reset Password
  type ResetPassword = (body: any) => ReturnValue;
  type CVerifyOtp = (body: any) => ReturnValue;
}
