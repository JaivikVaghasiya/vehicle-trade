import { NextFunction, Request, Response } from 'express';

export type Api = (req: Request, res: Response, next?: NextFunction) => any;
export type AsyncHandler = (fn: Api) => Api;
export type ValidationChain = import('express-validator').ValidationChain;
export { PaginateQuery, ReturnValue } from './utils.d';

export { User } from './user';
export { Dealer } from './dealer';
export { Admin } from './admin';
export { Mechanic } from './mechanic';
export { Utils } from './utils';
