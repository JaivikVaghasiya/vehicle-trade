import { NextFunction, Request, Response } from 'express';
import { Document as doc, Types } from 'mongoose';
import { User } from './user';

export type Document = doc;

export type ObjectId = Types.ObjectId;

export type ReturnValue = Promise<{
  message: string;
  responseData?: any;
}>;

export interface Common {
  isActive: boolean;
  isDeleted: boolean;
  createdAt: number;
  updatedAt: number;
}

export interface PaginateQuery {
  page: string;
  limit: number | string | any;
  sortField: string;
  sortType: 'asc' | 'desc';
  search: string;
  storeId?: ObjectId;
}

export declare namespace Utils {
  export type CResponse = (
    success: Boolean,
    Message: string,
    data?: any,
  ) => {
    success: boolean;
    message: string;
    data?: any;
  };

  export type ErrorHandler = (
    error: any,
    req: Request,
    res: Response,
    _next: NextFunction,
  ) => any;
  export interface CServices {
    sendResponse: (
      res: Response,
      httpStatus: number,
      Message: string,
      data?: any,
    ) => any;
    sendSuccess: (res: Response, message: string, payload: any) => any;
    generateToken: (user: User.User, expireTime: string) => string;
    verifyToken: (token: string) => DecodedToken | boolean;
    searching: {
      fullSearch: (search: string | any, operators: '$and' | '$or') => any;
      arraySearch: (search: string | any, operators: '$and' | '$or') => any;
      regexSearch: (search: any, key: string[]) => any;
    };
    paginationAndSorting: (
      query: PaginateQuery,
      defaultSortField: string,
    ) => {
      pageNumber: number;
      skipRecord: number;
      perPage: number;
      sortObj: { [key: string]: number };
    };
    paginateResponse: (
      data: any,
      pageNumber: number,
      perPage: number,
      totalRecord: number,
    ) => {
      list: any;
      page: number;
      perPage: number;
      RecordsFiltered: number;
      totalRecords: number;
      totalPage: number;
    };
  }

  export interface DecodedToken {
    _id: string;
    iat: number;
    role: number | string;
    exp: number;
  }

  export interface Files {
    name: string;
    data: Buffer;
    size: number;
    encoding: string;
    tempFilePath: string;
    truncated: boolean;
    mimetype: string;
    md5: string;
    mv: any;
  }
  export interface ImageData {
    'isMain[0]': string;
    'isMain[1]': string;
    image: string;
    vehicleId: string;
  }

  interface bunch {
    firstBunch: { key: string; value: any };
    secondBunch: { key: string; value: any };
    thirdBunch: { key: string; value: any };
  }
  export type DeleteFile = (filePath: string, id: string) => any;

  export type UploadImage = (
    [firstBunch, secondBunch, thirdBunch]: [
      { key: string; value: any },
      { key: string; value: any }?,
      { key: string; value: any }?,
    ],
    id: string,
  ) => Partial<ImageData | any>[] | any;

  export interface IsFileOptions {
    required: boolean;
    keys: string[];
    message: string | any;
  }
  export interface mailSender{
    from:string,
    to:string,
    subject:string,
    text:string
  }
}
