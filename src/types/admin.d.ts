import { PaginateQuery } from './utils.d';
import { Common, ObjectId, Document, ReturnValue } from './utils';

export declare namespace Admin {
  export interface Variant extends Document, Common {
    variantName: string;
    brandId: ObjectId;
    modelId: ObjectId;
  }

  interface Address extends Document, Common {
    countryCode: string;
    address: string;
    country: string;
    district: string;
    state: string;
    area: string;
    city: string;
    postCode: string;
    road: string;
  }

  export interface AttributesTypes extends Document, Common {
    attributeTypeName: string;
    description: string;
    flag: string;
  }

  export interface Attributes extends Document, Common {
    attributeName: string;
    attributeTypeId: ObjectId;
    attributeImage: string;
    description: string;
    flag: string;
  }

  export interface Brand extends Document, Common {
    attributeId: ObjectId;
    brandName: string;
    flag: string;
    logo?: string;
  }

  export interface VehicleBrandModel extends Document, Common {
    modelName: string;
    brandId: ObjectId;
  }

  export interface Country extends Document {
    countryName: string;
    countryIsoCode: string;
    isActive: boolean;
    isDeleted: boolean;
    createdAt: number;
    updatedAt: number;
  }

  export interface State extends Document {
    stateName: string;
    stateIsoCode: string;
    countryId: ObjectId;
    isActive: boolean;
    isDeleted: boolean;
    createdAt: number;
    updatedAt: number;
  }
  export interface BrandList extends PaginateQuery {
    flag?: string;
    isDropDown?: boolean;
  }

  //admin
  type VehicleRequests = (vehicleStatus: string) => ReturnValue;
  type UpdateVehicleStatus = (body: {
    vehicleId: string;
    vehicleStatus: number;
  }) => ReturnValue;
  type DealerRequests = (status: number) => ReturnValue;
  type UpdateDealerStatus = (body: {
    userId: string;
    dealerStatus: number;
  }) => ReturnValue;
  type AddressByLocation = (latitude: string, longitude: string) => ReturnValue;
  // attributes
  type AddAttribute = (
    body: { attributeName: string; attributeTypeId: string },
    files: any,
  ) => ReturnValue;
  type UpdateAttribute = (
    attributeId: string,
    body: { attributeName: string },
    files: any,
  ) => ReturnValue;
  interface AttributeQuery extends PaginateQuery {
    flag?: string;
  }
  type ListAttribute = (query: AttributeQuery) => ReturnValue;
  type DeleteAttribute = (attributeId: string) => ReturnValue;
  // attributes type
  type AddAttributeType = (body: {
    attributeTypeName: string;
    flag?: string;
  }) => ReturnValue;
  type UpdateAttributeType = (
    attributeTypeId: string,
    body: { attributeTypeName: string; flag?: string },
  ) => ReturnValue;
  type DeleteAttributeType = (attributeTypeId: string) => ReturnValue;
  // Brand
  type AddBrand = (
    body: { attributeId: string; brandName: string },
    files: any,
  ) => ReturnValue;
  type UpdateBrand = (
    brandId: string,
    body: { attributeId: string; brandName: string },
    files: any,
  ) => ReturnValue;
  type ListBrand = (body: BrandList) => ReturnValue;
  type DeleteBrand = (brandId: string) => ReturnValue;
  // country
  type AddCountry = (body: {
    countryName: string;
    countryIsoCode: string;
  }) => ReturnValue;
  type UpdateCountry = (
    countryId: string,
    body: { countryName: string; countryIsoCode: string },
  ) => ReturnValue;
  type ListCountry = () => ReturnValue;
  type DeleteCountry = (countryId: string) => ReturnValue;
  // Model
  interface ModelPaginateQuery extends PaginateQuery {
    brandId?: string | ObjectId;
  }
  type ListModel = (query: ModelPaginateQuery) => ReturnValue;
  type AddModel = (body: { brandId: string; modelName: string }) => ReturnValue;
  type UpdateModel = (
    modelId: string,
    body: { brandId: string; modelName: string },
  ) => ReturnValue;
  type DeleteModel = (modelId: string) => ReturnValue;
  // State
  type ListState = (body: {
    countryId: string;
    countryName: string;
  }) => ReturnValue;
  type AddState = (body: {
    stateIsoCode: string;
    stateName: string;
  }) => ReturnValue;
  type UpdateState = (
    stateId: string,
    body: { stateIsoCode: string; stateName: string },
  ) => ReturnValue;
  type DeleteState = (stateId: string) => ReturnValue;
  // Variant
  interface VariantPaginateQuery extends PaginateQuery {
    modelId?: string | ObjectId;
  }
  type ListVariant = (modelId: VariantPaginateQuery) => ReturnValue;
  type AddVariant = (body: {
    brandId: string;
    modelId: string;
    variantName: string;
  }) => ReturnValue;
  type UpdateVariant = (
    variantId: string,
    body: { brandId: string; modelId: string; variantName: string },
  ) => ReturnValue;
  type DeleteVariant = (variantId: string) => ReturnValue;
  type ListDealer = (query: any, userId: any) => ReturnValue;
  type UpdateDealer = (body: any) => ReturnValue;

  interface Role extends Document, Common {
    role: string;
    enum?: number;
  }
  type AddRole = (body: { role: string }) => ReturnValue;
  type ListRole = () => ReturnValue;
  type UpdateRole = (body: {
    roleId: string | ObjectId;
    role: string;
  }) => ReturnValue;
  type DeleteRole = (roleId: string | ObjectId) => ReturnValue;
  type updateStatus = (body: any) => ReturnValue;
}
