// import { NextFunction, Request, Response } from 'express';
// import { Document, Types } from 'mongoose';

// export type Api = (req: Request, res: Response, next?: NextFunction) => any;
// type ObjectId = Types.ObjectId;

// export interface Common {
//   isActive: boolean;
//   isDeleted: boolean;
//   createdAt: number;
//   updatedAt: number;
// }
// export interface User extends Document, Common {
//   firstName: string;
//   lastName: string;
//   profileImage?: string;
//   email: string;
//   mobileNo: string;
//   role: string;
//   approvalStatus: number;
//   password: string;
// }
// export interface Variant extends Document, Common {
//   variantName: string;
//   brandId: ObjectId;
//   modelId: ObjectId;
// }
// export interface Address extends Document, Common {
//   countryCode: string;
//   address: string;
//   country: string;
//   district: string;
//   state: string;
//   area: string;
//   city: string;
//   postCode: string;
//   road: string;
// }
// export interface AttributesTypes extends Document, Common {
//   attributeTypeName: string;
//   description: string;
// }
// export interface Attributes extends Document, Common {
//   attributeName: string;
//   attributeTypeId: ObjectId;
//   attributeImage: string;
//   description: string;
// }

// export type CResponse = (
//   success: Boolean,
//   Message: string,
//   data?: any,
// ) => {
//   success: boolean;
//   message: string;
//   data?: any;
// };

// export type ErrorHandler = (
//   error: any,
//   req: Request,
//   res: Response,
//   _next: NextFunction,
// ) => any;
// export interface CServices {
//   sendResponse: (
//     res: Response,
//     httpStatus: number,
//     Message: string,
//     data?: any,
//   ) => any;
//   sendSuccess: (res: Response, message: string, payload: any) => any;
//   generateToken: (user: User, expireTime: string) => string;
//   verifyToken: (token: string) => DecodedToken | boolean;
//   searching: {
//     fullSearch: (search: string | any, operators: '$and' | '$or') => any;
//     regexSearch: (search: any, key: string[]) => any;
//   };
//   paginationAndSorting: (
//     query: PaginateQuery,
//     defaultSortField: string,
//   ) => {
//     pageNumber: number;
//     skipRecord: number;
//     perPage: number;
//     sortObj: { [key: string]: number };
//   };
//   paginateResponse: (
//     data: any,
//     pageNumber: number,
//     perPage: number,
//     totalRecord: number,
//   ) => {
//     list: any;
//     page: number;
//     perPage: number;
//     RecordsFiltered: number;
//     totalRecords: number;
//     totalPage: number;
//   };
// }

// export interface DecodedToken {
//   _id: string;
//   iat: number;
//   role: number | string;
//   exp: number;
// }

// export interface DealerStore extends Document, Common {
//   dealerId: ObjectId;
//   storeName: string;
//   employeeName: string;
//   storeLocation:
//     | {
//         address: string;
//         countryCode: string;
//         country: string;
//         state: string;
//         district: string;
//         area: string;
//         city: string;
//         road: string;
//         postCode: number;
//       }
//     | any;
//   storeContact: string;
//   storeEmail: string;
//   storeCloseTime: string;
//   storeOpenTime: string;
//   storeDocument: string;
// }
// export interface DealerStoreBody {
//   dealerId: ObjectId;
//   storeName: string;
//   employeeName: string;
//   storeLocation:
//     | {
//         address: string;
//         countryCode: string;
//         country: string;
//         state: string;
//         district: string;
//         area: string;
//         city: string;
//         road: string;
//         postCode: number;
//       }
//     | any;
//   storeContact: string;
//   storeEmail: string;
//   storeCloseTime: string;
//   storeOpenTime: string;
//   storeDocument?: string;
// }

// export interface Vehicle extends Document, Common {
//   brandId: ObjectId;
//   modelId: ObjectId;
//   attributeId: ObjectId;
//   variantId: ObjectId;
//   plateNumber: string;
//   ownerType: number;
//   price: number;
//   manufactureYear: number;
//   vehicleStatus: number;
//   colorCode: string;
//   kiloMeters: number;
//   sellDetails: {
//     isSell: boolean;
//     userId: ObjectId | null;
//     sellingDate: number | null;
//   };
// }
// export interface VehicleBody {
//   brandId: string | ObjectId;
//   modelId: string | ObjectId;
//   attributeId: string | ObjectId;
//   variantId: string | ObjectId;
//   plateNumber: string;
//   ownerType: number;
//   price: number;
//   manufactureYear: number;
//   colorCode: string;
//   kiloMeters: number;
// }

// export interface VehicleDetails extends Document, Common {
//   vehicleId: ObjectId;
//   fuelType: ObjectId;
//   transmissionType: ObjectId;
//   bodyType: ObjectId;
//   dealerStoreId: ObjectId;
// }

// export interface VehicleDetailsBody {
//   fuelType: ObjectId;
//   transmissionType: ObjectId;
//   bodyType: ObjectId;
//   dealerStoreId: ObjectId;
// }

// export interface Brand extends Document, Common {
//   attributeId: ObjectId;
//   brandName: string;
//   logo?: string;
// }

// export interface VehicleBrandModel extends Document, Common {
//   modelName: string;
//   brandId: ObjectId;
// }

// export interface Files {
//   name: string;
//   data: Buffer;
//   size: number;
//   encoding: string;
//   tempFilePath: string;
//   truncated: boolean;
//   mimetype: string;
//   md5: string;
//   mv: any;
// }
// export interface ImageData {
//   'isMain[0]': string;
//   'isMain[1]': string;
//   image: string;
//   vehicleId: string;
// }

// interface bunch {
//   firstBunch: { key: string; value: any };
//   secondBunch: { key: string; value: any };
//   thirdBunch: { key: string; value: any };
// }
// export type DeleteFile = (filePath: string, id: string) => any;
// export type UploadImage = (
//   [firstBunch, secondBunch, thirdBunch]: [
//     { key: string; value: any },
//     { key: string; value: any }?,
//     { key: string; value: any }?,
//   ],
//   id: string,
// ) => Partial<ImageData | any>[] | any;

// export interface VehicleImageType extends Document, Common {
//   type: string;
//   vehicleId: ObjectId;
// }

// export interface VehicleImage extends Document, Common {
//   image: string;
//   vehicleId: ObjectId;
//   vehicleImageTypeId: ObjectId;
// }

// export interface Country extends Document {
//   countryName: string;
//   countryIsoCode: string;
//   isActive: boolean;
//   isDeleted: boolean;
//   createdAt: number;
//   updatedAt: number;
// }

// export interface State extends Document {
//   stateName: string;
//   stateIsoCode: string;
//   countryId: ObjectId;
//   isActive: boolean;
//   isDeleted: boolean;
//   createdAt: number;
//   updatedAt: number;
// }
// export type VerifyOtp = (mobileNo: string, code: string, sid: string) => any;
// export type SendOtp = (mobileNo: string) => any;

// export interface Favorite extends Document, Common {
//   vehicleId: ObjectId;
//   userId: ObjectId;
//   isFavorite: boolean;
// }
// export interface Comment extends Document, Common {
//   vehicleId: ObjectId;
//   userId: ObjectId;
//   comment: string;
// }

// export interface UserInquiry extends Document, Common {
//   vehicleId: ObjectId;
//   userId: ObjectId;
//   dealerStoreId: ObjectId;
//   isRead: boolean;
// }

// export interface Image {
//   vehicleId: ObjectId;
//   image: string;
//   mainImage: string;
//   interiorImages: string;
//   exteriorImages: string;
//   profileImage: string;
//   RCbookImage: string;
//   policyImage: string;
//   document: string;
//   isActive: boolean;
//   createdAt: number;
//   updatedAt: number;
// }

// export interface VehicleListFilters {
//   minPrice: string;
//   maxPrice: string;
//   minYear: string;
//   maxYear: string;
//   minKiloMeters: string;
//   maxKiloMeters: string;
//   search: string | any;
//   latitude: string;
//   longitude: string;
//   vehicleType: string;
//   brandName: string;
//   modelName: string;
//   variantName: string;
//   fuelType: string;
//   bodyType: string;
//   transmissionType: string;
//   country: string;
//   state: string;
// }

// export interface GarageBody {
//   mechanicId: ObjectId;
//   garageName: string;
//   employeeName: string;
//   location: {
//     type: string;
//     coordinates: string[];
//     address: string;
//     countryCode: string;
//     country: string;
//     state: string;
//     district: string;
//     area: string;
//     city: string;
//     road: string;
//     postCode: number;
//   };

//   garageContact: string;
//   garageEmail: string;
//   garageCloseTime: string;
//   garageOpenTime: string;
// }

// export interface Garage extends Document, Common, GarageBody {}

// export interface GarageService extends Document, Common {
//   garageId: ObjectId;
//   services: [
//     {
//       serviceId: ObjectId;
//       price: Number;
//       isActive: boolean;
//       isDeleted: boolean;
//     },
//   ];
//   serviceTypeId: ObjectId;
// }

// export type ReturnValue = Promise<{
//   message: string;
//   responseData?: any;
// }>;
// export interface UserActivity extends Document, Common {
//   dealerStoreId: ObjectId;
//   userId: ObjectId;
//   vehicleId: ObjectId;
// }
// type AsyncHandler = (fn: Api) => Api;
// type ValidationChain = import('express-validator').ValidationChain;

// export interface IsFileOptions {
//   required: boolean;
//   keys: string[];
//   message: string | any;
// }

// export interface PaginateQuery {
//   page: string;
//   limit: string;
//   sortField: string;
//   sortType: 'asc' | 'desc';
//   search: string;
//   storeId?: ObjectId;
// }
// export declare namespace Functions {
//   type SignUp = (body: User, file: any) => ReturnValue;
//   type LoginUser = (body: any) => ReturnValue;
//   type FSendOtp = (mobileNo: string) => ReturnValue;
//   type UserList = () => ReturnValue;
//   type UserById = (userId: string) => ReturnValue;
//   type UpdateUser = (userId: string, body: User, files: any) => ReturnValue;
//   type DeleteUser = (userId: string) => ReturnValue;
//   //admin
//   type VehicleRequests = (vehicleStatus: string) => ReturnValue;
//   type UpdateVehicleStatus = (body: {
//     vehicleId: string;
//     vehicleStatus: number;
//   }) => ReturnValue;
//   type DealerRequests = (status: number) => ReturnValue;
//   type UpdateDealerStatus = (body: {
//     userId: string;
//     dealerStatus: number;
//   }) => ReturnValue;
//   type AddressByLocation = (latitude: string, longitude: string) => ReturnValue;
//   // attributes
//   type AddAttribute = (
//     body: { attributeName: string; attributeTypeId: string },
//     files: any,
//   ) => ReturnValue;
//   type UpdateAttribute = (
//     attributeId: string,
//     body: { attributeName: string },
//     files: any,
//   ) => ReturnValue;
//   type ListAttribute = (attributeTypeId: string) => ReturnValue;
//   type DeleteAttribute = (attributeId: string) => ReturnValue;
//   // attributes type
//   type AddAttributeType = (body: { attributeTypeName: string }) => ReturnValue;
//   type UpdateAttributeType = (
//     attributeTypeId: string,
//     body: { attributeTypeName: string },
//   ) => ReturnValue;
//   type DeleteAttributeType = (attributeTypeId: string) => ReturnValue;
//   // Brand
//   type AddBrand = (
//     body: { attributeId: string; brandName: string },
//     files: any,
//   ) => ReturnValue;
//   type UpdateBrand = (
//     brandId: string,
//     body: { attributeId: string; brandName: string },
//     files: any,
//   ) => ReturnValue;
//   type ListBrand = (attributeId: string) => ReturnValue;
//   type DeleteBrand = (brandId: string) => ReturnValue;
//   // country
//   type AddCountry = (body: {
//     countryName: string;
//     countryIsoCode: string;
//   }) => ReturnValue;
//   type UpdateCountry = (
//     countryId: string,
//     body: { countryName: string; countryIsoCode: string },
//   ) => ReturnValue;
//   type ListCountry = () => ReturnValue;
//   type DeleteCountry = (countryId: string) => ReturnValue;
//   // Model
//   type ListModel = (brandId: string) => ReturnValue;
//   type AddModel = (body: { brandId: string; modelName: string }) => ReturnValue;
//   type UpdateModel = (
//     modelId: string,
//     body: { brandId: string; modelName: string },
//   ) => ReturnValue;
//   type DeleteModel = (modelId: string) => ReturnValue;
//   // State
//   type ListState = (countryId: string) => ReturnValue;
//   type AddState = (body: {
//     stateIsoCode: string;
//     stateName: string;
//   }) => ReturnValue;
//   type UpdateState = (
//     stateId: string,
//     body: { stateIsoCode: string; stateName: string },
//   ) => ReturnValue;
//   type DeleteState = (stateId: string) => ReturnValue;
//   // Variant
//   type ListVariant = (modelId: string) => ReturnValue;
//   type AddVariant = (body: {
//     brandId: string;
//     modelId: string;
//     variantName: string;
//   }) => ReturnValue;
//   type UpdateVariant = (
//     variantId: string,
//     body: { brandId: string; modelId: string; variantName: string },
//   ) => ReturnValue;
//   type DeleteVariant = (variantId: string) => ReturnValue;
//   // Dealer
//   type SoldVehicle = (dealerId: string, query: PaginateQuery) => ReturnValue;
//   type RemainingVehicles = (
//     dealerId: string,
//     query: PaginateQuery,
//   ) => ReturnValue;
//   type UserInquiries = (dealerId: string, query: PaginateQuery) => ReturnValue;
//   type ApproveInquiry = (body: {
//     userId: string;
//     vehicleId: string;
//   }) => ReturnValue;
//   type RefuseInquiry = (body: {
//     userId: string;
//     vehicleId: string;
//   }) => ReturnValue;
//   // Vehicle
//   type AddVehicle = (
//     vehicleBody: VehicleBody,
//     vehicleDetailsBody: VehicleDetailsBody,
//     files: any,
//   ) => ReturnValue;
//   type ListVehicle = (vehicleId: string) => ReturnValue;
//   type UpdateVehicle = (
//     vehicleId: string,
//     vehicleBody: VehicleBody,
//     vehicleDetailsBody: VehicleDetailsBody,
//     files: any,
//   ) => ReturnValue;
//   type DeleteVehicle = (vehicleId: string) => ReturnValue;
//   type DeleteVehicleImage = (imageId: string) => ReturnValue;
//   type UpdateVehicleDetail = (
//     vehicleDetailId: string,
//     body: VehicleDetailsBody,
//     files: any,
//   ) => ReturnValue;
//   type AddVehicleDetail = (body: VehicleDetailsBody, files: any) => ReturnValue;
//   // Dealer Store
//   type AddStore = (body: DealerStoreBody, files: any) => ReturnValue;
//   type ListStore = (dealerId: string) => ReturnValue;
//   type UpdateStore = (
//     storeId: string,
//     body: DealerStoreBody,
//     files: any,
//   ) => ReturnValue;
//   type DeleteStore = (storeId: string) => ReturnValue;

//   // User Comment
//   type AddComment = (body: {
//     vehicleId: string;
//     comment: string;
//     userId: string;
//   }) => ReturnValue;
//   type ListComment = (vehicleId: string) => ReturnValue;
//   type UpdateComment = (
//     commentId: string,
//     body: { vehicleId: string; comment: string; userId: string },
//   ) => ReturnValue;
//   type DeleteComment = (commentId: string) => ReturnValue;

//   // Favorite
//   type ListFavorite = (userId: string) => ReturnValue;
//   type AddFavorite = (body: {
//     userId: string;
//     vehicleId: string;
//   }) => ReturnValue;
//   type DeleteFavorite = (favoriteId: string) => ReturnValue;

//   // User Home
//   type VehicleList = (
//     query: Partial<VehicleListFilters>,
//     userId: string | null,
//   ) => ReturnValue;
//   type VehicleListById = (vehicleId: string) => ReturnValue;

//   // User Inquiry
//   type ListInquiry = (userId: string) => ReturnValue;
//   type AddInquiry = (body: {
//     userId: string;
//     vehicleId: string;
//     dealerStoreId: string;
//   }) => ReturnValue;
//   type DeleteInquiry = (inquiryId: string) => ReturnValue;

//   // Garage Store
//   type AddGarage = (body: GarageBody) => ReturnValue;
//   type ListGarage = (mechanicId: string) => ReturnValue;
//   type UpdateGarage = (garageId: string, body: GarageBody) => ReturnValue;
//   type DeleteGarage = (garageId: string) => ReturnValue;

//   //Garage Services
//   type AddService = (body: any) => ReturnValue;
//   type listService = (garageServicesId: string) => ReturnValue;
//   type UpdateService = (garageServicesId: string, body: any) => ReturnValue;
//   type UpdateStatus = (payload: {
//     garageServicesId: ObjectId;
//     isActive: boolean;
//     serviceId: ObjectId;
//   }) => ReturnValue;
//   type DeleteService = (payload: {
//     garageServicesId: ObjectId;
//     serviceId: ObjectId;
//     i;
//   }) => ReturnValue;

//   //dealerDashboard
//   type weeklySell = (
//     dealerId: string,
//     query: { startOfWeek: string; endOfWeek: string },
//   ) => ReturnValue;

//   type monthlySellStore = (dealerId: any, query: any) => ReturnValue;

//   type weeklySellStore = (
//     dealerId: string,
//     query: { startOfWeek: string; endOfWeek: string },
//   ) => ReturnValue;

//   type dailySellStore = (
//     dealerId: string,
//     query: { startOfDay: string; endOfDay: string },
//   ) => ReturnValue;

//   type daySell = (
//     dealerId: string,
//     query: { startOfDay: string; endOfDay: string },
//   ) => ReturnValue;

//   //userActivity
//   type UserActivity = (body: {
//     userId: string;
//     vehicleId: string;
//     dealerStoreId: string;
//   }) => ReturnValue;
//   type ListActivity = (dealerId: ObjectId, query: PaginateQuery) => ReturnValue;
//   type DeleteActivity = (ActivityId: string) => ReturnValue;

//   //allCurrentSellCount
//   type CurrentSellCount = (dealerId: string) => ReturnValue;

//   //monthlyDayStore
//   type monthlyDayStore = (dealerId: any, query: any) => ReturnValue;
//   //weeklyDayStore
//   type weeklyDaySell = (dealerId: any, query: any) => ReturnValue;
// }
